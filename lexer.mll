{
  open Lexing
  open Ast
  open Parser

  exception Lexing_error of string

  let new_line lexbuf =
          let pos = lexbuf.lex_curr_p in
          lexbuf.lex_curr_p <-
                { pos with pos_bol = lexbuf.lex_curr_pos;
                  pos_lnum = pos.pos_lnum + 1 
                }

  let id_or_kwd =
    let h = Hashtbl.create 32 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
    ["boolean", BOOLEAN; "class", CLASS; "else", ELSE; "extends", EXTENDS; 
    "false", FALSE; "if", IF; "implements", IMPLEMENTS; "int", INT; 
    "interface", INTERFACE; "main", MAIN; "new", NEW; "null", NULL; 
    "public", PUBLIC; "return", RETURN; "static", STATIC; "String", STRING; 
    "this", THIS; "true", TRUE; "void", VOID; "while", WHILE;];

  fun s -> try Hashtbl.find h s with Not_found -> IDENT s
  let string_buffer = Buffer.create 1024
}

let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let ident = (letter | '_') (letter | digit | '_')*
let integer = '0' | ['1'-'9'] digit*
let space = ' ' | '\t'
let comment = "//" [^'\n']*

rule next_tokens = parse
  | '\n'    { new_line lexbuf; next_tokens lexbuf }
  | "class Main" { [CLASS_MAIN] }
  | "System.out.print" | "System.out.println" { [SYSTEMOUTPRINT] }
  | (space | comment)+
            { next_tokens lexbuf }
  | ident as id { [id_or_kwd id] }
  | '+'     { [PLUS] }
  | '-'     { [MINUS] }
  | '*'     { [TIMES] }
  | "/"     { [DIV] }
  | '%'     { [MOD] }
  | '='     { [EQUAL] }
  | "=="    { [BEQ] }
  | "!="    { [BNEQ] }
  | "<="    { [BLE] }
  | "<"     { [BLT] }
  | ">"     { [BGT] }
  | ">="    { [BGE] }
  | '('     { [LP] }
  | ')'     { [RP] }
  | '['     { [LSQ] }
  | ']'     { [RSQ] }
  | '{'     { [LBRACE] }
  | '}'     { [RBRACE] }
  | ','     { [COMMA] }
  | ';'     { [SEMICOLON] }
  | '.'     { [DOT] }
  | "&&"    { [AND] }
  | "||"    { [OR] }
  | "!"     { [NOT] }
  | "&"     { [AMPERSAND] }
  | integer as s
            { try [CST (Cint (int_of_string s))]
              with _ -> raise (Lexing_error ("constant too large: " ^ s)) }
  | '"'     { [CST (Cstring (string lexbuf))] }
  | "/*"    { comment lexbuf }
  | eof     { [EOF] }
  | _ as c  { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }

and comment = parse
  |"*/"     { next_tokens lexbuf }
  |'\n'     { new_line lexbuf; comment lexbuf }
  | _       { comment lexbuf }
  | eof     { raise (Lexing_error ("unfinished comment")) } 

and string = parse
  | '"'
      { let s = Buffer.contents string_buffer in
	Buffer.reset string_buffer;
	s }
  | "\\n"
      { Buffer.add_char string_buffer '\n';
	string lexbuf }
  | "\\\""
      { Buffer.add_char string_buffer '"';
	string lexbuf }
  | "\\\\"
      { Buffer.add_char string_buffer '\\';
	string lexbuf }
  | "\\" 
      { raise (Lexing_error "illegal escape character") }
  | "\n"
      { raise (Lexing_error "unclosed string literal") }
  | _ as c
      { Buffer.add_char string_buffer c;
	string lexbuf }
  | eof
      { raise (Lexing_error "unclosed string literal") }

{
  let next_token =
    let tokens = Queue.create () in 
    fun lb ->
      if Queue.is_empty tokens then begin
	let l = next_tokens lb in
	List.iter (fun t -> Queue.add t tokens) l
      end;
      Queue.pop tokens
}



