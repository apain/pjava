(*May be useful using a definition of typing environment as this one*)

typingEnvironment = {
	suptypeDecl : (typ list) SMap.t ; (*Superclass is a ref to inheritance*)
	classes : typedClass SMap.t ;
	vars : varType SMap.t ;
	paramTypes : SSet.t (* Contains the class names of type parameters *)
}

(*To avoid double definition we check that a class exists*)

let classExists env clName=
	if not (SMap.mem clName env.classes) then
		raise (TyperError ("The class '"^clName^"' is not defined."))

(*FindClass*)

let findClass cl env reqLoc =
	try SMap.find cl env.classes
	with Not_found -> raise (TyperError (reqLoc, "Class "^cl^", requested "^
		"from here, was not found in the environment."))

(*To avoid that types/parameters/... have distinct names.*)

let rec doubleName l =
	let rec IsAtHead e = function
	| [] -> false
	| hd::tl -> if hd = e then true else IsAtHead e tl
	in
	match l with
	| [] -> false
	| hd::tl -> if IsAtHead hd tl then true else doubleName tl
	
    (*NOT SURE this FUNCTION is NECESSARY:
	We need:
	- substClassTypes: that basically corresponds to what you wrote concerning classes typing;
	-substTypesMethTypes: that is the equivalent
	-checkSubstWellFormed: that is already encoded in the typing you have done*)
	
	let substTypes ty = substClassTypes env classType exp.eloc
			(substMethTypes env meth argt exp.eloc ty) in
	(* Well-formed check *)
	(match argt with
	| EmptyAType -> ()
	| ArgType(t) -> 
		checkSubstWellFormed env meth.tparTypes t meth.tmname exp.eloc
			substTypes);

(*Types a full class in the given environment, and returns the same environment, to which the freshly typed class was added*)

let doClassTyping env (cl : Ast.cls) : typedClass * typingEnvironment = 
	
	(*IMPORTANT: I think we need to add a typed class to the typed abstract syntax tree*)

	let nEnv = ref env in
	let inheritFromClass env (cl : typedClass) supertype clLoc =
		let super = (try SMap.find (fst supertype) env.classes 
		(*env.classes is to be substitutide or we need the first def I put at the really beginning here *)
			
			(*Check that we are inheriting from an existing class*)
			with Not_found ->
				raise (TyperError (clLoc, "The class `"^(fst supertype)^
					", inherited by "^(cl.tcname)^", was not declared in "^
					"this scope."))) in

		let outCl = ref cl in
		let substTyp = substClassTypes env supertype clLoc in (*We should use your def for substClassTypes*)
		SMap.iter (fun key field ->
				outCl := classAddVar !outCl key (fst field, 
				
				(*the idea of classAddvariable is: 
				let classAddVar cl vName var =
				let nVars = SMap.add vName var cl.tcvars in
				{ cl with tcvars = nVars } *)

					(substTyp (snd field)))) 

	(* Check that the class was not previously defined. This avoid classes with the same name *)
	if SMap.mem cl.cname env.classes then
		raise (TyperError (cl.cLoc, "A class named "^cl.cname^" is "^
			"already defined."));

	(* Adding the T from [+/-/() T] to the environment *)
	if doubleName (List.map (fun k -> (fst k).name) cl.classTypes) then
		raise (TyperError (cl.cLoc, "The same name was used twice in the "^
			"type parameters of this class."));
	addParamType cl.classTypes nEnv (fun k -> k ) cl.cLoc true;

	(* Inheritance *)
   let nClass =
        cls = {
                 c_name: ident;
                 c_params: paramstype option;
                 c_extends: ntype option;
                 c_implements: ntype list option;
                 c_declarations: decl list }




	let nClass =
		{tcname = cl.cname ;
		 tclassTypes = cl.classTypes ;
		 tcparams = cl.cparams ;
		 textends = None ; (* Done later *)
		 tcmeth = SMap.empty ;
		 tcvars = SMap.empty ;
		} in
	let nClass = ref
		(match cl.extends with
		| None ->
			{ nClass with textends =
				Some {textType=("AnyRef",EmptyAType) ; tparam=[]} }
		| Some t ->
			if List.mem (fst t.extType) ["String"] then
				raise (TyperError (cl.cLoc, "This class cannot inherit from "^
					(fst t.extType)^", as it is a base type."))
			else if not (SMap.mem (fst t.extType) env.classes) then
				raise (TyperError (cl.cLoc, "This class cannot inherit from "^
					(fst t.extType)^", as it is not defined."));
			inheritFromClass !nEnv nClass t.extType cl.cLoc
		) in

	let curEnv cenv =
		addClass cenv (cl.cname) !nClass
	in
	(* Parameters checking and adding *)
	if doubleName (List.map fst cl.cparams) then
		raise (TyperError (cl.cLoc, "The same name was used twice "^
			"in the parameters of this class."));
	List.iter (fun (idt,ty) ->
			checkWellFormed (curEnv !nEnv) ty cl.cLoc (*FIXME imprecise loc*);
		cl.cparams;
	(* "This" adding *)
	let thisArgt = (match cl.classTypes with
		| [] -> EmptyAType
		| l -> ArgType (List.map (fun ptc -> ((fst ptc).name, EmptyAType)) l))
	in
	nEnv := addVar (curEnv !nEnv) "this" (false, (cl.cname, thisArgt)); (*I am sure you have defined a function like this but i was not able to find it, I need only to add the variable*)

	(* Check that we can construct the inherited class *)
	let makeTypeExtends = function
	| None ->
		Some { textType = ("AnyRef", EmptyAType) ; tparam = [] }
	| Some ext ->
		Some { textType = ext.extType ;
			tparam = List.map (exprType !nEnv) ext.param }
	in
	nClass := { !nClass with textends = makeTypeExtends cl.extends };
	(match cl.extends with
		| None -> ()
		| Some t ->
			let inheritedTyp = (exprType (curEnv !nEnv)
				{ ex =Cconstr( (fst t.extType),(snd t.extType), t.param);
				  eloc = cl.cLoc (*I think you used expr to do that*)
				}).etyp
			in
			if inheritedTyp <> t.extType then
				raise (TyperError (cl.cLoc, ("Illegal call to the superclass "^
					"constructor while declaring class "^cl.cname^".")))
	);

	(*** Body ***)
	(* Check that methods are uniquely defined *)
	if doubleName (List.fold_left (fun cur v -> match v with
			| Dmeth(meth) -> (meth.mname)::cur
			| _ -> cur) [] cl.cbody) then
		raise (TyperError (cl.cLoc, "The same name was used twice in the "^
			"methods names of "^cl.cname^"."));
				
	List.iter (fun declare -> match declare with
		| Dmeth(methDecl) ->
			let locEnv = ref !nEnv in

			(* Type parameters *)
			if doubleName (List.map (fun k->k.name) methDecl.parTypes) then
				raise (TyperError (methDecl.mLoc, "The same name was used "^
					"twice in the type parameters of this method."));
			addParamType (List.map (fun k -> (k,TMneutral)) methDecl.parTypes)
				locEnv curEnv methDecl.mLoc false;

			let curEnvLoc e =
				(*Cur class with the currently typed method with a dummy body*)
				addClass e cl.cname
					(addMeth !nClass methDecl.mname {
						tmname = methDecl.mname ;
						tparTypes = methDecl.parTypes ;
						tmparams = methDecl.mparams ;
						tretTyp = methDecl.retType ;
						tmbody = {tex = TEblock([]) ;etyp = methDecl.retType};
						toverride = methDecl.override
						})
			in
			(* Parameters *)
			if doubleName (List.map fst methDecl.mparams) then
				raise (TyperError (methDecl.mLoc, "The same name was used "^
					"twice in the parameters of this method."));
			List.iter (fun (idt,ty) ->
					(*FIXME imprecise loc*)
					checkWellFormed (curEnvLoc !locEnv) ty methDecl.mLoc;
					checkVariance (curEnvLoc !locEnv) ty TMminus methDecl.mLoc;
					locEnv := addVar !locEnv idt (false,ty))
				methDecl.mparams;
			locEnv := addVar !locEnv "_return" (false, methDecl.retType) ;

			(try
				let supermeth = SMap.find methDecl.mname !nClass.tcmeth in
				if not methDecl.override then
					raise (TyperError (methDecl.mLoc,("This method was "^
						"previously declared.")));

				(* As we are comparing from the inherited method we only have to substitute
				   type parameters' names *)
				let substParamTypeName tName =
					let rec doSubst curPT superPT = match (curPT,superPT) with
					| [],[] -> tName
					| [],_|_,[] -> 
						raise (TyperError (methDecl.mLoc, ("This overriding "^
							"method doesn't have the same number of "^
							"parameters than the method it is overriding.")))
					| chd::ctl, shd::stl ->
						if shd.name = tName then
							chd.name
						else
							doSubst ctl stl
					in
					doSubst methDecl.parTypes supermeth.tparTypes
				in
				let rec substParamType ty =
					(substParamTypeName (fst ty), (match snd ty with
						| EmptyAType -> EmptyAType
						| ArgType(tt) -> ArgType (List.map substParamType tt)
						))
				in

				let rec compareParTypes nPar supPar parId =
					match nPar,supPar with
				| [],[] -> ()
				| [],_ | _,[] ->
					raise (TyperError (methDecl.mLoc, ("This overriding "^
						"method doesn't have the same number of parameters "^
						"than the method it is overriding.")))
				| (_,newHd)::newTl, (_,supHd)::supTl ->
					if newHd <> (substParamType supHd) then
						raise (TyperError (methDecl.mLoc, ("Parameter "^
							(string_of_int parId)^" has type "^
							(dispType newHd)^", yet it is overriding a "^
							"parameter of type "^
							(dispType (substParamType supHd))^".")));
					compareParTypes newTl supTl (parId+1)
				in
				compareParTypes methDecl.mparams supermeth.tmparams 1;

				(* return type *)
				if not (isSubtype (curEnvLoc !locEnv)
						methDecl.retType methDecl.mLoc
						(substParamType supermeth.tretTyp)) then
					raise (TyperError (methDecl.mLoc, ("This overriding "^
						"method's return type "^(dispType methDecl.retType)^
						" is not a subtype of "^(dispType supermeth.tretTyp)^
						", the return type of the method it is overriding.")))
	

			with Not_found ->
				if methDecl.override then
					raise (TyperError (methDecl.mLoc, ("This method is "^
						"declared as overriding, yet there is no such method "^
						"to override in the superclass.")))
			);

			

let doPrgmTyping (prgm : Ast.prgm) =
	let smap_of_list l =
		List.fold_left (fun cur (id,v) -> SMap.add id v cur) SMap.empty l in
	let mkBaseClass (name,inher) =
		(name, { tcname=name ; tclassTypes=[] ; tcparams=[] ;
				 textends=
				 	if inher <> "" then Some
						{ textType=(inher,EmptyAType) ; tparam=[] }
					else None ;
				     tcvars=SMap.empty ; tcmeth=SMap.empty ;
				 } )
	in
	let baseClasses = smap_of_list (List.map mkBaseClass [
			"Object","String";
    ]) in
	(* Add Array. We consider it to be user-defined, would be a hell of a mess
	  otherwise. *)
	let baseClasses = SMap.add "Array"
		{ (snd (mkBaseClass ("Array","AnyRef"))) with
			tclassTypes = [( {name="ty";rel=NoClassRel;oth=("",EmptyAType)},
					TMneutral )]
		} baseClasses in
	let env = ref { suptypeDecl = SMap.empty ;
		classes = baseClasses ;
		vars = SMap.empty ;
		paramTypes = SSet.empty } 
