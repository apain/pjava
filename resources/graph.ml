exception Cycle
type mark = NotVisited | InProgress | Visited

type 'a graph =
    { mutable g_vertices : 'a node list }
and 'a node = {
  n_label : 'a;
  mutable n_mark : mark;
  mutable n_link_to : 'a node list;
  mutable n_linked_by : 'a node list;
}

let mk_graph () = { g_vertices = [] }

let add_node g x =
  let n = { n_label = x; n_mark = NotVisited; n_link_to = []; n_linked_by = [] } in
  g.g_vertices <- n::g.g_vertices

let node_for_label g x =
  List.find (fun n -> n.n_label = x) g.g_vertices

let add_edge g id1 id2 =
  let n1 = node_for_label g id1 in
  let n2 = node_for_label g id2 in
  n1.n_link_to <- n2::n1.n_link_to;
  n2.n_linked_by <- n1::n2.n_linked_by

let clear_marks g =
  List.iter (fun n -> n.n_mark <- NotVisited) g.g_vertices

let find_roots g =
  List.filter (fun n -> n.n_linked_by = []) g.g_vertices

let has_cycle g =
  clear_marks g;
  let rec helper lst =
    match lst with
    | [] -> false
    | y :: _ when y.n_mark = InProgress -> true
    | y :: ys -> pp y || helper ys
  and pp x =
    match x.n_mark with
    | Visited -> false
    | NotVisited ->
       x.n_mark <- InProgress;
       let res = helper x.n_link_to in
       x.n_mark <- Visited;
       res
    | InProgress -> assert false
  in
  List.exists pp g.g_vertices
