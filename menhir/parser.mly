%{
  open Ast
%}

/* Definition of tokens */
%token <Ast.constant> CST
%token <string> IDENT
%token BOOLEAN CLASS ELSE EXTENDS FALSE IF IMPLEMENTS INT INTERFACE MAIN NEW NULL PUBLIC RETURN STATIC STRING THIS TRUE VOID WHILE (* Keywords *)
%token CLASS_MAIN SYSTEMOUTPRINT
%token PLUS MINUS TIMES DIV MOD EQUAL (* Operators *)
%token BEQ BNEQ BLE BGE BLT BGT (* Comparisons *)
%token AND OR NOT (* Logic *)
%token LP RP LSQ RSQ LBRACE RBRACE (* Parenthesis *)
%token COMMA SEMICOLON DOT AMPERSAND (* Syntax *)
%token NO_ELSE (* To solve dangling else problem *)
%token EOF 

/* Definitions of properties and associativity of tokens */
%left EQUAL
%left OR
%left AND
%left BEQ BNEQ
%left BLE BGE BLT BGT
%left PLUS MINUS
%left TIMES DIV MOD
%nonassoc unary_minus
%right NOT
%nonassoc NO_ELSE
%nonassoc ELSE

/* Grammar */
%start file
%type <Ast.file> file

%%

file:
  class_intfs = class_interface*
  main = class_main
  EOF
  { Ffile (class_intfs, main) }
;

class_interface:
| CLASS id = ident par = paramstype?
    nt = preceded(EXTENDS, ntype)?
    ntList = preceded(IMPLEMENTS, separated_nonempty_list(COMMA, ntype))?
    LBRACE declList = decl* RBRACE; 
    { CClass 
            { 
            c_name = id;
            c_params = par;
            c_extends = nt;
            c_implements = ntList;
            c_declarations = declList;
            c_loc = $loc;
            }
    }
| INTERFACE id = ident par = paramstype?
  nt = preceded(EXTENDS, separated_nonempty_list(COMMA, ntype))?
  LBRACE prot = terminated(proto, SEMICOLON)* RBRACE;
  { CInterface
          {
          i_name = id;
          i_params = par;
          i_extends = nt;
          i_protocols = prot;
          it_loc = $loc;
          }
  }
;

paramstype:
par = delimited(BLT, separated_nonempty_list(COMMA, paramtype), BGT) { Pparamstype(par) }
;

paramtype:
id = ident nt = preceded(EXTENDS, separated_nonempty_list(AMPERSAND, ntype))? { Pparamtype (id, nt) }

decl:
d = _decl { { d_desc = d; d_loc = $loc } }

_decl:
| t = typ id = ident SEMICOLON { Ddecl (t, id) }
| cstr = constructor { Dconstructor cstr }
| methd = mtd { Dmethod methd }
| PUBLIC methd = mtd { Dmethod methd }
;

constructor:
id = ident LP par = separated_list(COMMA, parameter) RP LBRACE instr = instruction* RBRACE { Cconstr (id, par, instr) }
;

mtd:
prot = proto LBRACE instr = instruction* RBRACE { Mmtd (prot, instr) }
;

proto:
| t = typ id = ident LP par = separated_list(COMMA, parameter) RP { Pproto (t, id, par) }
| VOID id = ident LP par = separated_list(COMMA, parameter) RP { PprotoVoid (id, par) } 
;

parameter:
t = typ id = ident { Pparam (t, id) } 
;

typ:
| BOOLEAN { Tbool } 
| INT { Tint } 
| nt = ntype; { Tntype nt } 
| STRING { Tstring }
;

ntype:
| id = ident nt = delimited(BLT, separated_nonempty_list(COMMA, ntype), BGT)? { Nntype (id, nt) } 
(*| STRING { NntypeString }*)
;

class_main:
CLASS_MAIN LBRACE PUBLIC STATIC VOID MAIN LP STRING LSQ RSQ id = ident RP LBRACE instr = instruction* RBRACE RBRACE { CClassMain (id, instr) } 
 ;

expr:
e = _expr { { e_desc = e; e_loc = $loc } }

_expr:
| NULL { Enull }
| s_e = simple_expr { s_e.e_desc }
| acc = access EQUAL e = expr { EaccessExpr (acc, e) }
| MINUS e = expr %prec unary_minus { Eunop (Uneg, e) }
| NOT e = expr { Eunop (Unot, e) }
| e1 = expr op = binop e2 = expr { Ebinop (op, e1, e2) }
;

access:
a = _access { { a_desc = a; a_loc = $loc } }

_access:
| id = ident { Aident id }
| s_e = simple_expr DOT id = ident { Aattr (s_e, id) }
;

simple_expr:
e = _simple_expr { { e_desc = e; e_loc = $loc } }

_simple_expr:
| c = CST { SEconstant c }
| TRUE  { SEbool true }
| FALSE { SEbool false }
| THIS { SEthis }
| LP e = expr RP { e.e_desc }
| NEW nt = ntype LP e = separated_list(COMMA, expr) RP { SEnew (nt, e) }
| acc = access LP e = separated_list(COMMA, expr) RP { SEmethodCall (acc, e) }
| acc = access { SEaccess acc }
| SYSTEMOUTPRINT LP e = expr RP { Eprint e }
;

%inline binop:
| BEQ   { Beq }
| BNEQ  { Bneq }
| BLT   { Blt }
| BLE   { Ble }
| BGT   { Bgt }
| BGE   { Bge }
| PLUS  { Badd } 
| MINUS { Bsub }
| TIMES { Bmul }
| DIV   { Bdiv }
| MOD   { Bmod } 
| AND   { Band }
| OR    { Bor } 
;

instruction:
i = _instruction { { i_desc = i; i_loc = $loc } }

_instruction:
| SEMICOLON { Ivoid } 
| s_e = simple_expr SEMICOLON { Ise s_e }
(*| SYSTEMOUTPRINT LP e = expr RP SEMICOLON { Ise (Eprint e) }*)
| acc = access EQUAL e = expr SEMICOLON { Iassign (acc, e) }
| t = typ id = ident SEMICOLON { Ideclare (t, id) }
| t = typ id = ident EQUAL e = expr SEMICOLON { IassignTyp (t, id, e) }
| IF LP e = expr RP instr = instruction %prec NO_ELSE { Iif (e, instr) }
| IF LP e = expr RP instr1 = instruction ELSE instr2 = instruction { IifElse (e, instr1, instr2) }
| WHILE LP e = expr RP instr = instruction { Iwhile (e, instr) }
| LBRACE instr = instruction* RBRACE { Iblock instr }
| RETURN e = expr? SEMICOLON { Ireturn e }
; 
(*
ifexpr:
| LP e = expr RP instr = instruction { If (e, instr) }
;*)

ident:
id = IDENT { id }
;
