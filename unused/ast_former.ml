(*Abstract syntax tree for MiniJava*)

type ident = string (*Identificators*)

type void = TVoid

type unop =(*Unary operators*)
  | Uneg (*-e*)
  | Unot (*!e*)

type binop = (*Binary operators*)
  | Badd | Bsub | Bmul | Bdiv | Bmod    (* + - * / % *)
  | Beq | Bneq | Blt | Ble | Bgt | Bge  (* == != < <= > >= *)
  | Band | Bor                          (* && || *)

(*I think that normally here are defined the constants*)
 
type file = (*File*)
  Ffile of class_interface list * class_main

and class_interface = 
  | CClass of cls
  | CInterface of interface

and cls = {
  c_name: ident;
  c_params: paramstype option;
  c_extends: ntype option;
  c_implements: ntype list option;
  c_declarations: decl list }

and interface = {
  i_name: ident;
  i_params: paramstype option;
  i_extends: ntype list option;
  i_protocols: proto list }

and paramstype =
  Pparamstype of paramtype list

and paramtype =
  Pparamtype of ident * ntype list option

and decl =
  | Ddecl of typ * ident
  | Dconstructor of constructor
  | Dmethod of mtd
  
and constructor =
  Cconstr of ident * parameter list * instruction list

and mtd =
  Mmtd of proto * instruction list

and proto =
  | Pproto of typ * ident * parameter list
  | PprotoVoid of ident * parameter list

and parameter =
  Pparam of typ * ident

and typ =
  | Tbool 
  | Tint 
  | Tntype of ntype

and ntype = 
  | Nntype of ident * ntype list option 
  | NntypeString

and class_main =
  CClassMain of ident * instruction list

and expr = 
  | Enull
  | Esimpl of simple_expr
  | Eaccess of access * expr
  | Eunop of unop * expr 
  | Ebinop of binop * expr * expr
  | Eprint of expr

and access =
  | Aident of ident (** Get the value of a variable. *)
  | Aattr of simple_expr * ident

and simple_expr =
  | SEconstant of constant
(*  | Cstring of string
  | Cint of int*)
  | SEbool of bool
  | SEthis 
  | SEexpr of expr
  | SEnew of ntype * expr list
  | SEaccessList of access * expr list
  | SEaccess of access

and constant =
  | Cstring of string
  | Cint of int

and instruction =
  | Ivoid
  | Ise of simple_expr
  | Iassign of access * expr
  | Ideclare of typ * ident
  | IassignTyp of typ * ident * expr
  | Iif of expr * instruction
  | IifElse of expr * instruction * instruction
  | Iwhile of expr * instruction 
  | Iblock of instruction list 
  | Ireturn of expr option
 (*We may think to decore the abstract syntax tree directly here*)
 (* and pos_expr = {e_desc : expr; e_pos : Lexing.position * Lexing.position}
and type_decl = string
*)

