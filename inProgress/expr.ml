open Format
open Ast
open Typed_ast
open X86_64


type dataLocation =
	| Here							(* Right where we are pointing *)
	| FollowPtr of dataLocation		(* Follow the address of the pointer *)
	| Offset of int * dataLocation	(* Add an offset *)

(*type classMetaDescriptor = {
	memLoc : label ; (* Location in memory *)
	methods : int SMap.t ;
		(* maps a method name to its offset in the descriptor *)
	methLabels : label SMap.t ;
		(* maps a method name to its label in the assembly. Useful for
		inheritance. *)
	vals : int SMap.t ;
		(* maps a field name to its offset in an allocated object on the heap*)
	valsOrder : typVar list ;
		(* Lists the fields in the order they must be allocated. *)
	clType : typedClass
}*)

let metaDescriptors = ref SMap.empty

(***
 * Returns a fresh new unused ID for a string label in the data block
 ***)
let nextStringId =
	let nextStringId = ref (-1) in
	(fun () ->
		nextStringId := !nextStringId + 1 ;
		("string"^(string_of_int !nextStringId)))

let nextLazyLabel =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; "lazyness"^(string_of_int !next))
let nextIfId =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; "L"^(string_of_int !next)^"_")
let nextWhileId =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; "L"^(string_of_int !next)^"_while")
let nextMethLabel =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; (string_of_int !next))

(***
 * Inserts a string into the program data, return both its label and the
 * assembly code.
 ***)
let addDataString s =
	let dataLabel = nextStringId () in
	(dataLabel, (label dataLabel) ++ (string s))

(***
 * Returns a program with only a text field with value t
 ***)
let prgmText t =
	{
		data = nop;
		text = t
	}


(***
 * Returns a code following a location from the address in %rdx and putting it
 * in %rdx.
 ***)
let rec followLocation = function
| Here -> nop
| FollowPtr(next) -> movq (ind rdx) (reg rdx) ++ (followLocation next)
| Offset (offset, next) -> addq (imm offset) (reg rdx) ++ (followLocation next)

(***
 * Returns assembly code that allocates a data block for an instantiation of
 * clName on the heap using sbrk. After this code is executed, the segment
 * start is found in %rax
 ***)
let allocateBlock clName =
	let cl = SMap.find clName !metaDescriptors in
	let segSize = (SMap.cardinal cl.vals + 1) * 8 in

	(movq (imm segSize) (reg rdi)) ++ (call "sbrk") ++
		(movq (ilab cl.memLoc) (ind rax))

(***
 * Compiles a typed expression.
 * Until a better option is found (1st pass to assign registers to
 * intermediary calculations?), the result of an expression is stored
 * into %rdi at the end of the code returned by compileExpr.
 ***)
let rec compile_Expr argExp env stackDepth = match argExp.tex with
| TEint n ->
	prgmText (movq (ilab (string_of_int n)) (reg rdi))
| TEstr s ->
	let dataLabel,dataContent = addDataString s in
	let alloc = allocateBlock "String" in
	{
		data = dataContent ;
		text = alloc ++ (movq (ilab dataLabel) (reg rcx)) ++
			(movq (reg rcx) (ind ~ofs:8 rax)) ++ (movq (reg rax) (reg rdi))
	}

| TEbool b ->
	prgmText (movq (ilab (if b then "1" else "0")) (reg rdi))
| TEnull ->
	prgmText (movq (ilab "0") (reg rdi))
| TEthis ->
	let ofs = SMap.find "this" env in
	prgmText ((movq (reg rbp) (reg rdx)) ++ (followLocation ofs) ++
		(movq (ind rdx) (reg rdi)))
| TEaccess acc ->
	(match acc with
	| TAccIdent idt ->
		let offset = (try SMap.find idt env
			with Not_found -> raise (InternalError ("Variable '"^idt^"' not \
				found in the current context."))
			) in
		{
			text = (movq (reg rbp) (reg rdx)) ++ (followLocation offset) ++
				(movq (ind rdx) (reg rdi));
			data = nop
		}
	| TAccMember(expr,idt) ->
		let metaDescr = SMap.find (fst expr.etyp) !metaDescriptors in
		let ofs = Offset(SMap.find idt metaDescr.vals, Here) in
		let compExpr = compileExpr expr env stackDepth in
		{
			text = compExpr.text ++ (movq (reg rdi) (reg rdx)) ++
				(followLocation ofs) ++ (movq (ind rdx) (reg rdi)) ;
			data = compExpr.data
		}
	)
| TEassign(acc, exp) ->
	let exprComp = compileExpr exp env stackDepth in
	(match acc with
	| TAccIdent idt ->
		let offset = (try SMap.find idt env
			with Not_found -> raise (InternalError ("Variable '"^idt^"' not \
				found in the current context."))
			) in
		{
			text = exprComp.text ++ (movq (reg rbp) (reg rdx)) ++
				(followLocation offset) ++ (movq (reg rdi) (ind rdx)) ++
				(movq (imm 0) (reg rdi));
			data = exprComp.data
		}
	| TAccMember(accExp,idt) ->
		let metaDescr = SMap.find (fst accExp.etyp) !metaDescriptors in
		let ofs = Offset(SMap.find idt metaDescr.vals, Here) in
		let compAcc = compileExpr accExp env stackDepth in
		{
			text = exprComp.text ++ (pushq (reg rdi)) ++
				compAcc.text ++ (movq (reg rdi) (reg rdx)) ++
				(followLocation ofs) ++ (popq rdi) ++
				(movq (reg rdi) (ind rdx)) ++ (movq (imm 0) (reg rdi)) ;
			data = exprComp.data ++ compAcc.data
		}
	)
| TEcall(acc, argt, params) ->
	let thisAddr, callComp, expData = (match acc with
		| TAccIdent(idt) ->
			(raise (InternalError "Call to a TAccIdent, which is supposed to \
				be impossible."))
		| TAccMember(exp,idt) ->
			let expComp = compileExpr exp env stackDepth in
			let typ = SMap.find (fst exp.etyp) !metaDescriptors in
			let methOffset = SMap.find idt (typ.methods) in
			(expComp.text ++ (pushq (reg rdi)),
				(movq (ind rdx) (reg rdx)) ++
				(addq (imm methOffset) (reg rdx)) ++ (call_star (ind rdx)),
				expComp.data)
		) in
	
	let nbPar = List.length params in
	let stackParams,dataParams = List.fold_left (fun (curSt, curDat) ex ->
			let exComp = compileExpr ex env stackDepth in
			curSt ++ exComp.text ++ (pushq (reg rdi)), curDat ++ exComp.data)
		(nop,nop) params in
	
	let unstackParams = (addq (imm (8*(List.length params + 1))) (reg rsp)) in

	{
		text = thisAddr ++ stackParams ++
			(movq (ind ~ofs:(8*nbPar) rsp) (reg rdx)) ++
			callComp ++ unstackParams ++ (movq (reg rdi) (reg rax));
		data = expData ++ dataParams
	}

| TEunop(UnaryNot, exp) ->
	let exprComp = compileExpr exp env stackDepth in
	{ exprComp with
		text = exprComp.text ++ (xorq (ilab "1") (reg rdi)) }
| TEunop(UnaryMinus, exp) ->	
	let exprComp = compileExpr exp env stackDepth in
	{ exprComp with
		text = exprComp.text ++ (imulq (imm (-1)) (reg rdi)) }
| TEbinop(op,exp1, exp2) ->
	let exprComp1 = compileExpr exp1 env stackDepth
	and exprComp2 = compileExpr exp2 env stackDepth in

	(* Allow the program to jump over the evaluation of the right operand if
	 it can be evaluated lazily *)
	let lazyLabel = (match op with
		| Land | Lor -> nextLazyLabel ()
		| _ -> "") in
	let lazyLabelAsm = if lazyLabel <> "" then (label lazyLabel) else nop in
	let lazyJump = (match op with
		| Land -> (testq (reg rdi) (reg rdi)) ++ (je lazyLabel)
		| Lor -> (testq (reg rdi) (reg rdi)) ++ (jne lazyLabel)
		| _ -> nop) in

	let preOp = {
		text = exprComp1.text ++
			lazyJump ++
			pushq (reg rdi) ++
			exprComp2.text ++ (* Result of exp2 is in rdi *)
			popq rax (* Result of exp1 is on rax *)
			;
		data = exprComp1.data ++ exprComp2.data
	} in

	let setQReg setter = (setter (reg al)) ++ (movzbq (reg al) rdi) in

	let opAction = (match op with
		(* Arithmetic *)
		| Plus -> addq (reg rax) (reg rdi)
		| Minus -> (subq (reg rdi) (reg rax)) ++ (movq (reg rax) (reg rdi))
		| Times -> imulq (reg rax) (reg rdi)
		| Div ->
			cqto ++ (idivq (reg rdi)) ++ (movq (reg rax) (reg rdi))
		| Mod ->
			cqto ++ (idivq (reg rdi)) ++ (movq (reg rdx) (reg rdi))
		(* Logical *)
		(* Use the setcc functions! *)
		| KwEqual -> (cmpq (reg rdi) (reg rax)) ++ (setQReg sete)
		| KwNEqual -> (cmpq (reg rdi) (reg rax)) ++ (setQReg setne)
		| Equal -> (cmpq (reg rdi) (reg rax)) ++ (setQReg sete)
		| NEqual -> (cmpq (reg rdi) (reg rax)) ++ (setQReg setne)
		| Less -> (cmpq (reg rdi) (reg rax)) ++ (setQReg setl)
		| Leq -> (cmpq (reg rdi) (reg rax)) ++ (setQReg setle)
		| Greater -> (cmpq (reg rdi) (reg rax)) ++ (setQReg setg)
		| Geq -> (cmpq (reg rdi) (reg rax)) ++ (setQReg setge)
		| Land -> andq (reg rax) (reg rdi)
		| Lor -> orq (reg rax) (reg rdi)
	) in

	{ preOp with text = preOp.text ++ opAction ++ lazyLabelAsm}

| TEif(cond,expIf,expElse) ->
	let labelStr = nextIfId () in
	let condComp = compileExpr cond env stackDepth in
	let ifComp = compileExpr expIf env stackDepth
	and elseComp = compileExpr expElse env stackDepth in
	let ifTxt = ifComp.text ++ (jmp (labelStr^"end"))
	and elseTxt = (label (labelStr^"else")) ++ elseComp.text in

	{
		text = condComp.text ++ (cmpq (ilab "0") (reg rdi)) ++ 
			(jz (labelStr^"else")) ++
			ifTxt ++ elseTxt ++ (label (labelStr^"end")) ;
		data = condComp.data ++ ifComp.data ++ elseComp.data
	}

| TEwhile(cond,code) ->
	let labelStr = nextWhileId () in
	let condComp = compileExpr cond env stackDepth
	and codeComp = compileExpr code env stackDepth in
	
	{
		text = (label (labelStr^"while")) ++ (condComp.text) ++
			(cmpq (ilab "0") (reg rdi)) ++ (jz (labelStr^"end")) ++
			codeComp.text ++ (jmp (labelStr^"while")) ++
			(label (labelStr^"end")) ++
			(movq (imm 0) (reg rdi));
		data = condComp.data ++ codeComp.data
	}

| TEreturn exp ->
	let codeComp = compileExpr exp env stackDepth in
	{
		text =
			(* Evaluate return value *)
			codeComp.text ++ (movq (reg rdi) (reg rax)) ++
			(* Restore stack *)
			(addq (imm stackDepth) (reg rsp)) ++ (popq rbp) ++
			(* Return *)
			ret ;
		data = codeComp.data
	}
