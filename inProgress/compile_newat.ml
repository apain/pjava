





open Format
open Ast
open Typed_ast
open X86_64
exception Compile_error of loc * string

type pos_env = int Smap.t
let string_nb = ref 0 
let string_data = ref nop
let generate_label () =
        incr string_nb;
        "str" ^ string_of_int !string_nb
let print_string msg_lab =
        movq !%rdi !%rsi ++
        movq (lab ("$"^msg_lab)) !%rdi ++
        movq (imm 0) !%rax ++
        call "printf"

let rec compile_expr e =
        (* -> asm.text, frame_size, next, pos_env *)
        (* should also add them as arguments *)
        match e.e_tdesc with
        | TEconstant c ->
                begin match c with
                | Cstring s -> let msg_lab = generate_label () in
                string_data := !string_data ++ label msg_lab ++ string s; 
                movq (lab ("$"^msg_lab)) !%rax ++
                pushq !%rax
                (*print_string msg_lab*)
                | Cint n ->
                movq (imm n) !%rax ++
                pushq !%rax 
                end
        | TEprint e1 -> compile_expr e1 ++
                popq rdi ++
                movq (imm 0) !%rax ++
                call "printf"
                (*compile_print e1 *)
        | TEconcatStringInt (Badd, e1, e2) ->
                let s1 = begin match e1.e_tdesc with
                | TEconstant (Cstring s) -> s
                | _ -> failwith "should not happen"
                end
                in let s2 = begin match e2.e_tdesc with
                | TEconstant (Cint n) -> string_of_int n
                | _ -> failwith "should not happen"
                end
                in 
                let s = s1 ^ s2 in
                compile_expr ({ e_tdesc = (TEconstant (Cstring s)); e_typ = Tstring })


        | TEmethodCall (a, e_list) -> 
                let nb_params = List.length e_list in
                List.fold_left (fun code e -> code ++ compile_expr e)
                nop
                e_list
                ++ call (find_method_id a)
                ++ addq (imm 8*nb_params) !%rsp (* restore stack *)
                ++ pushq !%rax
        | _ -> failwith "not yet implemented in compile_expr"
let compile_instr i =
        (* -> asm.text, frame_size (nb of local vars), next (first free pos on stack), pos_env (maps a var to its pos on stack) *)
        (* should also add them as arguments *)
        match i with
        | TIse e -> compile_expr e
        | _ -> failwith "not yet implemented in compile_instr"
type dataLocation =
	| Here							(* Right where we are pointing *)
	| FollowPtr of dataLocation		(* Follow the address of the pointer *)
	| Offset of int * dataLocation	(* Add an offset *)

type classMetaDescriptor = {
	memLoc : label ; (* Location in memory *)
	methods : int SMap.t ;(* maps a method name to its offset in the descriptor *)
	methLabels : label SMap.t ;(* maps a method name to its label in the assembly. Useful for inheritance. *)
	vals : int SMap.t ;(* maps a field name to its offset in an allocated object on the heap*)
	valsOrder : typVar (*?*) list ;(* Lists the fields in the order they must be allocated. *)
	clType : tcls
}
let metaDescriptors = ref SMap.empty
let nextStringId =
	let nextStringId = ref (-1) in
	(fun () ->
		nextStringId := !nextStringId + 1 ;
		("string"^(string_of_int !nextStringId)))
let nextLazyLabel =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; "lazyness"^(string_of_int !next))
let nextIfId =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; "L"^(string_of_int !next)^"_")
let nextWhileId =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; "L"^(string_of_int !next)^"_while")
let nextMethLabel =
	let next = ref (-1) in
	(fun () -> next := !next+1 ; (string_of_int !next))
(*** Inserts a string into the program data, return both its label and the assembly code.***)
let addDataString s =
	let dataLabel = nextStringId () in
	(dataLabel, (label dataLabel) ++ (string s))
(*** Returns a program with only a text field with value t***)
let prgmText t =
	{
		data = nop;
		text = t
	}
(*** Returns a code following a location from the address in %rdx and putting it in %rdx.***)
let rec followLocation = function
| Here -> nop
| FollowPtr(next) -> movq (ind rdx) (reg rdx) ++ (followLocation next)
| Offset (offset, next) -> addq (imm offset) (reg rdx) ++ (followLocation next)
(***
 * Returns assembly code that allocates a data block for an instantiation of
 * clName on the heap using sbrk. After this code is executed, the segment
 * start is found in %rax
 ***)
let block_allocation clName =
	let cl = SMap.find clName !metaDescriptors in
	let segment_size = (SMap.cardinal cl.vals + 1) * 8 in
        (movq (imm segment_size) (reg rdi)) ++ (call "sbrk") ++
		(movq (ilab cl.memLoc) (ind rax))

(*** Initializes the fields of a class, assuming that "this" pointer to the allocated block is in %rax and %rbp is correctly set.***)
let initialization_Of_fields idt env params stackDepth =
	let metaDescr = SMap.find idt !metaDescriptors in
	let rec evaluation_param cur = function
	| [],[] -> cur
	| fHead::fTail, curParam::ptail ->
			let compile_param = compile_expr curParam env stackDepth in
			let offset = SMap.find (fst fHead) metaDescr.vals in
			evaluation_param {
					text = cur.text ++ compile_param.text ++ (popq rdx) ++
						(movq (reg rdi) (ind ~ofs:offset rdx)) ++
						(pushq (reg rdx)) ;
					data = cur.data ++ compile_param.data
				} (fTail, ptail)
	| _,_ -> raise (InternalError ("Wrong number of parameters. Typer error!"))
	in
	(* To make the program behave as if `this' was the class we're building
	 in order to fill the objects' fields, we fake a function start on the
	 stack, then reference `this' as rbp-8 *)
	let chThis = (pushq (reg rbp)) ++ (movq (reg rsp) (reg rbp))
	and unchThis = (popq rbp) in
	let clEnv = SMap.singleton "this" (Offset(-8,Here)) in

	let initialization_fields = List.fold_left (fun curPrgm var ->
			let subcompil_expr = compile_expr var.vexpr clEnv stackDepth in
			let offset = SMap.find var.vname metaDescr.vals in
			{
				text = curPrgm.text ++ subcompil_expr.text ++ (pushq (reg rdi)) ++
					(movq (ind ~ofs:(-8) rbp) (reg rdx)) ++
					(popq rdi) ++ (movq (reg rdi) (ind ~ofs:offset rdx)) ;
				data = curPrgm.data ++ subcompil_expr.data
			}
		)
		{text = nop ; data = nop}
		metaDescr.valsOrder in (* Keeping the order might be necessary *)
	
	let subevaluate_params = evaluation_param {text=nop;data=nop}
		(metaDescr.clType.tcparams, params) in

	let inheritance_attempt = (match metaDescr.clType.tc_extends with
		| None -> raise (InternalError ("Class inheriting from no other! Typer error."))
		| Some ext ->
    (*We should modify here: instead of textType: 
    typClassExtends = {
  	textType : typ ;
	  tparam : typExpr list*)
			if (fst ext.textType) <> "Any" then begin
				initialization_Of_fields (fst ext.textType) clEnv (ext.tparam) stackDepth
			end 
      else
				{ text=nop; data=nop }
		)
	in

	{
		text =
			(* Evaluate parameters *)
			(pushq (reg rax)) ++ subevaluate_params.text ++ (popq rax) ++
			(* Inherit class initialization *)
			inheritance_attempt.text ++
			(* Initialize fields (other than parameters *)
			chThis ++ (pushq (reg rax)) ++ initialization_fields.text ++
			(popq rax) ++ unchThis ;
		data = subevaluate_params.data ++ inheritance_attempt.data ++ initialization_fields.data
	}


let buildMethod meth methLab env =
	let env = ref SMap.empty in
	List.iteri (fun i par -> env := SMap.add par (Offset (16+8*i, Here)) !env)
		(List.rev ("this"::(List.map fst meth.tmparams))) ;
	let codeComp = compile_expr meth.tmbody !env 0 in
	let nText = (label methLab) ++ (pushq (reg rbp)) ++
		(movq (reg rsp) (reg rbp)) ++ codeComp.text ++ (popq rbp) ++
		(movq (reg rdi) (reg rax)) ++ ret in
	{ codeComp with text = nText }


let buildClassDescriptor cl =
  (*We can use here the superclass find*)
	let superName = (match cl.textends with
		| Some c -> fst (c.textType)
		| None -> raise (InternalError "Class inheriting from no other! A \
			rogue typer might be on the loose!")) in
	let superMetaDescr = SMap.find superName !metaDescriptors in

	let env = SMap.empty in
	let varsLocs = let next = ref ((SMap.cardinal superMetaDescr.vals) * 8)
		in SMap.fold (fun name var prev ->
			(* Add fields as vars *)
			if not (SMap.mem name prev) then (
				next := !next + 8 ;
				SMap.add name !next prev
			) else
				prev
		) cl.tcvars
		(List.fold_left (fun prev cur -> (* Add class parameters as vars *)
				next := !next + 8 ;
				SMap.add (fst cur) !next prev)
			(superMetaDescr.vals) cl.tcparams) in

	(* Rebuild locations list from the inherited class *)
	let addrLabelsArray = Array.make
		((SMap.cardinal superMetaDescr.methLabels)+1) "" in
	SMap.iter (fun name pos ->
			let lab = SMap.find name superMetaDescr.methLabels in
			addrLabelsArray.(pos / 8) <- lab)
		superMetaDescr.methods ;
	let inheritedLocs = List.rev (List.tl (Array.to_list addrLabelsArray)) in

	(* Build maps *)
	let overrideMap = ref SMap.empty in
	let methLocs,methLabels,locs,_ = SMap.fold
		(fun name _ (prevLocs,prevLbl,prevLocsLst,i) ->
			let methLab = ("M"^(nextMethLabel ())^"_"^cl.tcname^"_"^name) in
			(try (* If overriding *)
				let overrideLbl = SMap.find name prevLbl in
				overrideMap := SMap.add overrideLbl methLab !overrideMap ;
				(prevLocs,
				SMap.add name methLab prevLbl,
				prevLocsLst,
				i)
			with Not_found -> (* Not overriding *)
				(SMap.add name i prevLocs,
				SMap.add name methLab prevLbl,
				methLab::prevLocsLst,
				i+8)
			)
		) cl.tc_declarations (superMetaDescr.methods,
			superMetaDescr.methLabels,
			[], (Array.length addrLabelsArray)*8) in
	let locs = locs @ inheritedLocs in
	
	(* Redefine locs replacing the overriden methods' labels *)
	let locs = List.map (fun lab ->
			(try SMap.find lab !overrideMap
			 with Not_found -> lab))
		locs in
			
	let descriptorLabel = "D_"^cl.tcname in
	let dataAdd = (label descriptorLabel) ++
		(address [superMetaDescr.memLoc]) ++
		(address (List.rev locs))
	in

	(* We **DO NOT** take into account inherited values, as the constructors
	 * will be called recursively and will initialized the inherited fields. *)
	let valsOrderList = List.rev (List.fold_left (fun cur elem ->
		match elem with
			| TDvar(tv) -> tv::cur
			| TDmeth(_) -> cur
		) [] cl.tcbody) in

	metaDescriptors := SMap.add cl.tcname {
			memLoc = descriptorLabel ;
			methods = methLocs ;
			methLabels = methLabels ;
			vals = varsLocs ;
			valsOrder = valsOrderList ;
			clType = cl
		} !metaDescriptors ;

	let compiled = SMap.fold (fun name code prevComp ->
			let methLabel = SMap.find name methLabels in
			let methComp = buildMethod code methLabel env in
			{
				text = prevComp.text ++ methComp.text;
				data = prevComp.data ++ methComp.data
			}
		)
		cl.tc_declarations ({text=nop; data=nop}) in
	
	{ compiled with data = compiled.data ++ dataAdd }

(***
 * Compiles a typed program
 ***)
let compileTypPrgm prgm =
	(* Adds the base classes the way we want them to be *)
	let mkBaseClass fields name extends =
		let dummyVar = false,("",EmptyAType) in
		{
			tcname = name ;
			tclassTypes = [];
			tcparams = [];
			textends = Some {textType = extends,EmptyAType ; tparam=[]};
			tcbody = [];
			tcvars = List.fold_left (fun cur n ->
				SMap.add n dummyVar cur) SMap.empty fields ;
			tc_declarations = SMap.empty ;
		} in
    (*I should put here our def of class*)
	let mkBaseClassNF = mkBaseClass [] in
(* Add a dummy metaDescriptor from Any, from which Any can inherit *)
	metaDescriptors := SMap.add "Any" {
		memLoc = "0" ;
		methods = SMap.empty ;
		methLabels = SMap.empty ;
		vals = SMap.empty ;
		valsOrder = [] ;
		clType = mkBaseClassNF "Any" "Any"} !metaDescriptors ;
	
	(* Really build the classes *)
	let buildClasses = [
			mkBaseClassNF "Any" "Any";
			mkBaseClassNF "AnyRef" "Any" ;
			mkBaseClass ["data"] "String" "AnyRef"
		] @ (prgm.tclasses) @ [prgm.tmain] in

	let descriptorsComp = List.fold_left (fun prev cur ->
			let comp = buildClassDescriptor cur in
			{ text = prev.text ++ comp.text ;
			  data = prev.data ++ comp.data })
		{text=nop;data=nop} buildClasses in

	let callMainComp = compile_expr
		{ nop
		}
		SMap.empty
		0 in

	{
		text = (glabel "main") ++
			callMainComp.text ++
			(xorq (reg rax) (reg rax)) ++ ret ++
			descriptorsComp.text;
		data = (label "printfIntFormat") ++ (string "%d") ++
			callMainComp.data ++
			descriptorsComp.data
	}








