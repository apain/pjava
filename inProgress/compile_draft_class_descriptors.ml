open Format
open Ast
open Typed_ast
open X86_64
open Utils

exception Compile_error of string

type pos_env = int Smap.t

(*type object_table = (ident, (string, string, int, int Smap.t)) Hashtbl.t*)
(*let object_table = Hashtbl.create 17*)
let field_table = Hashtbl.create 17
type class_meta_descriptor = {
	loc_in_mem : label ; (* Location in memory *)
	methods : int SMap.t ;
		(* maps a method name to its offset in the descriptor *)
	meth_labels : label SMap.t ;
		(* maps a method name to its label in the assembly. Useful for
		inheritance. *)
	fields : int SMap.t ;
		(* maps a field name to its offset in an allocated object on the heap*)
	fields_order : tdecl list ;
		(* Lists the fields in the order they must be allocated. *)
	clType : tcls
}
let meta_descriptors = ref SMap.empty
let string_data = ref (label ".Sprint_int" ++ string "%d")
let string_nb = ref 0 
let w_nb = ref 0 
let l_nb = ref 0
let a_nb = ref 0
let o_nb = ref 0

let generate_str_label () =
        incr string_nb;
        "str" ^ string_of_int !string_nb

let generate_str_int_label () = (* not permanent *)
        incr string_nb;
        ".str_int" ^ string_of_int !string_nb

let generate_w_label () =
        incr w_nb;
        "W" ^ string_of_int !w_nb

let generate_l_label () =
        incr l_nb;
        "L" ^ string_of_int !l_nb

let generate_a_label () =
        incr a_nb;
        "A" ^ string_of_int !a_nb

let generate_o_label () =
        incr o_nb;
        "O" ^ string_of_int !o_nb

let print_string msg_lab =
        movq !%rdi !%rsi ++
        movq (lab ("$"^msg_lab)) !%rdi ++
        movq (imm 0) !%rax ++
        call "printf"
(*
let find_method_id a = 
        match a.a_tdesc with
        | TAident id -> id
        | TAattr (_, id) -> id (* change later *)*)

let rec compile_expr pos_env e =
        (* -> asm.text *)
        match e.e_tdesc with
        | TEconstant c ->
                begin match c with
                | Cstring s -> let msg_lab = generate_str_label () in
                string_data := !string_data ++ label msg_lab ++ string s; 
                movq (lab ("$"^msg_lab)) !%rax ++
                pushq !%rax
                | Cint n ->
                movq (imm n) !%rax ++
                pushq !%rax 
                end 
        | TEbool b -> 
                let bit = begin match b with
                | false -> 0
                | true -> 1
                end in
                movq (imm bit) !%rax ++
                pushq !%rax
        | TEnew (nt, class_id, scl_id, e_list) -> 
                (* APPEL À CONSTRUCTEUR *)
                let constr_label = class_id ^ "_constr" in
                let code_args = List.fold_left (fun code e -> code ++ compile_expr pos_env e) nop e_list in
                let alloc_size = (List.length e_list + 1) * 8 in
                code_args ++
                movq (imm alloc_size) !%rdi ++
                call "malloc" ++
                pushq !%rax ++
                (* mettre l'adresse du descripteur de classe dans le premier champ *)
                call constr_label ++
                addq (imm alloc_size) !%rsp ++
                pushq !%rax
                        (*
                let field_map = begin try
                       ( begin match scl_id with
                | None -> Hashtbl.find field_table class_id
                | Some s_id -> Hashtbl.find field_table s_id 
                end ) with Not_found -> failwith "class not found in field_table" end 
                in 
                Smap.fold
                (fun field_id pos code -> code ++ movq !%rax (ind ~ofs:(pos) rbp))
                field_map
                movq !%rax !%rax
               *) 
                       
        | TEassign (a, e) -> (* modification de la valeur d'une variable locale *)
                begin match a.a_tdesc with
                | TAident id ->
                        begin try
                                let ofs = - (Smap.find id pos_env) in
                                compile_expr pos_env e ++
                                popq rax ++
                                movq !%rax (ind ~ofs rbp) ++
                                pushq !%rax 
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id)) end
                | TAattr (e, id) -> failwith "not implemented yet in compile_expr"
                end
        | TEaccess a -> (* accès à une variable locale *)
                begin match a.a_tdesc with
                | TAident id -> 
                        begin try
                                let ofs = - (Smap.find id pos_env) in
                                movq (ind ~ofs rbp) !%rax ++
                                pushq !%rax
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id)) end
                | TAattr (e, id) ->
                        let static_type = begin match e.e_typ with
                        | Tnew (c_id, _) -> c_id (* get superclass id *)
                        | _ -> failwith "should not happen here"
                        end in
                        begin try
                                let field_map = Hashtbl.find field_table static_type in
                                let ofs = Smap.find id field_map in
                                compile_expr pos_env e ++
                                popq rcx ++
                                movq (ind ~ofs rcx) !%rax
                        with Not_found -> failwith "field_map not found in compile_expr" end
                end
        | TEnull -> 
                movq (imm 0) !%rax ++
                pushq !%rax
        | TEunop (o, e) ->
                let op = begin match o with
                | Uneg -> negq
                | Unot -> notq
                end in
                compile_expr pos_env e ++ 
                popq rax ++
                op !%rax ++
                pushq !%rax
        | TEbinop (o, e1, e2) ->
                let exprs = 
                compile_expr pos_env e1 ++
                compile_expr pos_env e2 ++
                popq rbx ++
                popq rax 
                in
                begin match o with
                | Bdiv | Bmod -> 
                        let result = begin match o with
                        | Bdiv -> !%rax
                        | Bmod -> !%rdx 
                        | _ -> assert false
                        end in
                        exprs ++
                        movq (imm 0) !%rdx ++
                        idivq !%rbx ++
                        pushq result
                | Badd | Bsub | Bmul  ->
                        let op = begin match o with
                        | Badd -> addq
                        | Bsub -> subq
                        | Bmul -> imulq
                        | _ -> assert false
                        end in 
                        exprs ++
                        op !%rbx !%rax ++
                        pushq !%rax
                | Band ->
                        let a_lab1 = generate_a_label () in
                        let a_lab2 = generate_a_label () in
                        compile_expr pos_env e1 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jz a_lab1 ++
                        compile_expr pos_env e2 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jz a_lab1 ++
                        movq (imm 1) !%rax ++
                        pushq !%rax ++
                        jmp a_lab2 ++
                        label a_lab1 ++
                        pushq !%rax ++
                        label a_lab2
                | Bor -> 
                        let o_lab1 = generate_o_label () in
                        let o_lab2 = generate_o_label () in
                        compile_expr pos_env e1 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jnz o_lab1 ++
                        compile_expr pos_env e2 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jnz o_lab1 ++
                        movq (imm 0) !%rax ++
                        pushq !%rax ++
                        jmp o_lab2 ++
                        label o_lab1 ++
                        pushq !%rax ++
                        label o_lab2
                | Beq | Bneq | Blt | Ble | Bgt | Bge ->
                        let op = begin match o with
                        | Beq -> sete
                        | Bneq -> setne
                        | Blt -> setl
                        | Ble -> setle
                        | Bgt -> setg
                        | Bge -> setge
                        | _ -> assert false
                        end in
                        compile_expr pos_env e1 ++
                        compile_expr pos_env e2 ++
                        popq rbx ++
                        popq rcx ++
                        movq (imm 0) !%rax ++
                        cmpq !%rbx !%rcx ++
                        op !%al ++
                        pushq !%rax
                (*        exprs ++
                        cmpq !%rbx !%rax ++
                        op !%al ++
                        pushq !%rax*)
                end
          | TEconcat (Badd, e1, e2) ->
                compile_expr pos_env e1 ++
                compile_expr pos_env e2 ++
                popq rsi ++
                popq rdi ++
                call "concat" ++
                pushq !%rax
          | TEconcat (_, _, _) -> failwith "should not happen"
          | TEconcatIntString (Badd, e1, e2) ->
                compile_expr pos_env e1 ++
                compile_expr pos_env e2 ++
                popq rsi ++
                popq rdi ++
                call "concat_int_str" ++
                pushq !%rax
          | TEconcatIntString (_, _, _) -> failwith "should not happen"
          | TEconcatStringInt (Badd, e1, e2) ->
                compile_expr pos_env e1 ++
                compile_expr pos_env e2 ++
                popq rsi ++
                popq rdi ++
                call "concat_str_int" ++
                pushq !%rax
          | TEconcatStringInt (_, _, _) -> failwith "should not happen" 
(*
        | TEmethodCall (a, e_list) -> 
                        (* place arguments on top of stack
                           call method_label
                           dépiler les arguments *)
                let nb_params = List.length e_list in
                List.fold_left (fun code e -> code ++ compile_expr e)
                nop
                e_list
                ++ call (find_method_id a)
                ++ addq (imm 8*nb_params) !%rsp (* restore stack *)
                ++ pushq !%rax*)
        | TEprint e1 -> compile_expr pos_env e1 ++
                popq rdi ++
                movq (imm 0) !%rax ++
                call "printf"
        | _ -> failwith "not yet implemented in compile_expr"

let rec compile_instr code pos_env next i =
        (* -> asm.text, pos_env (maps a var to its pos on stack), next (first free pos on stack) *)
        match i with
        | TIvoid -> code, pos_env, next
        | TIse e -> code ++ compile_expr pos_env e, pos_env, next
        | TIassign (a, e) ->
                begin match a.a_tdesc with
                | TAident id ->
                        begin try
                                let ofs = - (Smap.find id pos_env) in
                                code ++
                                compile_expr pos_env e ++
                                popq rax ++
                                movq !%rax (ind ~ofs rbp)
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id)) end
                | TAattr (e, id) -> failwith "not implemented yet in compile_expr"
                end
                , pos_env, next
        | TIdeclare (_, id) -> code, (Smap.add id next pos_env), (next + 8) 
        | TIdeclAssign (_, id, e) -> 
                code ++ 
                compile_expr pos_env e ++
                popq rax ++
                movq !%rax (ind ~ofs:(-next) rbp),
                (Smap.add id next pos_env), (next + 8)
        | TIif (e, i) ->
                let l_lab = generate_l_label () in
                let code_if, _, _ = compile_instr nop pos_env next i in
                code ++ 
                compile_expr pos_env e ++
                popq rax ++
                testq !%rax !%rax ++
                jz l_lab ++  (* if rax == 0: if false *)
                code_if ++
                label l_lab,
                pos_env, next
        | TIwhile (e, i) ->
                let w_lab = generate_w_label () in
                let l_lab = generate_l_label () in
                let code_while, _, _ = compile_instr nop pos_env next i in
                (*code ++
                label w_lab ++
                compile_expr pos_env e ++
                popq rax ++
                testq !%rax !%rax ++
                jz l_lab ++
                code_while ++
                jmp w_lab ++
                label l_lab *)
                code ++
                jmp l_lab ++
                label w_lab ++
                code_while ++
                label l_lab ++
                compile_expr pos_env e ++
                popq rax ++
                testq !%rax !%rax ++
                jnz w_lab (* if rax == 1: if true *),
                pos_env, next
        | TIblock (i_list) -> 
                let code_block, _, _ = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env next i)
                (nop, pos_env, 0)
                i_list
                in 
                code ++
                code_block,
                pos_env, next
        | _ -> failwith "not yet implemented in compile_instr"

let compile_main tclass_main =
        let _, i_list = tclass_main in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env next i) 
        (nop, Smap.empty, 0)
        i_list
        in
        subq (imm frame_size) !%rsp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        addq (imm frame_size) !%rsp ++
        movq (imm 0) !%rax

        (*
        (* pushq rbp (sauvegarde ancien rbp)
         movq rsp rbp (le sommet de la pile au moment de l'appel devient le fond de la frame de l'appelé)
         subq (imm frame_size) rsp (alloue son tableau d'activation = 8 * nombre de variables locales de la fonction) 
         on accède aux variables locales de la fonction par rapport à rbp 
         --code de la fonction--
         résultat dans rax
         leave (dépile tableau d'activation et restaure rbp)
         ret
         *)

let compile_method pos_env m =
        let p = m.tm_proto in 
        let lab = begin match p with
        | Pproto (_, id, _) -> id
        | PprotoVoid (id, _) -> id
        in 
        let m_env = m.tm_env in
        let formals = m_env.formals in
        let pos_env, _ = List.fold_left (fun (env, i) (_, p_id) ->
                (Smap.add p_id 16+8*i env, i+1) (*?*)
        )
        (pos_env, 1)
        formals
        in
        let compiled = List.fold_left (fun code i -> code ++ compile_instr i) 
        nop
        m.tm_instr
        in 
        let code =
        (label lab) ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        compiled ++
        popq rax ++
        popq rbp ++
        ret
        in code, pos_env *)

let compile_constructor constr =
        let class_id, formals, i_list, _ = constr in
        let field_map = begin try Hashtbl.find field_table class_id with Not_found -> failwith "class not found" (* get superclass id *) end
        in
        let pos_env, _ = 
        List.fold_left (fun (pos_env, ofs) (_, arg) ->
                (Smap.add arg ofs pos_env, ofs - 8))
        (field_map, (List.length formals + 1) * 8)
        formals
        in 
        let pos_env = Smap.add "this" 8 pos_env
        in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env next i) 
        (nop, pos_env, 0)
        i_list
        in
        label (class_id^"_constr") ++
        subq (imm frame_size) !%rsp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        movq (ind ~ofs:8 rbp) !%rax ++
        addq (imm frame_size) !%rsp ++
        ret



(*
(*        let descr_label, constr_label, nb_fields, field_map = Hashtbl.find object_table class_id in*)
        let field_values = List.fold_left (fun code field -> 
                let ofs = Smap.find field field_map in 
                code ++
                popq !%rax ++
                movq !%rax (ind ~ofs rdi)
        )
        nop
        fields
        in
        (label constr_label) ++
        field_values ++
        ret 

        let pos_env, _ = List.fold_left (fun (env, i) (_, p_id) ->
                (Smap.add p_id 16+8*i env, i+1) (*?*)
        )
        (pos_env, 1)
        formals
        in
        let compiled = List.fold_left (fun code i -> code ++ compile_instr i) 
        nop
        i_list
        in 
        let code =
        (label lab) ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        compiled ++
        popq rax ++
        popq rbp ++
        ret
        in code, pos_env
*)
let compile_decl d =
        match d with
        | TDdecl (_, id) -> nop
        | TDconstructor c -> compile_constructor c
        | TDmethod m -> nop 

let compile_class c =
        let decls = c.tc_declarations in
        List.fold_left (fun code d -> code ++ compile_decl d) 
        nop
        decls

let compile_classes cls_intf_list =
        List.fold_left (fun code c_i -> 
               begin match c_i with
               | TCClass c -> code ++ compile_class c
               | TCInterface i -> code
               end) 
        nop
        cls_intf_list
(*
let get_superclass full_env c_id =
        (* Some c_id | None *)
        let c_env = Smap.find c_id full_env in
        let scl = c_env.extends in
        begin match scl with
        | None -> None
        | Some (Nntype (c, _)) -> Some c
        end

let fill_field_table full_env field_table c_id = 
        let c_env = Smap.find c_id full_env in
        let fields = c_env.fields in
        let field_map, _ =
        Smap.fold
        (fun (env, i) field -> Smap.add field 16+8*i env, i+1)
        (Smap.empty, 1)
        fields
        in
        Hashtbl.add field_table c_id field_map

let make_class_descriptor class_table c_id =
    (*Check that the class is inheriting*)
        let super_name = c_env.extends in
        begin match super_name with
        | None -> None
        | Some (Nntype (c, _)) -> Some c
        end in
	    let super_meta_descr = SMap.find super_name !_d in
        let env = SMap.empty in
        (*I don't think that we need to include declarations, *)
        (*it would be necessary to include local variables but I cannot find exp. them in the AST*)
	    let declarations_locs = let next = ref ((SMap.cardinal super_meta_descr.fields) * 8)
		in SMap.fold (fun name var prev ->
			if not (SMap.mem name prev) then (
				next := !next + 8 ;
				SMap.add name !next prev
			) else
				prev
		) cl.tc_declarations
		(List.fold_left (fun prev cur -> (* Add class parameters as dec *)
				next := !next + 8 ;
				SMap.add (fst cur) !next prev)
			(super_meta_descr.fields) cl.tc_params) in
	(* Rebuild locations list from the inherited class *)
	let addr_labels_array = Array.make
		((SMap.cardinal super_meta_descr._)+1) "" in
	SMap.iter (fun name pos ->
			let lab = SMap.find name super_meta_descr._ in
			addr_labels_array.(pos / 8) <- lab)
		super_meta_descr.methods ;
	let inherited_locs = List.rev (List.tl (Array.to_list addr_labels_array)) in
	(* Build maps *)
	let over_ride_map = ref SMap.empty in
	let meth_locs,meth_labels,locs,_ = SMap.fold
		(fun name _ (prev_locs,prev_lbl,prev_locs_lst,i) ->
			let meth_lab = ("M"^(next_meth_label ())^"_"^cl.tc_name^"_"^name) in
			(try (* If overriding *)
				let overide_lbl = SMap.find name prev_lbl in
				over_ride_map := SMap.add overide_lbl meth_lab !over_ride_map ;
				(prev_locs,
				SMap.add name meth_lab prev_lbl,
				prev_locs_lst,
				i)
			with Not_found -> (* Not overriding *)
				(SMap.add name i prev_locs,
				SMap.add name meth_lab prev_lbl,
				meth_lab::prev_locs_lst,
				i+8)
			)
		) cl.tc_declarations (super_meta_descr.methods,
			super_meta_descr._,
			[], (Array.length addr_labels_array)*8) in
	let locs = locs @ inherited_locs in
	(* Redefine locs replacing the overriden methods' labels *)
	let locs = List.map (fun lab ->
			(try SMap.find lab !over_ride_map
			 with Not_found -> lab))
		locs in
	let descriptr_label = "D_"^cl.tc_name in
	let data_add = (label descriptr_label) ++
		(address [super_meta_descr.loc_in_mem]) ++
		(address (List.rev locs))
	in

	(* We **DO NOT** take into account inherited values, as the constructors
	 * will be called recursively and will initialized the inherited fields. *)
	let fields_order_list = List.rev (List.fold_left (fun cur elem ->
		match elem with
			| TDvar(tv) -> tv::cur
			| TDmeth(_) -> cur
		) [] cl.tc_body) in

	_d := SMap.add cl.tc_name {
			loc_in_mem = descriptr_label ;
			methods = methLocs ;
			_ = _ ;
			vals = varsLocs ;
			valsOrder = fields_order_list ;
			clType = cl
		} !_d ;

	let compiled = SMap.fold (fun name code prevComp ->
			let meth_label = SMap.find name _ in
			let methComp = buildMethod code meth_label env in
			{
				text = prevComp.text ++ methComp.text;
				data = prevComp.data ++ methComp.data
			}
		)
		cl.tcmeth ({text=nop; data=nop}) in
	
	{ compiled with data = compiled.data ++ data_add }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



let make_class_descriptors full_env cls_intf_list =
        
        let class_table = Hashtbl.create 17 in
        let field_table = Hashtbl.create 17 in
        
        let rec make_class c_id =
                if Hashtbl.mem class_table c_id then ()
                else begin match get_superclass full_env c_id with
                | None -> make_class_descriptor class_table c_id
                | Some scl -> make_class scl; make_class_descriptor class_table c_id; fill_field_table field_table c_id
        in List.iter (fun c -> make_class c) cls_intf_list
        *)


let make_field_map class_id field_list =
        let field_map, _ =
        List.fold_left (fun (env, pos) field ->
                (Smap.add field pos env, pos + 8)) 
        (Smap.empty, 8)
        field_list
        in field_map

let make_field_table cls_intf_list full_env =
        List.iter
        (fun cls_intf ->
                begin match cls_intf with
                | TCClass cls ->
                        let sclass_env = cls.tc_scl in
                        let sclass_id = sclass_env.name in
                        if Hashtbl.mem field_table sclass_id then ()
                        else let field_list = sclass_env.field_list in
                        let field_map = make_field_map sclass_id field_list in
                        Hashtbl.add field_table sclass_id field_map
                | TCInterface _ -> ()
                end 
        )
        cls_intf_list        
        
(* main function *)
let compile_file typed_ast out_file = 
        (* takes as input the output of the main typer function = typed ast *)

        let cls_intf_list = typed_ast.tf_cls_intf_list in
        let class_main = typed_ast.tf_class_main in
        let full_env = typed_ast.tf_full_env in
        make_field_table cls_intf_list full_env;

(*        make_class_descriptors full_env cls_intf_list; *)

        let class_code = compile_classes cls_intf_list in
        
        let main_code = compile_main class_main in

        let p =
        {
	        text =
                        globl "main" ++
                        label "main" ++
                        main_code ++
                        ret ++
                        class_code ++
                        concat_code ++
                        concat_int_str_code ++
                        concat_str_int_code
        ;
                data = 
                        !string_data ++
                        nop
	}

        in
        let f = open_out out_file in
        let fmt = formatter_of_out_channel f in
        print_program fmt p;
        fprintf fmt "@?";
        close_out f
