open Ast
(*Introduction of types*)
type typ =
    | Tvoid
    | Tboolean
    | Tint
    | Ttypenull
    | Tany
    | Tparameter of typ (*Not sure at all*)
    | Tclass of typ list
    | Tinterface of typ list 
(*Typing Environment*)
module SM = StringMap
module S = StringSet
type method_type = typ list * typ (** Parameters types and return type of a method. *)
type method_env = method_type SM.t
type attribute_env = typ SM.t
type class_type = attribute_env * method_env
type class_env = class_type SM.t
type variable_env = typ SM.t
(*Set of parameters of types, classes and interfaces*)
(*Set of the relations extends*)
(*Set of the relations implements*)
(*Set of the declarations: this*)
(*Set of the declarations of variables of form x:tau*)
(*Predefined classes: Object*)
(*No inheritance*)
(*Every class inherits from it*)
(*Predefined classes: String*)
(*inheritance from the class Object*)
(*well formed types*)
