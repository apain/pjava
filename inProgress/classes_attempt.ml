(*open Ast*)
(*** AST (to be removed) ***)

type ident = string (*Identificators*)

type void = TVoid

type unop =(*Unary operators*)
  | Uneg (*-e*)
  | Unot (*!e*)

type binop = (*Binary operators*)
  | Badd | Bsub | Bmul | Bdiv | Bmod    (* + - * / % *)
  | Beq | Bneq | Blt | Ble | Bgt | Bge  (* == != < <= > >= *)
  | Band | Bor                          (* && || *)

(*I think that normally here are defined the constants*)

type file = (*File*)
  Ffile of class_interface list * class_main

and class_interface =
  | CClass of cls
  | CInterface of interface

and cls = {
  c_name: ident;
  c_params: paramstype option;
  c_extends: ntype option;
  c_implements: ntype list option;
  c_declarations: decl list }

and interface = {
  i_name: ident;
  i_params: paramstype option;
  i_extends: ntype list option;
  i_protocols: proto list }

and paramstype =
  Pparamstype of paramtype list

and paramtype =
  Pparamtype of ident * ntype list option

and decl =
  | Ddecl of typ * ident
  | Dconstructor of constructor
  | Dmethod of mtd

and constructor =
  Cconstr of ident * parameter list * instruction list

and mtd =
  Mmtd of proto * instruction list

and proto =
  | Pproto of typ * ident * parameter list
  | PprotoVoid of ident * parameter list

and parameter =
  Pparam of typ * ident

and typ =
  | Ttbool
  | Ttint
  | Ttntype of ntype

and ntype =
  | Nntype of ident * ntype list option
  | NntypeString

and class_main =
  CClassMain of ident * instruction list

and expr =
 | SEconstant of constant
(*  | Cstring of string
  | Cint of int*)
  | SEbool of bool
  | SEthis
  | SEnew of ntype * expr list
  | SEaccessList of access * expr list
  | SEaccess of access
  | Enull
  | EaccessExpr of access * expr
  | Eunop of unop * expr
  | Ebinop of binop * expr * expr
  | Eprint of expr

and access =
  | Aident of ident (** Get the value of a variable. *)
  | Aattr of expr * ident

and constant =
  | Cstring of string
  | Cint of int

and instruction =
  | Ivoid
  | Ise of expr
  | Iassign of access * expr
  | Ideclare of typ * ident
  | IassignTyp of typ * ident * expr
  | Iif of expr * instruction
  | IifElse of expr * instruction * instruction
  | Iwhile of expr * instruction
  | Iblock of instruction list
  | Ireturn of expr option

(* =========== *)

exception Typing_error of string

type pj_typ = 
        | Tvoid
        | Tboolean
        | Tint
        | Tstring (* allowed? *)
        | Ttypenull
        | Tobject of ntype

(* == TYPED AST == *)

type texpr = {
        e_tdesc: e_tdesc;
        e_typ: pj_typ;
        }

and taccess = {
        a_tdesc: a_tdesc;
        a_typ: pj_typ;
        }

and e_tdesc =
  | TSEconstant of constant
  | TSEbool of bool
  | TSEthis
  | TSEnew of ntype * texpr list
  | TSEaccessList of taccess * texpr list
  | TSEaccess of taccess
  | TEnull
  | TEaccess of access * texpr
  | TEunop of unop * texpr
  | TEbinop of binop * texpr * texpr
  | TEprint of texpr

and a_tdesc =
  | TAident of ident 
  | TAattr of texpr * ident

(* === *)

module Smap = Map.Make(String)
module Imap = Map.Make(ident)

type env = { vars : pj_typ Smap.t ; extends : ident Imap.t ; implements : pj_typ Tmap.t } (* or ident map ? *)

let empty_env = { vars = Smap.empty }

let get_typ t = match t with
        | Ttbool -> Tboolean
        | Ttint -> Tint
        | Ttntype nt -> Tobject nt

let rec compatible tau1 tau2 =
        (* returns true if types tau1 and tau2 are compatible *)
        true

let rec is_left_val v = 
        (* return true if v is a left value *)
        ()

let rec type_expr env e = 
        (* type_expr: env expr -> texpr *)
        let d, tau = compute_type_expr env e in
        { e_tdesc = d; e_typ = tau }

and type_access env a e =
        let d, tau = compute_type_access env a e in
        { a_tdesc = d; a_typ = tau }

and compute_type_access env a e =
        (* compute_type_access: env expr -> (a_tdesc, pj_typ) *)
        begin match a with
        | Aident id -> TAident id, Smap.find id env.vars (* is a left value *)
        | Aattr (e, id) -> TAattr (type_expr env e, id), Smap.find id env.vars (* need to modify this *)
        end     

and compute_type_expr env e =
        (* compute_type_expr: env expr -> (e_tdesc, pj_typ) *)
        match e with
        | Enull -> TEnull, Ttypenull
        | SEconstant c -> begin match c with
                | Cint n -> TSEconstant (Cint n), Tint
                | Cstring s -> TSEconstant (Cstring s), Tstring
                end 
        | SEbool b -> TSEbool b, Tboolean
                (* | this ?? *)
        | EaccessExpr (a, e) -> let (a_t, tau) = compute_type_access env a e in TSEaccess ( { a_tdesc = a_t; a_typ = tau } ), tau
        | Eunop (unop, e1) -> begin match unop with
                | Uneg when (type_expr env e1).e_typ = Tint -> TEunop (unop, type_expr env e1), Tint
                | Unot when (type_expr env e1).e_typ = Tboolean -> TEunop (unop, type_expr env e1), Tboolean
                | _ -> raise (Typing_error ("unsupported type for that operand"))
                end
        | Ebinop (binop, e1, e2) -> begin match binop with
                | Beq | Bneq when compatible (type_expr env e1) (type_expr env e2) -> TEbinop (binop, type_expr env e1, type_expr env e2), Tboolean
                | Blt | Bgt | Bge when (type_expr env e1).e_typ = Tint && (type_expr env e2).e_typ = Tint -> TEbinop (binop, type_expr env e1, type_expr env e2), Tboolean
                | Badd | Bsub | Bmul | Bdiv | Bmod when (type_expr env e1).e_typ = Tint && (type_expr env e2).e_typ = Tint -> TEbinop (binop, type_expr env e1, type_expr env e2), Tint
                | Band | Bor when (type_expr env e1).e_typ = Tboolean && (type_expr env e2).e_typ = Tboolean -> TEbinop (binop, type_expr env e1, type_expr env e2), Tboolean                
                | Badd when (type_expr env e1).e_typ = Tstring && ((type_expr env e2).e_typ = Tint || (type_expr env e2).e_typ = Tstring) -> TEbinop (binop, type_expr env e1, type_expr env e2), Tstring
                | Badd when ((type_expr env e1).e_typ = Tint || (type_expr env e1).e_typ = Tstring) && (type_expr env e2).e_typ = Tstring -> TEbinop (binop, type_expr env e1, type_expr env e2), Tstring
                | _ -> raise (Typing_error ("unsupported type for that operand"))
                end
        | Eprint e when (type_expr env e).e_typ = Tstring -> TEprint (type_expr env e), Tvoid
        | _ -> raise ((Typing_error "unrecognized expression"))
        (* SEthis, SEnew, SEaccessList, SEaccess *)
                
let rec type_instr env i = 
        (* returns new environment *)
        match i with
        | IassignTyp (t, id, _) -> let tau = get_typ t in { vars = Smap.add id tau env.vars }
        | Ideclare (t, id) -> let tau = get_typ t in { vars = Smap.add id tau env.vars }
        | _ -> env

(*Method*)
let add_param env param =
        (* env, parameter -> env *)
        let (t, id) = param in
        Env.add id t env

let extend_env_mtd env param_list =
        (* env, parameter list -> env *)
        List.fold_left (fun env param -> add_param env param) 
        env 
        param_list

(*let check_return_type mtd =
        let (p, instr) = mtd in*)
        

(*Class*)
let type_cls env cls =

        let extend_env_cls env cls =
                (* env, cls -> env *)
                match cls.c_params with
                | None -> env
                | Some paramstype -> let p_list = paramstype in
                List.fold_left (fun env p -> Env.add cls.c_name p env)
                env
                p_list

        let build_dependencies env cls =

                let dep_params env cls =

                let dep_extends env cls =
                        (* -> env *)
                        match cls.c_extends with
                        | None -> env
                        | Some nt -> let (nt_id, _) = nt in        
                                Env.add cls.c_name nt_id env

                let dep_implements env cls =
                        (* -> env *)
                        match cls.c_implements with
                        | None -> env
                        | Some nt_list -> 
                                        List.fold_left
                                        (let (nt_id, _) = nt in Env.add cls.c_name nt_id env)
                                        env
                                        nt_list

let type_cls_intf cls_intf_list =
        iteration 
        | CClass c -> type_cls c
        | CInterface i -> type_intf i

let type_file file =
        let (cls_intf_list, class_main) = file in
        (type_cls_intf cls_intf_list,
        type_class_main class_main)

        

 add_var var_list env = match var_list with
                                  | [] -> ()
                                  | (var_type, id, None)::t -> Hashtbl.add env id (var_type,None); add_var t env
                                  | (var_type, id, Some(exp))::t -> begin 
                                                                    Hashtbl.add env id (var_type,Some(exp));
                                                                    exp_check exp env;
                                                                    if (get_exp_type exp = var_type) then () else Error.assign_incompatible_types var_type exp
                                                                    add_var t env;
                                                                    end
   and add_var_for var_list env = match var_list with
                                  | [] -> ()
                                  | (Some(var_type), id, None)::t -> Hashtbl.add env id (var_type,None); add_var_for t env
                                  | (Some(var_type), id, Some(exp))::t -> exp_check exp env; Hashtbl.add env id (var_type,Some(exp)); add_var_for t env
                                  | (None, id, None)::t -> if Hashtbl.mem env id then () else Error.unknown_variable id method_ast.mloc
                                  | (None, id, Some(exp))::t -> if Hashtbl.mem env id then begin let var_type, _ = Hashtbl.find   env id in exp_check exp env; Hashtbl.remove env id; Hashtbl.add env id (var_type,Some(exp)) end else Error.unknown_variable id method_ast.mloc
   and exp_list_check exp_list env = match exp_list with [] -> () | h::t -> exp_check h env; exp_list_check t env
   in match statement with
    (* All good *)
    | Nop                  -> ()
    | Expr(exp)            -> exp_check exp var_env
    | Block(sl)            -> list_check sl (Hashtbl.copy var_env) (* New block = new scope for variables ! *)
    | VarDecl(var)         -> add_var var var_env
    | Return(Some(exp))    -> exp_check exp var_env; begin match exp.etype with Some(t) when t = method_ast.mreturntype -> () | _ -> (Error.wrong_return method_ast) end
    | Return(None)         -> begin match method_ast.mreturntype with Void -> () | _ -> (Error.wrong_return method_ast) end 
    (* Throw - Only check if ref_type is inside the throw block, not that it is actually an exception *)
    | Throw(exp)           -> exp_check exp var_env; begin match exp.etype with Some(Ref(_)) -> () | _ -> (Error.wrong_throw method_ast) end
    | While(cond,s)        -> exp_check cond var_env; iter s var_env
    | If(cond,s1,Some(s2)) -> exp_check cond var_env; iter s1 var_env; iter s2 var_env; check_boolean_exp cond;
    | If(cond,s,None)      -> exp_check cond var_env; iter s var_env; check_boolean_exp cond;
    | For(var,Some(cond),exp_list,s) -> let for_env = (Hashtbl.copy var_env) in add_var_for var for_env; exp_check cond for_env; exp_list_check exp_list for_env; iter s for_env
    | For(var,None,exp_list,s)       -> let for_env = (Hashtbl.copy var_env) in add_var_for var for_env; exp_list_check exp_list for_env; iter s for_env
    (* END - All good *)
 (* TODO *)
(*Class*)

  let execute method_table object_descriptor_table =
  let rec class_typing exp classname var_env =
  let rec iter exp = class_typing exp classname var_env in
  let t=match exp.edesc with
   | Name(id)                -> let attr_env = (Env.find object_descriptor_table classname) in
                                 begin match id with
                                  | this when id = "this" -> Some(Ref(Type.mk_type [] classname))
                                  | var when Hashtbl.mem var_env id = true -> let var_type, exp = Hashtbl.find var_env id in Some(var_type)
                                  | attr when Env.mem attr_env id = true -> let attr = Env.find attr_env id in Some(attr.atype)
                                  | _ -> Error.unknown_variable id exp.eloc end
    | Attr(e,id)              -> iter e; let exp_type = get_exp_type e in
                                 let attr_env = (Env.find object_descriptor_table (Type.stringOf exp_type)) in 
                                 begin match id with
                                  | attr when Env.mem attr_env id = true -> let attr = Env.find attr_env id in Some(attr.atype)
                                  | _ -> Error.unknown_attribute id exp.eloc end
    | New(None,p,_)           -> begin match p with
                                  | class_ref when Env.mem object_descriptor_table (String.concat "." p) = true -> Some(Ref(Type.extract_type p))
                                  | _ -> Error.unknown_class (String.concat "." p) exp.eloc end
    | Val(v)                  -> val_typing v
    | CondOp(c,e1,e2)         -> iter e1; iter e2; iter c; check_boolean_exp c; begin match e1.etype with exp_type when exp_type = e2.etype -> exp_type | _ -> Error.type_mismatch e1 e2 end
    | Op(e1,op,e2)            -> iter e1; iter e2; infix_typing e1 op e2
    | AssignExp(e1,op,e2)     -> iter e1; iter e2; assign_typing e1 op e2
    | Type(tp)                -> Some(tp)
    | ClassOf(tp)             -> Some(tp)
    | Instanceof(e,t)         -> iter e; Some(Primitive(Boolean))
    | VoidClass               -> Some(Void)

  and statement_check statement method_ast classname var_env =
   let exp_check exp env = exp_typing exp classname env
   and iter other env = statement_check other method_ast classname env
   in let rec list_check s_list env = match s_list with [] -> () | h::t -> statement_check h method_ast classname env; list_check t env
   and add_var var_list env = match var_list with
                                  | [] -> ()
                                  | (var_type, id, None)::t -> Hashtbl.add env id (var_type,None); add_var t env
                                  | (var_type, id, Some(exp))::t -> begin 
                                                                    Hashtbl.add env id (var_type,Some(exp));
                                                                    exp_check exp env;
                                                                    if (get_exp_type exp = var_type) then () else Error.assign_incompatible_types var_type exp
                                                                    add_var t env;
                                                                    end
   and add_var_for var_list env = match var_list with
                                  | [] -> ()
                                  | (Some(var_type), id, None)::t -> Hashtbl.add env id (var_type,None); add_var_for t env
                                  | (Some(var_type), id, Some(exp))::t -> exp_check exp env; Hashtbl.add env id (var_type,Some(exp)); add_var_for t env
                                  | (None, id, None)::t -> if Hashtbl.mem env id then () else Error.unknown_variable id method_ast.mloc
                                  | (None, id, Some(exp))::t -> if Hashtbl.mem env id then begin let var_type, _ = Hashtbl.find   env id in exp_check exp env; Hashtbl.remove env id; Hashtbl.add env id (var_type,Some(exp)) end else Error.unknown_variable id method_ast.mloc
   and exp_list_check exp_list env = match exp_list with [] -> () | h::t -> exp_check h env; exp_list_check t env
   in match statement with
    (* All good *)
    | Nop                  -> ()
    | Expr(exp)            -> exp_check exp var_env
    | Block(sl)            -> list_check sl (Hashtbl.copy var_env) (* New block = new scope for variables ! *)
    | VarDecl(var)         -> add_var var var_env
    | Return(Some(exp))    -> exp_check exp var_env; begin match exp.etype with Some(t) when t = method_ast.mreturntype -> () | _ -> (Error.wrong_return method_ast) end
    | Return(None)         -> begin match method_ast.mreturntype with Void -> () | _ -> (Error.wrong_return method_ast) end 
    (* Throw - Only check if ref_type is inside the throw block, not that it is actually an exception *)
    | Throw(exp)           -> exp_check exp var_env; begin match exp.etype with Some(Ref(_)) -> () | _ -> (Error.wrong_throw method_ast) end
    | While(cond,s)        -> exp_check cond var_env; iter s var_env
    | If(cond,s1,Some(s2)) -> exp_check cond var_env; iter s1 var_env; iter s2 var_env; check_boolean_exp cond;
    | If(cond,s,None)      -> exp_check cond var_env; iter s var_env; check_boolean_exp cond;
    | For(var,Some(cond),exp_list,s) -> let for_env = (Hashtbl.copy var_env) in add_var_for var for_env; exp_check cond for_env; exp_list_check exp_list for_env; iter s for_env
    | For(var,None,exp_list,s)       -> let for_env = (Hashtbl.copy var_env) in add_var_for var for_env; exp_list_check exp_list for_env; iter s for_env
    (* END - All good *)
 (* TODO *)
    | New(Some o,p,_)       -> None    (* Not implemented syntax for inner classes ... *)
    | If(c,e1,e2)           -> None (* What is this ? *)
    | Cast(t,e)             -> iter e; begin match e.etype with (* Incomplete - Only cast the type are the same *)
                                  | Some(t) -> Some(t)
                                  | _ -> Error.malformed_expression e end
    | NewArray(t,l,Some(e)) -> None
    | NewArray(t,l,None)    -> None
    | ArrayInit(l)          -> None
    | Array(e,el)           -> None
    (* END - TODO *)
  in exp.etype <- t
    (* TODO *)
    | Try(sl1,l,sl2)       -> list_check sl1 (Hashtbl.copy var_env); list_check sl2 (Hashtbl.copy var_env)
    (* END - TODO *)
    
    (* END - Statement typing *)

  (* Entry point *)
  in let body_check method_ast classname var_env =
    let rec iter = function
      | [] -> ()
      | s::others -> statement_check s method_ast classname var_env; iter others
    in iter method_ast.mbody
  in let iter_method_table (id,method_ast) =
       let classname = List.hd (Str.split_delim (Str.regexp "_") id)
       and var_env = (Hashtbl.create 4 : (string, Type.t * AST.expression option) Hashtbl.t)
       in body_check method_ast classname var_env
  in Env.iter iter_method_table method_table;
