open Format
open Lexing
open Parser
open Compile

let usage = "usage: pjava [options] file.java"

let parse_only = ref false
let type_only = ref false

let out_file = ref ""

let spec =
  [
    "--parse-only", Arg.Set parse_only, "  stop after parsing";
    "--type-only", Arg.Set type_only, " stop after typing";
  ]

let file =
  let file = ref None in
  let set_file s =
    if not (Filename.check_suffix s ".java") then
      raise (Arg.Bad "no .java extension");
    file := Some s
  in
  Arg.parse spec set_file usage;
  match !file with Some f -> f | None -> Arg.usage spec usage; exit 1

let report (b,e) =
  let l = b.pos_lnum in
  let fc = b.pos_cnum - b.pos_bol + 1 in
  let lc = e.pos_cnum - b.pos_bol + 1 in
  eprintf "File \"%s\", line %d, characters %d-%d:\n" file l fc lc

let () =

  if !out_file = "" then out_file := Filename.chop_suffix file ".java" ^ ".s";

  let c = open_in file in
  let lb = Lexing.from_channel c in

  let ast_file = 
    begin try (
      Parser.file Lexer.next_token lb
      ) 
    with
    | Lexer.Lexing_error s ->
	report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "lexical error: %s@." s;
	exit 1
    | Parser.Error ->
	report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "syntax error@.";
	exit 1
    end
  in
    close_in c;

    if !parse_only then exit 0;

    let typed_ast =
      begin try ( 
        Typer.type_file ast_file
      )
      with
   | Typer.Typing_error ((l, c), msg) -> 
        eprintf "File '%s', line %d: \n" file l.pos_lnum;
        eprintf "type error: %s@." msg;
        exit 1
    | e ->
	eprintf "Anomaly: %s\n@." (Printexc.to_string e);
	exit 2
      end
    in

    if !type_only then exit 0;

    begin try (
      Compile.compile_file typed_ast !out_file
    )
    with
    | Compile.Compile_error msg -> 
(*        eprintf "File '%s', line %d: \n" file l.pos_lnum;*)
        eprintf "compile error: %s@." msg;
        exit 1
    | e -> 
	eprintf "Anomaly: %s\n@." (Printexc.to_string e);
	exit 2
    end

