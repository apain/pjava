open X86_64

let string_data = ref (label ".Sprint_int" ++ string "%d")
let string_nb = ref 0
let w_nb = ref 0
let l_nb = ref 0
let i_nb = ref 0
let e_nb = ref 0
let r_nb = ref 0
let a_nb = ref 0
let o_nb = ref 0
let h_nb = ref 0
let f_nb = ref 0

let generate_str_label () =
        incr string_nb;
        "str" ^ string_of_int !string_nb

let generate_w_label () =
        incr w_nb;
        "W" ^ string_of_int !w_nb

let generate_l_label () =
        incr l_nb;
        "L" ^ string_of_int !l_nb

let generate_i_label () =
        incr i_nb;
        "I" ^ string_of_int !i_nb

let generate_e_label () =
        incr e_nb;
        "E" ^ string_of_int !e_nb

let generate_r_label () =
        incr r_nb;
        "R" ^ string_of_int !r_nb

let generate_a_label () =
        incr a_nb;
        "A" ^ string_of_int !a_nb

let generate_o_label () =
        incr o_nb;
        "O" ^ string_of_int !o_nb

let generate_h_label () =
        incr h_nb;
        "H" ^ string_of_int !h_nb

let generate_f_label () =
        incr f_nb;
        "F" ^ string_of_int !f_nb

let concat_code =
        label "concat" ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++ 
        pushq !%rdi ++
        pushq !%rsi ++
        call "strlen" ++ 
        pushq !%rax ++
        movq (ind ~ofs:(-16) rbp) !%rdi ++
        call "strlen" ++
        movq !%rax !%r10 ++
        addq (ind ~ofs:(-24) rbp) !%r10 ++
        movq !%r10 !%rdi ++
        call "malloc" ++
        pushq !%rax ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        movq (ind ~ofs:(-8) rbp) !%rsi ++
        call "strcpy" ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        movq (ind ~ofs:(-24) rbp) !%rcx ++
        addq !%rcx !%rdi ++
        movq (ind ~ofs:(-16) rbp) !%rsi ++
        call "strcpy" ++
        movq (ind ~ofs:(-32) rbp) !%rax ++
        addq (imm 32) !%rsp ++
        popq rbp ++
        ret

let concat_int_str_code =
        label "concat_int_str" ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        pushq !%rdi ++
        pushq !%rsi ++
        movq (ind ~ofs:(-16) rbp) !%rdi ++
        call "strlen" ++
        pushq !%rax ++
        movq (ind ~ofs:(-24) rbp) !%rdi ++
        addq (imm 21) !%rdi ++
        movq (imm 0) !%rax ++
        call "malloc" ++
        pushq !%rax ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        leaq (lab ".Sprint_int") rsi ++
        movq (ind ~ofs:(-8) rbp) !%rdx ++
        movq (imm 0) !%rax ++
        movq (imm 8) !%rcx ++
        testq !%rsp !%rcx ++
        jne "align_stack_int_str" ++
        call "sprintf" ++
        jmp "int_str_end" ++
        label "align_stack_int_str" ++
        subq (imm 8) !%rsp ++
        call "sprintf" ++
        addq (imm 8) !%rsp ++
        label "int_str_end" ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        call "strlen" ++
        pushq !%rax ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        addq (ind ~ofs:(-40) rbp) !%rdi ++
        movq (ind ~ofs:(-16) rbp) !%rsi ++
        call "strcpy" ++
        movq (ind ~ofs:(-32) rbp) !%rax ++
        addq (imm 40) !%rsp ++
        popq rbp ++
        ret

let concat_str_int_code =
        label "concat_str_int" ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        pushq !%rdi ++
        pushq !%rsi ++
        call "strlen" ++
        pushq !%rax ++
        movq (ind ~ofs:(-24) rbp) !%rdi ++
        addq (imm 21) !%rdi ++
        movq (imm 0) !%rax ++
        call "malloc" ++
        pushq !%rax ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        movq (ind ~ofs:(-8) rbp) !%rsi ++
        call "strcpy" ++
        movq (ind ~ofs:(-32) rbp) !%rdi ++
        addq (ind ~ofs:(-24) rbp) !%rdi ++
        leaq (lab ".Sprint_int") rsi ++
        movq (ind ~ofs:(-16) rbp) !%rdx ++
        movq (imm 0) !%rax ++
        movq (imm 8) !%rcx ++
        testq !%rsp !%rcx ++
        jne "align_stack_str_int" ++
        call "sprintf" ++
        jmp "str_int_end" ++
        label "align_stack_str_int" ++
        subq (imm 8) !%rsp ++
        call "sprintf" ++
        addq (imm 8) !%rsp ++
        label "str_int_end" ++
        movq (ind ~ofs:(-32) rbp) !%rax ++
        addq (imm 32) !%rsp ++
        popq rbp ++
        ret

