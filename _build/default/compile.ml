open Format
open Ast
open Typed_ast
open X86_64
open Utils

exception Compile_error of string

type pos_env = int Smap.t

type class_descriptor =
        { cls_label : label; (* class label *)
        scl_label : label option; (* superclass label *)
        cstr_label : label option; (* class constructor label *)
        mtd_ofs : int Smap.t; (* offset of methods in class descriptor *)
        mtd_labels: label Smap.t; (* method labels in class descriptor *)
        mtd_list : ident list; (* methods including superclass methods *)
        field_map: int Smap.t; (* offset of fields *)
        field_list: ident list; (* list of fields *)
        nb_fields: int} (* nb of fields including superclass fields *)

let empty_cls_descr =
        { cls_label = ""; 
        scl_label = None; 
        cstr_label = None;
        mtd_ofs = Smap.empty; 
        mtd_labels = Smap.empty;
        mtd_list = [];
        field_map = Smap.empty;
        field_list = [];
        nb_fields = 0}

(* main data structure, mapping a class ident to its class_descriptor *)
let class_descriptor_table = Hashtbl.create 17

let print_string msg_lab =
        movq !%rdi !%rsi ++
        movq (lab ("$"^msg_lab)) !%rdi ++
        movq (imm 0) !%rax ++
        call "printf"

let rec compile_expr pos_env cls_descr e =
        match e.e_tdesc with
        | TEconstant c ->
                begin match c with
                | Cstring s -> let msg_lab = generate_str_label () in
                string_data := !string_data ++ label msg_lab ++ string s; 
                movq (lab ("$"^msg_lab)) !%rax ++
                pushq !%rax
                | Cint n ->
                movq (imm n) !%rax ++
                pushq !%rax 
                end 
        | TEbool b -> 
                let bit = begin match b with
                | false -> 0
                | true -> 1
                end in
                movq (imm bit) !%rax ++
                pushq !%rax
        | TEthis -> 
                begin try (let this_ofs = Smap.find "this" pos_env in
                movq (ind ~ofs:this_ofs rbp) !%rdi ++
                pushq !%rdi
                ) with Not_found -> failwith "this not found" end
        | TEnew (nt, class_id, scl_id, e_list) -> (* call to constructor *)
                let call_super_constr = begin match scl_id with
                | None -> nop
                | Some s_id ->
                begin try (let scl_descr = Hashtbl.find class_descriptor_table s_id in
                let super_constr_label = scl_descr.cstr_label in 
                begin match super_constr_label with
                | None -> nop
                | Some s_lab -> call s_lab 
                end
                ) with Not_found -> failwith "superclass not found in compile_expr" end 
                end in
                begin try (let obj_cl_descr = Hashtbl.find class_descriptor_table class_id
                in
                let call_constr = begin match obj_cl_descr.cstr_label with
                | None -> nop
                | Some cstr_lab -> call cstr_lab
                end in
                let descr_lab = obj_cl_descr.cls_label in
                let obj_field_list = obj_cl_descr.field_list in
                let obj_field_map = obj_cl_descr.field_map in
                let nb_fields = (obj_cl_descr.nb_fields + 1) * 8 in
                let init_fields = List.fold_left
                (fun code field_id -> let ofs = Smap.find field_id obj_field_map in code ++ movq (imm 0) (ind ~ofs:ofs rax))
                nop obj_field_list in
                let code_args = List.fold_left (fun code e -> code ++ compile_expr pos_env cls_descr e) nop e_list in
                let nb_args = (List.length e_list + 1) * 8 in
                code_args ++
                movq (imm nb_fields) !%rdi ++
                call "malloc" ++
                init_fields ++
                movq (ilab descr_lab) (ind rax) ++
                pushq !%rax ++
                call_super_constr ++
                call_constr ++
                addq (imm nb_args) !%rsp ++
                pushq !%rax
                ) with Not_found -> failwith "class not found in compile_expr" end 
        | TEassign (a, e) -> 
                begin match a.a_tdesc with
                | TAident id ->
                        if Smap.mem id pos_env then (* either a local variable *)
                                let ofs = - (Smap.find id pos_env) in
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                movq !%rax (ind ~ofs rbp) ++
                                pushq !%rax 
                        else begin try (* or a current class field *)
                                let obj_ofs = Smap.find id cls_descr.field_map in
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                movq (ind ~ofs:24 rbp) !%rdi ++
                                movq !%rax (ind ~ofs:obj_ofs rdi) ++
                                pushq !%rax
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in compile_expr")) end
                | TAattr (e2, id) -> 
                        let static_type = begin match e.e_typ with
                        | Tnew (c_id, _) -> c_id 
                        | _ -> failwith "should not happen"
                        end in
                        begin try
                                let obj_cls_descr = Hashtbl.find class_descriptor_table static_type in
                                let ofs = Smap.find id cls_descr.field_map in
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                compile_expr pos_env obj_cls_descr e2 ++
                                popq rcx ++
                                movq !%rax (ind ~ofs rcx) ++
                                pushq !%rax
                        with Not_found -> failwith "class not found in compile_expr" end
                end
        | TEmethodCall (a, e_list) -> (* method call *)
                let code_args = List.fold_left (fun code e -> code ++ compile_expr pos_env cls_descr e) nop e_list in
                let nb_args = (List.length e_list + 1) * 8 in
                begin match a.a_tdesc with
                | TAident mtd_id -> let mtd_ofs_map = cls_descr.mtd_ofs in
                        begin try (let mtd_ofs = Smap.find mtd_id mtd_ofs_map in
                        code_args ++
                        movq (ind ~ofs:24 rbp) !%rax ++
                        pushq !%rax ++
                        movq (ind rax) !%rcx ++
                        addq (imm mtd_ofs) !%rcx ++
                        call_star (ind rcx) ++
                        addq (imm nb_args) !%rsp ++
                        pushq !%rax
                        ) with Not_found -> failwith "method not found in compile_expr" end
                | TAattr (e, mtd_id) ->
                        begin match e.e_typ with
                        | Tnew (c_id, _) -> 
                                begin try (let obj_cl_descr = Hashtbl.find class_descriptor_table c_id in
                                let mtd_ofs_map = obj_cl_descr.mtd_ofs in
                                begin try (let mtd_ofs = Smap.find mtd_id mtd_ofs_map in 
                                code_args ++
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                movq (ind rax) !%rcx ++
                                pushq !%rax ++
                                addq (imm mtd_ofs) !%rcx ++
                                call_star (ind rcx) ++
                                addq (imm nb_args) !%rsp ++
                                pushq !%rax) with Not_found -> failwith "method not found in compile_expr" end)
                                with Not_found -> failwith "class not found in compile_expr" end
                        | Tstring -> let h_lab = generate_h_label () in
                                let f_lab = generate_f_label () in
                                compile_expr pos_env cls_descr e ++
                                code_args ++
                                popq rdx ++
                                popq rcx ++
                                cmpq !%rdx !%rcx ++
                                jz h_lab ++ (* if true *)
                                movq (imm 0) !%rax ++ (* false *)
                                pushq !%rax ++
                                jmp f_lab ++
                                label h_lab ++
                                movq (imm 1) !%rax ++ (* true *)
                                pushq !%rax ++
                                label f_lab
                        | _ -> failwith "should not happen"
                        end
               end
        | TEaccess a -> 
                begin match a.a_tdesc with
                | TAident id -> 
                        if Smap.mem id pos_env then 
                                let ofs = - (Smap.find id pos_env) in
                                movq (ind ~ofs rbp) !%rax ++
                                pushq !%rax
                        else begin try
                                let obj_ofs = Smap.find id cls_descr.field_map in
                                movq (ind ~ofs:24 rbp) !%rdi ++
                                movq (ind ~ofs:obj_ofs rdi) !%rax ++
                                pushq !%rax
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in compile_expr")) end
                | TAattr (e, id) ->
                        let static_type = begin match e.e_typ with
                        | Tnew (c_id, _) -> c_id 
                        | _ -> failwith "should not happen"
                        end in
                        begin try
                                let obj_cls_descr = Hashtbl.find class_descriptor_table static_type in
                                let obj_field_map = obj_cls_descr.field_map in
                                let ofs = Smap.find id obj_field_map in
                                compile_expr pos_env cls_descr e ++
                                popq rcx ++
                                movq (ind ~ofs rcx) !%rax ++
                                pushq !%rax
                        with Not_found -> failwith "class not found in compile_expr" end
                end
        | TEnull -> 
                movq (imm 0) !%rax ++
                pushq !%rax
        | TEunop (o, e) ->
                let op = begin match o with
                | Uneg -> negq
                | Unot -> notq
                end in
                compile_expr pos_env cls_descr e ++ 
                popq rax ++
                op !%rax ++
                pushq !%rax
        | TEbinop (o, e1, e2) ->
                let exprs = 
                compile_expr pos_env cls_descr e1 ++
                compile_expr pos_env cls_descr e2 ++
                popq rbx ++
                popq rax 
                in
                begin match o with
                | Bdiv | Bmod -> 
                        let result = begin match o with
                        | Bdiv -> !%rax
                        | Bmod -> !%rdx 
                        | _ -> assert false
                        end in
                        exprs ++
                        movq (imm 0) !%rdx ++
                        idivq !%rbx ++
                        pushq result
                | Badd | Bsub | Bmul  ->
                        let op = begin match o with
                        | Badd -> addq
                        | Bsub -> subq
                        | Bmul -> imulq
                        | _ -> assert false
                        end in 
                        exprs ++
                        op !%rbx !%rax ++
                        pushq !%rax
                | Band ->
                        let a_lab1 = generate_a_label () in
                        let a_lab2 = generate_a_label () in
                        compile_expr pos_env cls_descr e1 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jz a_lab1 ++
                        compile_expr pos_env cls_descr e2 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jz a_lab1 ++
                        movq (imm 1) !%rax ++
                        pushq !%rax ++
                        jmp a_lab2 ++
                        label a_lab1 ++
                        pushq !%rax ++
                        label a_lab2
                | Bor -> 
                        let o_lab1 = generate_o_label () in
                        let o_lab2 = generate_o_label () in
                        compile_expr pos_env cls_descr e1 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jnz o_lab1 ++
                        compile_expr pos_env cls_descr e2 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jnz o_lab1 ++
                        movq (imm 0) !%rax ++
                        pushq !%rax ++
                        jmp o_lab2 ++
                        label o_lab1 ++
                        pushq !%rax ++
                        label o_lab2
                | Beq | Bneq | Blt | Ble | Bgt | Bge ->
                        let op = begin match o with
                        | Beq -> sete
                        | Bneq -> setne
                        | Blt -> setl
                        | Ble -> setle
                        | Bgt -> setg
                        | Bge -> setge
                        | _ -> assert false
                        end in
                        compile_expr pos_env cls_descr e1 ++
                        compile_expr pos_env cls_descr e2 ++
                        popq rbx ++
                        popq rcx ++
                        movq (imm 0) !%rax ++
                        cmpq !%rbx !%rcx ++
                        op !%al ++
                        pushq !%rax
               end
          | TEconcat (Badd, e1, e2) ->
                compile_expr pos_env cls_descr e1 ++
                compile_expr pos_env cls_descr e2 ++
                popq rsi ++
                popq rdi ++
                call "concat" ++
                pushq !%rax
          | TEconcat (_, _, _) -> failwith "should not happen"
          | TEconcatIntString (Badd, e1, e2) ->
                compile_expr pos_env cls_descr e1 ++
                compile_expr pos_env cls_descr e2 ++
                popq rsi ++
                popq rdi ++
                call "concat_int_str" ++
                pushq !%rax
          | TEconcatIntString (_, _, _) -> failwith "should not happen"
          | TEconcatStringInt (Badd, e1, e2) ->
                compile_expr pos_env cls_descr e1 ++
                compile_expr pos_env cls_descr e2 ++
                popq rsi ++
                popq rdi ++
                call "concat_str_int" ++
                pushq !%rax
          | TEconcatStringInt (_, _, _) -> failwith "should not happen" 
          | TEprint e1 -> compile_expr pos_env cls_descr e1 ++
                popq rdi ++
                movq (imm 0) !%rax ++
                call "printf"

let rec compile_instr code pos_env cls_descr next i =
        (* -> asm.text, pos_env, next (first free pos on stack) *)
        match i with
        | TIvoid -> code, pos_env, next
        | TIse e -> begin match e.e_tdesc with
                | TEnew _ | TEmethodCall _ -> code ++ compile_expr pos_env cls_descr e ++ popq rax, pos_env, next
                |  _ -> code ++ compile_expr pos_env cls_descr e, pos_env, next
        end
        | TIassign (a, e) ->
                begin match a.a_tdesc with
                | TAident id ->
                        if Smap.mem id pos_env then
                                let ofs = - (Smap.find id pos_env) in
                                code ++
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                movq !%rax (ind ~ofs rbp)
                        else begin try
                                let obj_ofs = Smap.find id cls_descr.field_map in
                                let this_ofs = Smap.find "this" pos_env in
                                code ++
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                movq (ind ~ofs:this_ofs rbp) !%rdi ++
                                movq !%rax (ind ~ofs:obj_ofs rdi)
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in compile_instr")) end
                | TAattr (e2, id) -> 
                        let static_type = begin match e2.e_typ with
                        | Tnew (c_id, _) -> c_id 
                        | _ -> failwith "should not happen"
                        end in
                        begin try
                                let obj_cls_descr = Hashtbl.find class_descriptor_table static_type in
                                begin try (
                                let ofs = Smap.find id obj_cls_descr.field_map in
                                code ++
                                compile_expr pos_env cls_descr e ++
                                popq rax ++
                                compile_expr pos_env obj_cls_descr e2 ++
                                popq rcx ++
                                movq !%rax (ind ~ofs rcx))
                                with Not_found -> failwith "field not found in compile_instr" end 
                        with Not_found -> failwith "class descriptor not found in compile_instr" end
                end
                , pos_env, next
        | TIdeclare (_, id) -> code, (Smap.add id next pos_env), (next + 8) 
        | TIdeclAssign (_, id, e) -> 
                code ++ 
                compile_expr pos_env cls_descr e ++
                popq rax ++
                movq !%rax (ind ~ofs:(-next) rbp),
                (Smap.add id next pos_env), (next + 8)
        | TIif (e, i) ->
                let i_lab = generate_i_label () in
                let code_if, _, _ = compile_instr nop pos_env cls_descr next i in
                code ++ 
                compile_expr pos_env cls_descr e ++
                popq rax ++
                testq !%rax !%rax ++
                jz i_lab ++  (* if false *)
                code_if ++
                label i_lab,
                pos_env, next
        | TIifElse (e, i1, i2) -> 
                let e_lab = generate_e_label () in
                let r_lab = generate_r_label () in
                let code_if_true, _, _ = compile_instr nop pos_env cls_descr next i1 in
                let code_if_false, _, _ = compile_instr nop pos_env cls_descr next i2 in
                code ++ 
                compile_expr pos_env cls_descr e ++
                popq rax ++
                testq !%rax !%rax ++
                jz e_lab ++  (* if false *)
                code_if_true ++
                jmp r_lab ++
                label e_lab ++
                code_if_false ++
                label r_lab,
                pos_env, next
        | TIwhile (e, i) ->
                let w_lab = generate_w_label () in
                let l_lab = generate_l_label () in
                let code_while, _, _ = compile_instr nop pos_env cls_descr next i in
                code ++
                jmp l_lab ++
                label w_lab ++
                code_while ++
                label l_lab ++
                compile_expr pos_env cls_descr e ++
                popq rax ++
                testq !%rax !%rax ++
                jnz w_lab (* if true *),
                pos_env, next
        | TIblock (i_list) -> 
                let code_block, _, _ = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env cls_descr next i)
                (nop, pos_env, 0)
                i_list
                in 
                code ++
                code_block,
                pos_env, next
        | TIreturn e -> begin match e with
                | Some e1 -> code ++
                        compile_expr pos_env cls_descr e1 ++
                        popq rax
                        , pos_env, next
                | None -> code ++
                        movq (imm 0) !%rax
                        , pos_env, next
                end

let compile_main tclass_main =
        let _, i_list = tclass_main in
        let cls_descr = empty_cls_descr in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env cls_descr next i) 
        (nop, Smap.empty, 0)
        i_list
        in
        subq (imm frame_size) !%rsp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        addq (imm frame_size) !%rsp ++
        movq (imm 0) !%rax

let compile_method mtd =
        let mtd_id = mtd.tm_id in
        let class_id = mtd.tm_cl_id in
        let method_env = mtd.tm_env in
        let formals = method_env.formals in
        let i_list = mtd.tm_instr in
        let arg_size = (List.length formals + 1) * 8 in
        let pos_env, this_ofs = 
        List.fold_left (fun (pos_env, ofs) (_, arg) ->
                (Smap.add arg (-ofs) pos_env, ofs - 8))
        (Smap.empty, arg_size + 16 (*8 *))
        formals
        in 
        let pos_env = Smap.add "this" this_ofs pos_env 
        in
        begin try (let cls_descr = Hashtbl.find class_descriptor_table class_id in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env cls_descr next i) 
        (nop, pos_env, 0)
        i_list
        in
        label (class_id^"_"^mtd_id) ++
        pushq !%rbp ++
        leaq (ind ~ofs:((*frame_size*) - 8) rsp) rbp ++
        code_instr ++
        popq rbp ++
        ret
        ) with Not_found -> failwith "class not found in compile_method" end

let compile_constructor constr =
        let class_id, scl_id, formals, i_list, _ = constr in
        let cls_descr = begin try Hashtbl.find class_descriptor_table class_id with Not_found -> failwith "class not found in compile_constructor" end in
        let arg_size = (List.length formals + 1) * 8 in
        let pos_env, this_ofs = 
        List.fold_left (fun (pos_env, ofs) (_, arg) ->
                (Smap.add arg (-ofs) pos_env, ofs - 8))
        (Smap.empty, arg_size + 16)
        formals
        in 
        let pos_env = Smap.add "this" this_ofs pos_env
        in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env cls_descr next i) 
        (nop, pos_env, 0)
        i_list
        in
        label (class_id^"_constr") ++
        pushq !%rbp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        movq (ind ~ofs:24 rbp) !%rax ++
        popq rbp ++
        ret

let compile_decl d =
        match d with
        | TDdecl (_, id) -> nop
        | TDconstructor c -> compile_constructor c
        | TDmethod m -> compile_method m

let compile_class c =
        let decls = c.tc_declarations in
        List.fold_left (fun code d -> code ++ compile_decl d) 
        nop
        decls

let compile_classes cls_intf_list =
        List.fold_left (fun code c_i -> 
               begin match c_i with
               | TCClass c -> code ++ compile_class c
               | TCInterface i -> code
               end) 
        nop
        cls_intf_list

let make_field_map start_map start_ofs field_list =
        let field_map, _ =
        List.fold_left (fun (env, pos) field ->
                (Smap.add field pos env, pos + 8)) 
        (start_map, start_ofs)
        field_list
        in field_map

let make_class_descriptor class_env =
        let c_id = class_env.name in 
        let descr_label = "descr_" ^ c_id in 
        let mtd_list = class_env.mtd_list in
        let cstr_lab = begin match class_env.has_cstr with
        | true -> Some (c_id ^ "_constr")
        | false -> None
        end in
        let scl_env = class_env.scl_env in 
        let cls_descriptor = begin match scl_env with
        | None -> 
        let mtd_ofs, mtd_labels,  _ = List.fold_left
        (fun (mtd_ofs, mtd_labels, ofs) mtd_id ->
                (Smap.add mtd_id ofs mtd_ofs, Smap.add mtd_id (c_id ^ "_" ^ mtd_id) mtd_labels, ofs + 8))
        (Smap.empty, Smap.empty, 8)
        mtd_list
        in
        let field_list = class_env.field_list in
        let field_map = make_field_map Smap.empty 8 field_list in
        { cls_label = descr_label;
        scl_label = None;
        cstr_label = cstr_lab;
        mtd_ofs = mtd_ofs;
        mtd_labels = mtd_labels; 
        mtd_list = mtd_list;
        field_map = field_map; 
        field_list = field_list;
        nb_fields = List.length field_list} 
        | Some scl_env -> let scl_id = scl_env.name in
                let scl_descriptor = begin try 
                        (Hashtbl.find class_descriptor_table scl_id) with Not_found -> failwith "superclass not found in class_descriptor_table" end
                in let scl_label = scl_descriptor.cls_label
                in
                let scl_mtd_ofs = scl_descriptor.mtd_ofs in
                let scl_mtd_labels = scl_descriptor.mtd_labels in
                let scl_mtd_list = scl_descriptor.mtd_list in
                let nb_super_mtds = (List.length scl_mtd_list * 8) in
                let mtd_ofs, mtd_labels, full_mtd_list, _ = List.fold_left
                (fun (mtd_ofs, mtd_labels, f_mtd_l, ofs) mtd_id ->
                        (Smap.add mtd_id ofs mtd_ofs, Smap.add mtd_id (c_id ^ "_" ^ mtd_id) mtd_labels, f_mtd_l@[mtd_id], ofs + 8))
                (scl_mtd_ofs, scl_mtd_labels, scl_mtd_list, nb_super_mtds + 8)
                mtd_list
        in
        let field_list = class_env.field_list in
        let start_ofs = (scl_descriptor.nb_fields + 1) * 8 in
        let scl_field_map = scl_descriptor.field_map in
        let field_map = make_field_map scl_field_map start_ofs field_list in
        let nb_fields = scl_descriptor.nb_fields + (List.length field_list) in
        { cls_label = descr_label;
        scl_label = Some scl_label;
        cstr_label = cstr_lab;
        mtd_ofs = mtd_ofs; 
        mtd_labels = mtd_labels; 
        mtd_list = full_mtd_list;
        field_map = field_map; 
        field_list = field_list;
        nb_fields = nb_fields}
        end
        in Hashtbl.add class_descriptor_table c_id cls_descriptor

let make_class_descriptors cls_intf_list =
        let rec treat cls_env =
                if Hashtbl.mem class_descriptor_table cls_env.name then ()
                else begin match cls_env.scl_env with
                | None -> make_class_descriptor cls_env
                | Some sclass_env -> treat sclass_env;
                make_class_descriptor cls_env
                end
        in
        List.iter (fun c_intf -> begin match c_intf with
        | TCClass c -> treat c.tc_env
        | TCInterface _ -> ()
        end 
        )
        cls_intf_list 

let write_class_descriptors cls_intf_list =
        make_class_descriptors cls_intf_list;
        Hashtbl.fold (fun c_id cls_descr code -> let s_label = begin match cls_descr.scl_label with
        | None -> "0"
        | Some scl_label -> scl_label
        end in 
        code ++
        label cls_descr.cls_label ++
        address [s_label] ++
        List.fold_left (fun code mtd_id -> let mtd_label = Smap.find mtd_id cls_descr.mtd_labels in
        code ++ address [mtd_label]
        ) nop cls_descr.mtd_list
        )
        class_descriptor_table 
        nop
       
(* main function *)
let compile_file typed_ast out_file = 
        (* takes as input the output of the main typer function = typed ast *)

        let cls_intf_list = typed_ast.tf_cls_intf_list in
        let class_main = typed_ast.tf_class_main in

        let cls_data = write_class_descriptors cls_intf_list in

        let class_code = compile_classes cls_intf_list in
        
        let main_code = compile_main class_main in

        let p =
        {
	        text =
                        globl "main" ++
                        label "main" ++
                        main_code ++
                        ret ++
                        class_code ++
                        concat_code ++
                        concat_int_str_code ++
                        concat_str_int_code
        ;
                data = 
                        !string_data ++
                        cls_data
	}

        in
        let f = open_out out_file in
        let fmt = formatter_of_out_channel f in
        print_program fmt p;
        fprintf fmt "@?";
        close_out f
