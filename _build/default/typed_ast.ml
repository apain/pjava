open Ast

type pj_typ = 
        | Tvoid
        | Tboolean
        | Tint
        | Tstring 
        | Ttypenull
        | Tnew of ident * ntype list option (* can be followed by a list of type parameters *)

(* == DATA STRUCTURES == *)

module Smap = Map.Make(String)
module Sset = Set.Make(String)

type method_env = { ret_typ : pj_typ; formals : parameter list ; m_vars : pj_typ Smap.t }
let empty_method_env = { ret_typ = Ttypenull; formals = []; m_vars = Smap.empty }

type class_env = { is_class : bool; name : ident; has_cstr : bool; extends : ntype option; implements : ntype list option; type_vars : paramtype list option; methods : method_env Smap.t; fields : pj_typ Smap.t; field_list : ident list; mtd_list : ident list; scl_env : class_env option }
let empty_class_env = { is_class = true; name = ""; has_cstr = false; extends = Some (Nntype ("Object", None)); implements = None; type_vars = None; methods = Smap.empty; fields = Smap.empty; field_list = []; mtd_list = []; scl_env = None }

type full_env = class_env Smap.t
let empty_env = Smap.empty 

let fake_pos = ({ Lexing.pos_fname = "fake"; Lexing.pos_lnum = 1000; Lexing.pos_bol = 1000; Lexing.pos_cnum = 1000 }, { Lexing.pos_fname = "fake"; Lexing.pos_lnum = 1000; Lexing.pos_bol = 1000; Lexing.pos_cnum = 1000 })

(* == TYPED AST == *)

type tfile = { tf_cls_intf_list: tclass_interface list;
  tf_class_main: tclass_main;
  tf_full_env: full_env 
}

and tclass_interface =
  | TCClass of tcls
  | TCInterface of tinterface

and tcls = {
  tc_name: ident;
  tc_params: paramstype option;
  tc_extends: ntype option;
  tc_scl: class_env option;
  tc_sestcl: class_env;
  tc_implements: ntype list option;
  tc_declarations: tdecl list;
  tc_env: class_env 
}

and tinterface = {
  ti_name: ident;
  ti_params: paramstype option;
  ti_extends: ntype list option;
  ti_protocols: proto list;
  ti_env: class_env 
}

and tdecl = 
  | TDdecl of typ * ident
  | TDconstructor of tconstructor
  | TDmethod of tmtd

and tconstructor =
  ident * ident option * parameter list * tinstruction list * method_env (* class_id, superclass id *)

and tmtd = {
  tm_id : ident;
  tm_proto : proto;
  tm_instr: tinstruction list;
  tm_cl_id: ident;
  tm_scl_id: ident option;
  tm_env: method_env
}

and tclass_main =
  ident * tinstruction list

and texpr = {
        e_tdesc: e_tdesc;
        e_typ: pj_typ;
        }

and taccess = {
        a_tdesc: a_tdesc;
        a_typ: pj_typ;
        }

and e_tdesc =
  | TEconstant of constant
  | TEbool of bool
  | TEthis
  | TEnew of ntype * ident * ident option * texpr list (* class_id, scl_id *)
  | TEassign of taccess * texpr
  | TEmethodCall of taccess * texpr list
  | TEaccess of taccess
  | TEnull
  | TEunop of unop * texpr
  | TEbinop of binop * texpr * texpr
  | TEconcat of binop * texpr * texpr (* string + string *)
  | TEconcatStringInt of binop * texpr * texpr (* string + int *)
  | TEconcatIntString of binop * texpr * texpr (* int + string *)
  | TEprint of texpr

and a_tdesc =
  | TAident of ident 
  | TAattr of texpr * ident 

and tinstruction = 
  | TIvoid
  | TIse of texpr
  | TIassign of taccess * texpr
  | TIdeclare of typ * ident
  | TIdeclAssign of typ * ident * texpr
  | TIif of texpr * tinstruction
  | TIifElse of texpr * tinstruction * tinstruction
  | TIwhile of texpr * tinstruction
  | TIblock of tinstruction list
  | TIreturn of texpr option
