
(* The type of tokens. *)

type token = 
  | WHILE
  | VOID
  | TRUE
  | TIMES
  | THIS
  | SYSTEMOUTPRINT
  | STRING
  | STATIC
  | SEMICOLON
  | RSQ
  | RP
  | RETURN
  | RBRACE
  | PUBLIC
  | PLUS
  | OR
  | NULL
  | NO_ELSE
  | NOT
  | NEW
  | MOD
  | MINUS
  | MAIN
  | LSQ
  | LP
  | LBRACE
  | INTERFACE
  | INT
  | IMPLEMENTS
  | IF
  | IDENT of (string)
  | FALSE
  | EXTENDS
  | EQUAL
  | EOF
  | ELSE
  | DOT
  | DIV
  | CST of (Ast.constant)
  | COMMA
  | CLASS_MAIN
  | CLASS
  | BOOLEAN
  | BNEQ
  | BLT
  | BLE
  | BGT
  | BGE
  | BEQ
  | AND
  | AMPERSAND

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val file: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Ast.file)
