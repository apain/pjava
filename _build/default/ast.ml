
type loc = Lexing.position * Lexing.position (* Error localisation *)

type ident = string (*Identificators*)

type void = TVoid

type unop =(*Unary operators*)
  | Uneg (*-e*)
  | Unot (*!e*)

type binop = (*Binary operators*)
  | Badd | Bsub | Bmul | Bdiv | Bmod    (* + - * / % *)
  | Beq | Bneq | Blt | Ble | Bgt | Bge  (* == != < <= > >= *)
  | Band | Bor                          (* && || *)

type file = 
  class_interface list * class_main

and class_interface = 
  | CClass of cls
  | CInterface of interface

and cls = {
  c_name: ident;
  c_params: paramstype option;
  c_extends: ntype option;
  c_implements: ntype list option;
  c_declarations: decl list;
  c_loc: loc }

and interface = {
  i_name: ident;
  i_params: paramstype option;
  i_extends: ntype list option;
  i_protocols: proto list;
  it_loc: loc }

and paramstype =
  paramtype list

and paramtype =
  ident * ntype list option

and decl = {
  d_desc: d_desc;
  d_loc: loc;
}

and d_desc =
  | Ddecl of typ * ident
  | Dconstructor of constructor
  | Dmethod of mtd
  
and constructor =
  ident * parameter list * instruction list

and mtd =
  proto * instruction list

and proto =
  | Pproto of typ * ident * parameter list
  | PprotoVoid of ident * parameter list

and parameter =
  typ * ident

and typ =
  | Tbool 
  | Tint 
  | Tntype of ntype
  | Tstring

and ntype = 
  Nntype of ident * ntype list option 

and class_main =
  ident * instruction list

and expr = {
  e_desc: e_desc;
  e_loc: loc;
}

and e_desc = 
  | Enull
  | Econstant of constant
  | Ebool of bool
  | Ethis 
  | Enew of ntype * expr list
  | Eassign of access * expr
  | EmethodCall of access * expr list
  | Eaccess of access
  | Eunop of unop * expr 
  | Ebinop of binop * expr * expr
  | Eprint of expr

and access = {
  a_desc: a_desc;
  a_loc: loc;
}

and a_desc =
  | Aident of ident 
  | Aattr of expr * ident

and constant =
  | Cstring of string
  | Cint of int

and instruction = {
  i_desc: i_desc;
  i_loc: loc;
}

and i_desc =
  | Ivoid
  | Ise of expr
  | Iassign of access * expr
  | Ideclare of typ * ident
  | IdeclAssign of typ * ident * expr
  | Iif of expr * instruction
  | IifElse of expr * instruction * instruction
  | Iwhile of expr * instruction 
  | Iblock of instruction list 
  | Ireturn of expr option
