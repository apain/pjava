all: minijava.exe

tests: minijava.exe
	cd tests; ./test -3 ../_build/default/pjava.exe

minijava.exe:
	dune build pjava.exe
	cp _build/default/pjava.exe pjava

clean:
	@dune clean
	@rm tests/*/*.s
	@rm tests/a.out
	@rm test/out
	@echo "Cleaned !"

.PHONY: all clean pjava.exe
