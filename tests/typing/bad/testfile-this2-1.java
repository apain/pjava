class A { void m() { B b = this; } }

class B extends A { }

class Main { public static void main(String[] args) { } }
