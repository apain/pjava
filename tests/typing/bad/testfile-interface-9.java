class A {}
class B extends A {}
/*class B {}
class A extends B {}*/

interface I { B m(); }
interface J extends I { A m(); }
class Main { public static void main(String[] args) { } }
