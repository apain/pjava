
class A {}
class B extends A {}

interface I { B m(); }
class C implements I { public A m() { return null; } }
class Main { public static void main(String[] args) { } }
