
class A { }
class B extends A { }
class C { A m() { return null; } }
class D extends C { B m() { return null; } }
class Main { public static void main(String[] args) { } }
