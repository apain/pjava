
class A implements I { public void m() {} }
class B extends A {}
interface I { void m(); }
class D<X extends I> {}
class Test { void test(D<B> c) {} }
class Main { public static void main(String[] args) { } }
