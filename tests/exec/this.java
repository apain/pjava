class A {
    int x;

    A() { }

    boolean f() { return this.x == x; }
}

class B extends A {
    int y;

    B(int x) {
	this.y = x;
	this.x = x;
    }

    boolean g() { return this.f() && this.x == x; }
}

class Main {
    public static void main(String[] args) {
	B b = new B(1);
	if (new A().f() && b.f() && b.g())
	    System.out.print("ok\n");
    }
}
