	.text
	.globl	main
main:
	subq $8, %rsp
	leaq 0(%rsp), %rbp
	movq $1, %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	movq $0, %rdx
	idivq %rbx
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A5
	movq $str13, %rax
	pushq %rax
	movq $str12, %rax
	pushq %rax
	popq %rdx
	popq %rcx
	cmpq %rdx, %rcx
	jz H6
	movq $0, %rax
	pushq %rax
	jmp F6
H6:
	movq $1, %rax
	pushq %rax
F6:
	popq %rax
	testq %rax, %rax
	jz A5
	movq $1, %rax
	pushq %rax
	jmp A6
A5:
	pushq %rax
A6:
	popq %rax
	testq %rax, %rax
	jz A3
	movq $str9, %rax
	pushq %rax
	movq $str8, %rax
	pushq %rax
	popq %rdx
	popq %rcx
	cmpq %rdx, %rcx
	jz H4
	movq $0, %rax
	pushq %rax
	jmp F4
H4:
	movq $1, %rax
	pushq %rax
F4:
	popq %rax
	testq %rax, %rax
	jz A3
	movq $1, %rax
	pushq %rax
	jmp A4
A3:
	pushq %rax
A4:
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str1, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	addq $8, %rsp
	movq $0, %rax
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "ok\n"
str2:
	.string "dfsdf/* dfsdf */ fsdf // sdfsdf "
str3:
	.string "dfsdf/* dfsdf */ fsdf // sdfsdf "
str4:
	.string "\n \" /* */ // qsdq#sds\n "
str5:
	.string "\n \" /* */ // qsdq#sds\n "
str6:
	.string "\n \" /* */ // qsdq#sds\n "
str7:
	.string "\n \" /* */ // qsdq#sds\n "
str8:
	.string "dfsdf/* dfsdf */ fsdf // sdfsdf "
str9:
	.string "dfsdf/* dfsdf */ fsdf // sdfsdf "
str10:
	.string "\n \" /* */ // qsdq#sds\n "
str11:
	.string "\n \" /* */ // qsdq#sds\n "
str12:
	.string "\n \" /* */ // qsdq#sds\n "
str13:
	.string "\n \" /* */ // qsdq#sds\n "
