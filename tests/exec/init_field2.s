	.text
	.globl	main
main:
	subq $8, %rsp
	leaq 0(%rsp), %rbp
	movq $40, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $0, 32(%rax)
	movq $descr_A, 0(%rax)
	pushq %rax
	call A_constr
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $str3, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	movq $str2, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq $str5, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_str_int
	pushq %rax
	movq $str4, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str6, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 32(%rcx), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I2
	movq $str7, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I2:
	addq $8, %rsp
	movq $0, %rax
	ret
A_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $str1, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 8(%rdi)
	movq $42, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	movq $1, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 24(%rdi)
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 32(%rdi)
	movq 24(%rbp), %rax
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "toto"
str2:
	.string "\n"
str3:
	.string "x = "
str4:
	.string "\n"
str5:
	.string "y = "
str6:
	.string "z = true\n"
str7:
	.string "next = a\n"
descr_A:
	.quad 0
