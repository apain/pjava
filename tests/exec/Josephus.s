	.text
	.globl	main
main:
	subq $24, %rsp
	leaq 16(%rsp), %rbp
	movq $1, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_ListeC, 0(%rax)
	pushq %rax
	call ListeC_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq $3, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq $2, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq $8, %rdi
	call malloc
	movq $descr_Josephus, 0(%rax)
	pushq %rax
	call Josephus_constr
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq $7, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, -16(%rbp)
	movq -16(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq $7, %rax
	pushq %rax
	movq $5, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	movq $6, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A13
	movq $5, %rax
	pushq %rax
	movq $5, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	movq $2, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A13
	movq $1, %rax
	pushq %rax
	jmp A14
A13:
	pushq %rax
A14:
	popq %rax
	testq %rax, %rax
	jz A11
	movq $5, %rax
	pushq %rax
	movq $17, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	movq $4, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A11
	movq $1, %rax
	pushq %rax
	jmp A12
A11:
	pushq %rax
A12:
	popq %rax
	testq %rax, %rax
	jz A7
	movq $13, %rax
	pushq %rax
	movq $2, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	movq $11, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A7
	movq $1, %rax
	pushq %rax
	jmp A8
A7:
	pushq %rax
A8:
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str7, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	addq $24, %rsp
	movq $0, %rax
	ret
ListeC_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 8(%rdi)
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 24(%rdi)
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	movq 24(%rbp), %rax
	popq %rbp
	ret
ListeC_insererApres:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_ListeC, 0(%rax)
	pushq %rax
	call ListeC_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq %rax, 16(%rcx)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rcx
	movq %rax, 24(%rcx)
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq %rax, 24(%rcx)
	popq %rbp
	ret
ListeC_supprimer:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	popq %rcx
	movq %rax, 16(%rcx)
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rcx
	movq %rax, 24(%rcx)
	popq %rbp
	ret
ListeC_afficher:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $str1, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_int_str
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L1
W1:
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $str2, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_int_str
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
L1:
	movq 0(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W1
	movq $str3, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	popq %rbp
	ret
Josephus_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	popq %rbp
	ret
Josephus_cercle:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $1, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_ListeC, 0(%rax)
	pushq %rax
	call ListeC_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	jmp L2
W2:
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq -8(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
L2:
	movq -8(%rbp), %rax
	pushq %rax
	movq $2, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setge %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W2
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	popq %rbp
	ret
Josephus_josephus:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 40(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L3
W3:
	movq $1, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L4
W4:
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
L4:
	movq 0(%rbp), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setl %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W4
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $str4, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_int_str
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
L3:
	movq 0(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W3
	movq $str6, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_str_int
	pushq %rax
	movq $str5, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	popq %rax
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string " "
str2:
	.string " "
str3:
	.string "\n"
str4:
	.string " sort\n"
str5:
	.string "\n"
str6:
	.string "le gagnant est "
str7:
	.string "ok\n"
descr_ListeC:
	.quad 0
	.quad ListeC_insererApres
	.quad ListeC_supprimer
	.quad ListeC_afficher
descr_Josephus:
	.quad 0
	.quad Josephus_cercle
	.quad Josephus_josephus
