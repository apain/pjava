	.text
	.globl	main
main:
	subq $0, %rsp
	leaq -8(%rsp), %rbp
	movq $str8, %rax
	pushq %rax
	movq $str9, %rax
	pushq %rax
	movq $16, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $descr_A, 0(%rax)
	pushq %rax
	call A_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A1
	movq $str6, %rax
	pushq %rax
	movq $str7, %rax
	pushq %rax
	movq $16, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $descr_A, 0(%rax)
	pushq %rax
	call A_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	notq %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A1
	movq $1, %rax
	pushq %rax
	jmp A2
A1:
	pushq %rax
A2:
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str1, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	addq $0, %rsp
	movq $0, %rax
	ret
A_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq %rax, 8(%rcx)
	movq 24(%rbp), %rax
	popq %rbp
	ret
A_equals:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	popq %rdx
	popq %rcx
	cmpq %rdx, %rcx
	jz H1
	movq $0, %rax
	pushq %rax
	jmp F1
H1:
	movq $1, %rax
	pushq %rax
F1:
	popq %rax
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "ok\n"
str2:
	.string "toto"
str3:
	.string "titi"
str4:
	.string "toto"
str5:
	.string "toto"
str6:
	.string "toto"
str7:
	.string "titi"
str8:
	.string "toto"
str9:
	.string "toto"
descr_A:
	.quad 0
	.quad A_equals
