	.text
	.globl	main
main:
	subq $16, %rsp
	leaq 8(%rsp), %rbp
	movq $8, %rdi
	call malloc
	movq $descr_Morris, 0(%rax)
	pushq %rax
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $0, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $0, %rax
	pushq %rax
	movq $42, %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_Bintree, 0(%rax)
	pushq %rax
	call Bintree_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $0, %rax
	pushq %rax
	movq $41, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_Bintree, 0(%rax)
	pushq %rax
	call Bintree_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq -8(%rbp), %rax
	pushq %rax
	movq $43, %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_Bintree, 0(%rax)
	pushq %rax
	call Bintree_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $3, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $5, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $5, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $5, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq -8(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	addq $16, %rsp
	movq $0, %rax
	ret
Bintree_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 48(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq %rax, 16(%rcx)
	movq 40(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq %rax, 8(%rcx)
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq %rax, 24(%rcx)
	movq 24(%rbp), %rax
	popq %rbp
	ret
Bintree_perfect:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I1
	movq $0, %rax
	pushq %rax
	popq %rax
I1:
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_Bintree, 0(%rax)
	pushq %rax
	call Bintree_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	popq %rbp
	ret
Bintree_left:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $0, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L1
W1:
	movq 0(%rbp), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_Bintree, 0(%rax)
	pushq %rax
	call Bintree_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, 32(%rbp)
L1:
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setg %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W1
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	popq %rbp
	ret
Bintree_right:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $0, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L2
W2:
	movq $0, %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_Bintree, 0(%rax)
	pushq %rax
	call Bintree_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, 32(%rbp)
L2:
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setg %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W2
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	popq %rbp
	ret
Morris_inorder:
	pushq %rbp
	leaq -8(%rsp), %rbp
	jmp L3
W3:
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E1
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $str1, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_int_str
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 32(%rbp)
	jmp R1
E1:
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L4
W4:
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
L4:
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A1
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A1
	movq $1, %rax
	pushq %rax
	jmp A2
A1:
	pushq %rax
A2:
	popq %rax
	testq %rax, %rax
	jnz W4
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E2
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq %rax, 24(%rcx)
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 32(%rbp)
	jmp R2
E2:
	movq $0, %rax
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq %rax, 24(%rcx)
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $str2, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_int_str
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq 32(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	popq %rax
	movq %rax, 32(%rbp)
R2:
R1:
L3:
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W3
	movq $str3, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "."
str2:
	.string "."
str3:
	.string "\n"
descr_Morris:
	.quad 0
	.quad Morris_inorder
descr_Bintree:
	.quad 0
	.quad Bintree_perfect
	.quad Bintree_left
	.quad Bintree_right
