	.text
	.globl	main
main:
	subq $8, %rsp
	leaq 0(%rsp), %rbp
	movq $40, %rdi
	call malloc
	movq $0, 32(%rax)
	movq $descr_D, 0(%rax)
	pushq %rax
	call D_constr
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A13
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	movq $2, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A13
	movq $1, %rax
	pushq %rax
	jmp A14
A13:
	pushq %rax
A14:
	popq %rax
	testq %rax, %rax
	jz A11
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 24(%rcx), %rax
	pushq %rax
	movq $3, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A11
	movq $1, %rax
	pushq %rax
	jmp A12
A11:
	pushq %rax
A12:
	popq %rax
	testq %rax, %rax
	jz A7
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 32(%rcx), %rax
	pushq %rax
	movq $4, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A7
	movq $1, %rax
	pushq %rax
	jmp A8
A7:
	pushq %rax
A8:
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str1, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	addq $8, %rsp
	movq $0, %rax
	ret
A_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	popq %rbp
	ret
A_init_a:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $1, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 8(%rdi)
	popq %rbp
	ret
B_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $16, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	popq %rbp
	ret
B_init_b:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $2, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	popq %rbp
	ret
C_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $16, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	popq %rbp
	ret
C_init_c:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $3, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 24(%rdi)
	popq %rbp
	ret
D_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $16, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $32, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rax
	popq %rbp
	ret
D_init_d:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $4, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 32(%rdi)
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "ok\n"
descr_D:
	.quad descr_C
	.quad A_init_a
	.quad B_init_b
	.quad C_init_c
	.quad D_init_d
descr_C:
	.quad descr_B
	.quad A_init_a
	.quad B_init_b
	.quad C_init_c
descr_A:
	.quad 0
	.quad A_init_a
descr_B:
	.quad descr_A
	.quad A_init_a
	.quad B_init_b
