	.text
	.globl	main
main:
	subq $8, %rsp
	leaq 0(%rsp), %rbp
	movq $0, %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_ABR, 0(%rax)
	pushq %rax
	call ABR_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $17, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $5, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $8, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	movq $str4, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	movq $5, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A31
	movq $0, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	notq %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A31
	movq $1, %rax
	pushq %rax
	jmp A32
A31:
	pushq %rax
A32:
	popq %rax
	testq %rax, %rax
	jz A29
	movq $17, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A29
	movq $1, %rax
	pushq %rax
	jmp A30
A29:
	pushq %rax
A30:
	popq %rax
	testq %rax, %rax
	jz A25
	movq $3, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	notq %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A25
	movq $1, %rax
	pushq %rax
	jmp A26
A25:
	pushq %rax
A26:
	popq %rax
	testq %rax, %rax
	jz A17
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	movq $str7, %rax
	pushq %rax
	popq %rdx
	popq %rcx
	cmpq %rdx, %rcx
	jz H2
	movq $0, %rax
	pushq %rax
	jmp F2
H2:
	movq $1, %rax
	pushq %rax
F2:
	popq %rax
	testq %rax, %rax
	jz A17
	movq $1, %rax
	pushq %rax
	jmp A18
A17:
	pushq %rax
A18:
	popq %rax
	testq %rax, %rax
	jz I7
	movq $str5, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I7:
	addq $8, %rsp
	movq $0, %rax
	ret
ABR_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 40(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 8(%rdi)
	movq 48(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 24(%rdi)
	movq 24(%rbp), %rax
	popq %rbp
	ret
ABR_insere:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I1
	movq $0, %rax
I1:
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setl %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E1
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E2
	movq $0, %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_ABR, 0(%rax)
	pushq %rax
	call ABR_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	jmp R2
E2:
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
R2:
	jmp R1
E1:
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E3
	movq $0, %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq $32, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $0, 24(%rax)
	movq $descr_ABR, 0(%rax)
	pushq %rax
	call ABR_constr
	addq $32, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 24(%rdi)
	jmp R3
E3:
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
R3:
R1:
	popq %rbp
	ret
ABR_contient:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I2
	movq $1, %rax
	pushq %rax
	popq %rax
I2:
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setl %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A1
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A1
	movq $1, %rax
	pushq %rax
	jmp A2
A1:
	pushq %rax
A2:
	popq %rax
	testq %rax, %rax
	jz I3
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
I3:
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I4
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
I4:
	movq $0, %rax
	pushq %rax
	popq %rax
	popq %rbp
	ret
ABR_toStr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $str1, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I5
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
I5:
	movq 0(%rbp), %rax
	pushq %rax
	movq $str3, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat_str_int
	pushq %rax
	movq $str2, %rax
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I6
	movq 0(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 24(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rsi
	popq %rdi
	call concat
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
I6:
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string ""
str2:
	.string ")"
str3:
	.string "("
str4:
	.string "\n"
str5:
	.string "ok\n"
str6:
	.string "(1)(5)(8)(17)"
str7:
	.string "(1)(5)(8)(17)"
descr_ABR:
	.quad 0
	.quad ABR_insere
	.quad ABR_contient
	.quad ABR_toStr
