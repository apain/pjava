	.text
	.globl	main
main:
	subq $32, %rsp
	leaq 24(%rsp), %rbp
	movq $1, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A3
	movq $0, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A3
	movq $1, %rax
	pushq %rax
	jmp A4
A3:
	pushq %rax
A4:
	popq %rax
	testq %rax, %rax
	jnz O1
	movq $1, %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rax
	notq %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O1
	movq $0, %rax
	pushq %rax
	jmp O2
O1:
	pushq %rax
O2:
	popq %rax
	movq %rax, 0(%rbp)
	movq $1, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A5
	movq $0, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O9
	movq $1, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O9
	movq $0, %rax
	pushq %rax
	jmp O10
O9:
	pushq %rax
O10:
	movq $0, %rax
	pushq %rax
	popq %rax
	notq %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A5
	movq $1, %rax
	pushq %rax
	jmp A6
A5:
	pushq %rax
A6:
	popq %rax
	movq %rax, -8(%rbp)
	movq $1, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A7
	movq $0, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O17
	movq $0, %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O17
	movq $0, %rax
	pushq %rax
	jmp O18
O17:
	pushq %rax
O18:
	movq $0, %rax
	pushq %rax
	popq %rax
	notq %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A7
	movq $1, %rax
	pushq %rax
	jmp A8
A7:
	pushq %rax
A8:
	popq %rax
	movq %rax, -16(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A9
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O21
	movq -16(%rbp), %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz O21
	movq $0, %rax
	pushq %rax
	jmp O22
O21:
	pushq %rax
O22:
	popq %rax
	notq %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A9
	movq $1, %rax
	pushq %rax
	jmp A10
A9:
	pushq %rax
A10:
	popq %rax
	notq %rax
	pushq %rax
	popq %rax
	movq %rax, -24(%rbp)
	movq -24(%rbp), %rax
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str1, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	addq $32, %rsp
	movq $0, %rax
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "ok\n"
