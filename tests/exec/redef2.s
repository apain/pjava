	.text
	.globl	main
main:
	subq $24, %rsp
	leaq 16(%rsp), %rbp
	movq $16, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $descr_A, 0(%rax)
	pushq %rax
	call A_constr
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $24, %rdi
	call malloc
	movq $0, 16(%rax)
	movq $descr_B, 0(%rax)
	pushq %rax
	call B_constr
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq $24, %rdi
	call malloc
	movq $descr_C, 0(%rax)
	pushq %rax
	call C_constr
	addq $8, %rsp
	pushq %rax
	popq %rax
	movq %rax, -16(%rbp)
	movq $1, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $2, %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq $3, %rax
	pushq %rax
	movq -16(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A29
	movq -8(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A29
	movq $1, %rax
	pushq %rax
	jmp A30
A29:
	pushq %rax
A30:
	popq %rax
	testq %rax, %rax
	jz A27
	movq -8(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	movq $4, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A27
	movq $1, %rax
	pushq %rax
	jmp A28
A27:
	pushq %rax
A28:
	popq %rax
	testq %rax, %rax
	jz A23
	movq -16(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 8(%rcx), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A23
	movq $1, %rax
	pushq %rax
	jmp A24
A23:
	pushq %rax
A24:
	popq %rax
	testq %rax, %rax
	jz A15
	movq -16(%rbp), %rax
	pushq %rax
	popq %rcx
	movq 16(%rcx), %rax
	pushq %rax
	movq $9, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz A15
	movq $1, %rax
	pushq %rax
	jmp A16
A15:
	pushq %rax
A16:
	popq %rax
	testq %rax, %rax
	jz I1
	movq $str1, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
I1:
	addq $24, %rsp
	movq $0, %rax
	ret
A_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	popq %rbp
	ret
A_set_a:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq %rax, 8(%rcx)
	popq %rbp
	ret
B_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	popq %rbp
	ret
B_set_a:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq $2, %rax
	pushq %rax
	popq %rbx
	popq %rax
	imulq %rbx, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	popq %rbp
	ret
C_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rax
	popq %rbp
	ret
C_set_a:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq $3, %rax
	pushq %rax
	popq %rbx
	popq %rax
	imulq %rbx, %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "ok\n"
descr_C:
	.quad descr_B
	.quad C_set_a
	.quad C_set_a
	.quad C_set_a
descr_A:
	.quad 0
	.quad A_set_a
descr_B:
	.quad descr_A
	.quad B_set_a
	.quad B_set_a
