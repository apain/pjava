	.text
	.globl	main
main:
	subq $0, %rsp
	leaq -8(%rsp), %rbp
	movq $42, %rax
	pushq %rax
	movq $16, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $descr_Pascal, 0(%rax)
	pushq %rax
	call Pascal_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $24, %rcx
	call *0(%rcx)
	addq $8, %rsp
	pushq %rax
	popq %rax
	addq $0, %rsp
	movq $0, %rax
	ret
List_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I1
	movq $0, %rax
I1:
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq $24, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $descr_List, 0(%rax)
	pushq %rax
	call List_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 16(%rdi)
	movq 24(%rbp), %rax
	popq %rbp
	ret
List_get:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I2
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rax
I2:
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rax
	popq %rbp
	ret
List_set:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 40(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E1
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	movq %rax, 8(%rdi)
	jmp R1
E1:
	movq 40(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 16(%rdi), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	popq %rax
R1:
	popq %rbp
	ret
Pascal_constr:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 32(%rbp), %rax
	pushq %rax
	popq %rax
	movq 24(%rbp), %rdi
	pushq %rdi
	popq %rcx
	movq %rax, 8(%rcx)
	movq 24(%rbp), %rax
	popq %rbp
	ret
Pascal_print_row:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $0, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp L1
W1:
	movq 0(%rbp), %rax
	pushq %rax
	movq 40(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setne %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E2
	movq $str1, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	jmp R2
E2:
	movq $str2, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
R2:
	movq 0(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
L1:
	movq 0(%rbp), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setle %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W1
	movq $str3, %rax
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	popq %rbp
	ret
Pascal_compute_row:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq $0, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	sete %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz E3
	movq $1, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	jmp R3
E3:
	movq 32(%rbp), %rax
	pushq %rax
	movq 40(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq 40(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $8, %rcx
	call *0(%rcx)
	addq $16, %rsp
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	movq $7, %rax
	pushq %rax
	popq %rbx
	popq %rax
	movq $0, %rdx
	idivq %rbx
	pushq %rdx
	popq %rax
	movq %rax, 0(%rbp)
R3:
	movq 32(%rbp), %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	movq 40(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	popq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setg %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz I3
	movq 40(%rbp), %rax
	pushq %rax
	movq 32(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	popq %rax
I3:
	popq %rbp
	ret
Pascal_run:
	pushq %rbp
	leaq -8(%rsp), %rbp
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	movq $24, %rdi
	call malloc
	movq $0, 8(%rax)
	movq $0, 16(%rax)
	movq $descr_List, 0(%rax)
	pushq %rax
	call List_constr
	addq $16, %rsp
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $0, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	jmp L2
W2:
	movq -8(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	movq 0(%rbp), %rax
	pushq %rax
	popq %rax
	movq 0(%rax), %rcx
	pushq %rax
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $16, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	popq %rax
	movq 0(%rbp), %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rax
	pushq %rax
	movq 0(%rax), %rcx
	addq $8, %rcx
	call *0(%rcx)
	addq $24, %rsp
	pushq %rax
	popq %rax
	movq -8(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
L2:
	movq -8(%rbp), %rax
	pushq %rax
	movq 24(%rbp), %rdi
	movq 8(%rdi), %rax
	pushq %rax
	popq %rbx
	popq %rcx
	movq $0, %rax
	cmpq %rbx, %rcx
	setl %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jnz W2
	popq %rbp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
	jmp int_str_end
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
	jmp str_int_end
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "*"
str2:
	.string "0"
str3:
	.string "\n"
descr_List:
	.quad 0
	.quad List_get
	.quad List_set
descr_Pascal:
	.quad 0
	.quad Pascal_print_row
	.quad Pascal_compute_row
	.quad Pascal_run
