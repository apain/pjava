	.text
	.globl	main
main:
	subq $16, %rsp
	leaq 8(%rsp), %rbp
	movq $10, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	movq $20, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $30, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	movq -8(%rbp), %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	popq %rdi
	call print_int
	addq $16, %rsp
	movq $0, %rax
	ret
print_int:
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .Sprint_int, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
	ret
	.data
x:
	.quad 1
y:
	.quad 1
.Sprint_int:
	.string "%d\n"
