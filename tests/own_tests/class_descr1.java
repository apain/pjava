

class A {
    int valeur1;
    int valeur2;

    A(int v1, int v2) { valeur1 = v1; valeur2 = v2; }

    int add(int v, int a) { return v + a; }

}

class Main {

    public static void main(String[] args) {

	A test = new A(2, 40);
	int result = test.add(22, 20);
	System.out.print(result + "\n");
	}

}

