	.text
	.globl	main
main:
	subq $0, %rsp
	#leaq 0(%rsp), %rbp
	movq $40, %rax
	pushq %rax
	movq $2, %rax
	pushq %rax
	movq $24, %rdi
	call malloc
	#pushq %rax
	movq %rax, %rdi
	call print_int
	#call A_constr
	movq %rax, %rdi	
	#pushq %rdi
	#popq %rax
	#movq %rax, 0(%rbp)
	#movq 0(%rbp), %rax
	#pushq %rax
	#popq %rcx
	popq %r9
	popq %r10
	#popq %r11
	movq 8(%rdi), %rdi
	movq $str1, %rsi
	#pushq %rax
	#popq %rsi
	#popq %rdi
	call concat_int_str
	pushq %rax
	popq %rdi
	movq $0, %rax
	call printf
	addq $0, %rsp
	movq $0, %rax
	ret
A_constr:
	#subq $0, %rsp
	#leaq -8(%rsp), %rbp
	popq %rdi
	popq %rcx
	movq %rcx, 16(%rdi)
	popq %rdx
	movq %rdx, 8(%rdi)
	#movq 8(%rbp), %rdi
	#movq 24(%rbp), %rax
	#pushq %rax
	#popq %rax
	#movq %rax, 8(%rdi)
	#movq 16(%rbp), %rax
	#pushq %rax
	#popq %rax
	#movq %rax, 16(%rdi)
	movq %rdi, %rax
	#addq $0, %rsp
	ret
print_int:
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .Sprint_int, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
	ret
concat:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
concat_int_str:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	movq -16(%rbp), %rdi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -8(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_int_str
	call sprintf
align_stack_int_str:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
	jmp int_str_end
int_str_end:
	movq -32(%rbp), %rdi
	call strlen
	pushq %rax
	movq -32(%rbp), %rdi
	addq -40(%rbp), %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rax
	addq $40, %rsp
	popq %rbp
	ret
concat_str_int:
	pushq %rbp
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack_str_int
	call sprintf
align_stack_str_int:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
	jmp str_int_end
str_int_end:
	movq -32(%rbp), %rax
	addq $32, %rsp
	popq %rbp
	ret
	.data
.Sprint_int:
	.string "%d"
str1:
	.string "\n"
