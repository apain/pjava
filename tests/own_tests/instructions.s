	.text
	.globl	main
main:
	subq $16, %rsp
	leaq 8(%rsp), %rbp
	movq $4, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq $5, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq $55, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq $1, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq $5555, %rax
	pushq %rax
	popq %rax
	movq %rax, -8(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rdi
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .str_int1, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
	movq -8(%rbp), %rax
	pushq %rax
	popq %rdi
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .str_int2, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
	addq $16, %rsp
	movq $0, %rax
	ret
	.data
.str_int1:
	.string "%d\n"
.str_int2:
	.string "%d\n"
