	.text
	.globl	main
main:
	subq $8, %rsp
	leaq 0(%rsp), %rbp
	movq $4, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rax
	cmpq %rbx, %rax
	setl %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz L1
	movq 0(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	subq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
L1:
	movq 0(%rbp), %rax
	pushq %rax
	movq $0, %rax
	pushq %rax
	popq %rbx
	popq %rax
	cmpq %rbx, %rax
	setg %al
	pushq %rax
	popq %rax
	testq %rax, %rax
	jz L2
	movq 0(%rbp), %rax
	pushq %rax
	movq $1, %rax
	pushq %rax
	popq %rbx
	popq %rax
	addq %rbx, %rax
	pushq %rax
	popq %rax
	movq %rax, 0(%rbp)
	movq 0(%rbp), %rax
	pushq %rax
	popq %rdi
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .str_int1, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
L2:
	addq $8, %rsp
	movq $0, %rax
	ret
	.data
.str_int1:
	.string "%d\n"
