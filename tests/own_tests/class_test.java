

class A
{

    int valeur;

    void changerValeur(int newValeur)
    {
	    valeur = newValeur;
    }

}

class B extends A
{

	int valeur2;

    void incrValeur()
    {
	    valeur = valeur + 1;
    }
    void changerValeur2()
    { 
	    valeur2 = 30;
    }

}

class Main
{
	public static void main (String[] args) {
	A a = new A();
	a.changerValeur(10);
	B b = new B();
	b.changerValeur(41);
	b.incrValeur();
	b.changerValeur2();
	System.out.println(b.valeur);
	System.out.println(b.valeur2);
	}
}
