        .text
        .globl  main
main:
        call    print_int
        ret

        ## une routine pour afficher un entier (%rdi) avec printf
print_int:
        mov     %rdi, %rsi
        mov     $message, %rdi  # arguments pour printf
        mov     $0, %rax
        call    printf
        ret

        .data
message:
        .string "Hello World!\n"
