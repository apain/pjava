	.text
	.globl	main
main:
	movq $str1, %rdi
	movq $str2, %rsi
	call concat
	movq $0, %rax
	ret

concat:
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -16(%rbp), %rdi
	call strlen
	movq %rax, %r10
	addq -24(%rbp), %r10
	movq %r10, %rdi
	call malloc
	pushq %rax 
	movq -32(%rbp), %rdi
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	movq -24(%rbp), %rcx
	addq %rcx, %rdi
	movq -16(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	call puts
	addq $32, %rsp
	movq $0, %rax
	ret

print_int:
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .Sprint_int, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
	ret

	.data
.Sprint_int:
	.string "%d \n"
str1:
	.string "1234"
str2:
	.string "abcd"
