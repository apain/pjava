	.text
	.globl	main
main:
	movq $str, %rdi
	movq $123, %rsi
	call concat_str_int
	movq $0, %rax
	ret

concat_str_int:
	movq %rsp, %rbp
	pushq %rdi
	pushq %rsi
	call strlen
	pushq %rax
	movq -24(%rbp), %rdi
	addq $21, %rdi
	movq $0, %rax
	call malloc
	pushq %rax
	movq -32(%rbp), %rdi 
	movq -8(%rbp), %rsi
	call strcpy
	movq -32(%rbp), %rdi
	addq -24(%rbp), %rdi
	leaq .Sprint_int, %rsi
	movq -16(%rbp), %rdx
	movq $0, %rax
	movq $8, %rcx
	testq %rsp, %rcx
	jne align_stack
	call sprintf
end:
	movq -32(%rbp), %rdi
	call puts
	addq $32, %rsp
	movq $0, %rax
	ret

align_stack:
	subq $8, %rsp
	call sprintf
	addq $8, %rsp
	jmp end

print_int:
	subq $8, %rsp
	movq %rdi, %rsi
	leaq .Sprint_int, %rdi
	movq $0, %rax
	call printf
	addq $8, %rsp
	ret

	.data
.Sprint_int:
	.string "%d"
str:
	.string "abcde"
