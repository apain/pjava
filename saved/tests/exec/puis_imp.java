class Puiss {
    Puiss() {}
    int puis(int a, int n) {
	int r = 1;
	while (n != 0) {
          if (n % 2 == 1)
	    r = r * a;
          a = a * a;
          n = n / 2;
        }
	return r;
    }
}

class Main {
    public static void main(String[] args) {
        Puiss p = new Puiss();
	if (p.puis(2, 8) == 256 && p.puis(6, 3) == 216)
	    System.out.print("ok\n");
    }
}
