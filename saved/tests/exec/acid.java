interface I { void m(); }
interface J extends I { }
interface K { void n(); }
interface L extends J, K { void o(); }

class A implements J {
  public void m() { System.out.print("A.m\n"); }
}
class B {}
class C { void subtype(I x) { x.m(); } }

class D<X extends I, Y extends X> {
  void expectsX(X x) {}
  void subtype1(Y y) { expectsX(y); }
  void expectsI(I i) {}
  void subtype2(X x) { expectsI(x); }
}

interface C1 { void m(); }
interface C2 { void m(); }
interface I1 extends C1, C2 {}

class E<X> { void m() { X x = null; } }

class Main {
    public static void main(String[] args) {
      new C().subtype(new A());
      new Object();
    }
}
