
/*** listes circulaires doublement chaînées ***/

class ListeC
{

    int valeur;
    ListeC suivant;
    ListeC precedent;

    /* liste réduite à un élément */
    ListeC(int v)
    {
	valeur = v;
	suivant = precedent = this;
    }

    /* insertion après un élément donnée */
    void insererApres(int v)
    {
	ListeC e = new ListeC(v);
	e.suivant = suivant;
	suivant = e;
	e.suivant.precedent = e;
	e.precedent = this;
    }

    /* suppression d'un élément donné */
    void supprimer()
    {
	precedent.suivant = suivant;
	suivant.precedent = precedent;
    }

    /* affichage */
    void afficher()
    {
	ListeC c = this;
	System.out.print(c.valeur + " ");
	c = c.suivant;
	while (c != this)
        {
	    System.out.print(c.valeur + " ");
	    c = c.suivant;
        }
	System.out.print("\n");
    }

}

/*** Partie 3 : problème de Josephus ***/

class Josephus
{

    Josephus() {}

    /* construction de la liste circulaire 1,2,...,n;
       l'élément retourné est celui contenant 1 */
    ListeC cercle(int n)
    {
	ListeC l = new ListeC(1);
        int i = n;
	while (i >= 2)
	{
	    l.insererApres(i);
            i = i - 1;
	}
	return l;
    }

    /* jeu de Josephus */
    int josephus(int n, int p)
    {
	/* c est le joueur courant, 1 au départ */
	ListeC c = cercle(n);

	/* tant qu'il reste plus d'un joueur */
	while (c != c.suivant)
	{
	    /* on élimine un joueur */
            int i = 1;
	    while (i < p) { c = c.suivant; i = i + 1; }
	    c.supprimer();
	    System.out.print(c.valeur + " sort\n");
	    c = c.suivant;
	}
	System.out.print("le gagnant est " + c.valeur + "\n");
	return c.valeur;
    }

}

/*** Tests ***/

class Main
{

    public static void main(String[] args)
    {
 	ListeC l = new ListeC(1);
 	l.afficher();
 	l.insererApres( 3);
 	l.afficher();
 	l.insererApres(2);
 	l.afficher();
 	l.suivant.supprimer();
 	l.afficher();

        Josephus j = new Josephus();
	ListeC c = j.cercle(7);
	c.afficher();

	if (j.josephus(7, 5) == 6 &&
	    j.josephus(5, 5) == 2 &&
	    j.josephus(5, 17) == 4 &&
	    j.josephus(13, 2) == 11)
	    System.out.print("ok\n");
    }

}
