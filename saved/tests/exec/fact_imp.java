class Fact {
    Fact() {}
    int fact_imp(int n) {
	int res = 1;
	while (n > 1) {
	    res = res * n;
            n = n - 1;
        }
	return res;
    }
}
class Main {
    public static void main(String[] args) {
        Fact f = new Fact();
	if (f.fact_imp(5) == 120)
	    System.out.print("ok\n");
    }
}
