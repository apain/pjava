
class A {}
class B extends A {}

interface I { A m(); }
interface J { B m(); }
interface K extends I, J {}

class C implements K { public B m() { return null; } }
class Main { public static void main(String[] args) { } }
