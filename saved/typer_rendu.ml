open Ast

type pj_typ = 
        | Tvoid
        | Tboolean
        | Tint
        | Tstring 
        | Ttypenull
        | Tnew of ident * ntype list option (* can be followed by a list of type parameters *)

(* == TYPED AST == *)

type texpr = {
        e_tdesc: e_tdesc;
        e_typ: pj_typ;
        }

and taccess = {
        a_tdesc: a_tdesc;
        a_typ: pj_typ;
        }

and e_tdesc =
  | TSEconstant of constant
  | TSEbool of bool
  | TSEthis
  | TSEnew of ntype * texpr list
  | TEaccessExpr of taccess * texpr
  | TSEmethodCall of taccess * texpr list
  | TSEaccess of taccess
  | TEnull
  | TEunop of unop * texpr
  | TEbinop of binop * texpr * texpr
  | TEprint of texpr

and a_tdesc =
  | TAident of ident 
  | TAattr of texpr * ident

(* == DATA STRUCTURES == *)

exception Typing_error of loc * string

module Smap = Map.Make(String)
module Sset = Set.Make(String)

type method_env = { ret_typ : pj_typ; formals : parameter list ; m_vars : pj_typ Smap.t }
let empty_method_env = { ret_typ = Ttypenull; formals = []; m_vars = Smap.empty }

type class_env = { is_class : bool; name : ident; extends : ntype option; implements : ntype list option; type_vars : paramtype list option; methods : method_env Smap.t; fields : pj_typ Smap.t } 
let empty_class_env = { is_class = true; name = ""; extends = Some (Nntype ("Object", None)); implements = None; type_vars = None; methods = Smap.empty; fields = Smap.empty }

type full_env = class_env Smap.t
let empty_env = Smap.empty 

let fake_pos = ({ Lexing.pos_fname = "fake"; Lexing.pos_lnum = 1000; Lexing.pos_bol = 1000; Lexing.pos_cnum = 1000 }, { Lexing.pos_fname = "fake"; Lexing.pos_lnum = 1000; Lexing.pos_bol = 1000; Lexing.pos_cnum = 1000 })

(* == USEFUL FUNCTIONS == *)

let get_typ t = match t with
        | Tbool -> Tboolean
        | Tint -> Tint
        | Tstring -> Tstring
        | Tntype (Nntype (id, n_list)) -> Tnew (id, n_list)

let print_typ t = match t with
        | Tvoid -> "void"
        | Tboolean -> "bool"
        | Tint -> "int"
        | Tstring -> "String"
        | Ttypenull -> "<null>"
        | Tnew (id, _) -> id

let print_incompatible tau1 tau2 =
        "type " ^ print_typ tau1 ^ " is incompatible with type " ^ print_typ tau2 

let type_extends env class_env id1 id2 =
        (* returns true if id1 and id2 are type parameters and id1 extends id2 *)
        let type_vars = class_env.type_vars in
        begin match type_vars with 
        | None -> false
        | Some paramtype_list -> 
                        List.exists
                        (fun type_var -> begin match type_var with
                        | Pparamtype (var_id, Some nt_list) when var_id = id1 ->
                                        List.exists 
                                        (fun nt -> match nt with
                                        | Nntype (nt_id, None) when nt_id = id2 -> true
                                        | _ -> false)
                                        nt_list
                        | _ -> false
                        end
                        )
                        paramtype_list
        end

let rec is_superclass env a b = 
        if a = b then true
        else if b = "Object" then false (* reached the top *)
        else
        let class_b = Smap.find b env in
        let scl = class_b.extends in
        begin
        match scl with
        | None -> false
        | Some (Nntype (c, None)) -> c = a || is_superclass env a c
        | Some (Nntype (c, Some _)) -> c = a || is_superclass env a c 
        end

let rec is_superinterface env a b =
        if a = b then true
        else let intf_b = Smap.find b env in
        begin match intf_b.implements with
        | None -> false
        | Some nt_list -> 
                List.exists 
                (fun (Nntype (c, _)) -> c = a || is_superinterface env a c)
                nt_list
        end

let implements env class_id intf_id =
        (* returns true if class implements interface *)
        begin try (
        let dir_implements class_id intf_id = 
                let class_env = Smap.find class_id env in
                let implements = class_env.implements in
                begin match implements with
                | None -> false
                | Some (impl_list) ->
                                List.exists
                                (fun (Nntype (nt_id, _)) 
                                -> nt_id = intf_id
                                )
                                impl_list
                end 
        in 
        if dir_implements class_id intf_id then true 
        else
        let class_env = Smap.find class_id env in
        let extends = class_env.extends in 
        begin match extends with
        | None -> false
        | Some (Nntype (superclass_id, _)) -> dir_implements superclass_id intf_id
        end
        ) with Not_found -> failwith "anomaly" end

let is_type_var class_env nt_id =
        match class_env.type_vars with
        | None -> false
        | Some p_list -> List.exists
        (fun (Pparamtype (id, _)) -> id = nt_id)
        p_list

let is_instance env class_env id1 id2 = 
        (* returns true if class/interface id1 instantiates type parameter id2 *)
        let model = Smap.find id1 env in
        if model.is_class then 
        let is_extends =
                let extends = class_env.extends in
                begin match extends with
                | None -> false
                | Some (Nntype (nt_id, Some nt_list)) -> 
                        begin match class_env.type_vars with
                                | None -> false
                                | Some p_list -> 
                                List.length nt_list = List.length p_list
                                && 
                                List.exists2
                                (fun (Nntype (nt_id1, _)) (Pparamtype (nt_id2, _)) -> nt_id1 = id1 && nt_id2 = id2)
                                nt_list 
                                p_list 
                        end
                | Some (Nntype (nt_id, None)) -> false
                end
        in
        if is_extends then true
        else
        let match_type_vars type_vars1 type_vars2 id1 id2 =
                begin match type_vars2 with
                | None -> false
                | Some type_var_list2 ->
                List.length type_vars1 = List.length type_var_list2 &&
                List.exists2
                (fun (Nntype (nt_id1, _)) (Pparamtype (nt_id2, _))
                -> nt_id1 = id1 && nt_id2 = id2
                )
                type_vars1
                type_var_list2
                end
        in
        let extends = class_env.extends in
        begin match extends with
        | Some (Nntype (nt_id, Some nt_list)) -> 
                begin try (let superclass_env = Smap.find nt_id env in
                match_type_vars nt_list superclass_env.type_vars id1 id2
                ) with Not_found -> failwith "anomaly" end
        | _ -> false
        end
        else (* type var extends interface = implements interface *)
        begin match class_env.type_vars with
        | Some paramtype_list ->
                List.exists
                (fun (Pparamtype (p_id, extend_list)) ->
                begin match extend_list with
                | None -> false
                | Some nt_list -> List.exists (fun (Nntype (nt_id, _)) ->
                        p_id = id1 && nt_id = id2) nt_list
                end
                )
                paramtype_list
        | None -> false
                end

let is_type_var_instance env ntype =
        match ntype with
        | Nntype (nt_id, Some nt_list) ->
                begin try (let model_class = Smap.find nt_id env in 
                begin match model_class.type_vars with
                | Some paramtype_list ->
                List.length paramtype_list = List.length nt_list &&
                List.for_all2
                (fun (Pparamtype (pt_id, extends_list)) (Nntype (instance_id, instance_ext_list))
                -> if pt_id = instance_id then true else 
                        begin match extends_list with
                        | Some ext_list ->
                        List.exists
                        (fun ext -> begin match ext with
                        | Nntype (extend_id, _) -> instance_id = extend_id || is_superclass env extend_id instance_id || implements env instance_id extend_id
                        end
                        )
                        ext_list
                        | None -> begin match instance_ext_list with
                                | None -> true
                                | Some _ -> false
                        end
                end
                )
                paramtype_list
                nt_list
                | None -> false
                end) with Not_found -> failwith "anomaly" end
        | Nntype (nt_id, None) ->
                begin try (let model_class = Smap.find nt_id env in
                begin match model_class.type_vars with
                | None -> true
                | Some _ -> false
                end) with Not_found -> failwith "anomaly" end

let sub_type env class_env tau1 tau2 =
        if tau1 = tau2 then true 
        else
        begin match tau1 with
        | Tnew (id1, None) ->
                begin match tau2 with
                | Tnew (id2, None) -> 
                if Smap.mem id1 env && Smap.mem id2 env then is_superclass env id2 id1 (* either they are both classes/interfaces *)
                else if not (Smap.mem id1 env) && not (Smap.mem id2 env) then type_extends env class_env id2 id1 (* or they are type params *) 
                else if Smap.mem id1 env && not (Smap.mem id2 env) then is_instance env class_env id1 id2 (* or one instantiates the other *)
                else if not (Smap.mem id1 env) && Smap.mem id2 env then is_instance env class_env id2 id1 
                else false
                | Tnew (id2, Some nt_list2) -> if Smap.mem id1 env && Smap.mem id2 env then 
                        if is_superclass env id2 id1 
                        then 
                        let scl_extends = (Smap.find id1 env).extends in
                        begin match scl_extends with
                        | None -> false
                        | Some (Nntype (_, None)) -> false
                        | Some (Nntype (scl_id, Some scl_type_vars)) ->
                                List.for_all2 
                                (fun (Nntype (nt_id, _)) (Nntype (tvar_id, _)) ->
                                        nt_id = tvar_id (* fix this *)
                                )
                                nt_list2
                                scl_type_vars
                        end
                        else false
                else if not (Smap.mem id1 env) && not (Smap.mem id2 env) then type_extends env class_env id2 id1 
(*                else if Smap.mem id1 env && not (Smap.mem id2 env) then is_instance_nt env class_env (Nntype (id1, None)) (Nntype (id2, Some nt_list))
                else if not (Smap.mem id1 env) && Smap.mem id2 env then is_instance_nt env class_env (Nntype (id2, Some nt_list)) (Nntype (id1, None)) (* or one instantiates the other *)*)
                else false
                | Ttypenull -> true
                | _ -> false 
                end
        | Tnew (id1, Some x) -> 
                begin match tau2 with
                | Tnew (id2, None) -> is_superclass env id2 id1
                | Tnew (id2, Some _) -> is_superclass env id2 id1 
                | Ttypenull -> true
                | _ -> false 
                end
        | Ttypenull -> true
        | _ -> false
        end

let compatible env class_env tau1 tau2 =
        tau1 = tau2 || sub_type env class_env tau1 tau2 || sub_type env class_env tau2 tau1 

let well_formed env class_env t =
        match t with
        | Tbool | Tint | Tstring -> true
        | Tntype nt -> let (Nntype (id1, n_list)) = nt in if Smap.mem id1 env then is_type_var_instance env nt 
                else is_type_var class_env id1

let find_field env nt_id attr_id loc =
        (* goes through fields of superclasses *)
        let rec find_f nt_id attr_id =
                begin try (
                let class_env = Smap.find nt_id env in
                if Smap.mem attr_id class_env.fields then Smap.find attr_id class_env.fields
                else let scl = class_env.extends in
                        begin match scl with
                        | None | Some (Nntype ("Object", _)) -> raise (Typing_error (loc, "reached Object or no superclass, field not found in any parents while looking for " ^ attr_id))
                        | Some (Nntype (cls_id, _)) -> find_f cls_id attr_id
                        end
                ) with Not_found -> raise (Typing_error (loc, "class " ^ nt_id ^ " not found")) end
        in find_f nt_id attr_id

(* == TYPING DOWN THE AST == *)
(* type_expr and type_access return new trees decorated with types *)

let rec type_expr env class_env method_var_env e = 
        let d, tau = compute_type_expr env class_env method_var_env e in
        { e_tdesc = d; e_typ = tau }

and compute_type_expr env class_env method_var_env e =
        begin match e.e_desc with
        | Enull -> TEnull, Ttypenull
        | SEconstant c -> begin match c with
                | Cint n -> TSEconstant (Cint n), Tint
                | Cstring s -> TSEconstant (Cstring s), Tstring
                end 
        | SEbool b -> TSEbool b, Tboolean
        | SEthis -> if class_env.name = "Main" then raise (Typing_error (e.e_loc, "non-static variable this cannot be referenced from a static context"))
                else TSEthis, Tnew (class_env.name, None)
        | SEnew (nt, e_list) -> let te_list =
                List.map (fun e -> type_expr env class_env method_var_env e) e_list in
                if check_constr_call env nt te_list e.e_loc then
                let Nntype (nt_id, nt_list) = nt in 
                TSEnew (nt, te_list), Tnew (nt_id, nt_list)
                else raise (Typing_error (e.e_loc, "not the right arguments for constructor"))
        | EaccessExpr (a, e) -> let tau = type_access env class_env method_var_env a in
                let t_expr = type_expr env class_env method_var_env e in 
                TEaccessExpr (tau, t_expr ), tau.a_typ
        | SEaccess a -> let tau = type_access env class_env method_var_env a in 
                TSEaccess tau, tau.a_typ
        | SEmethodCall (a, e_list) -> type_method_call env class_env method_var_env a e_list
        | Eunop (unop, e1) -> let tau = type_expr env class_env method_var_env e1 in
                 begin match unop with
                | Uneg -> 
                        begin match tau.e_typ with
                        | Tint -> TEunop (unop, tau), Tint
                        | _ -> raise (Typing_error (e1.e_loc, "bad argument type. expected: int. found: " ^ print_typ tau.e_typ))
                        end
                | Unot -> begin match tau.e_typ with 
                        | Tboolean -> TEunop (unop, type_expr env class_env method_var_env e1), Tboolean 
                        | _ -> raise (Typing_error (e1.e_loc, "bad argument type. expected: bool. found: " ^ print_typ tau.e_typ))
                        end
                end
        | Ebinop (binop, e1, e2) -> let tau1 = type_expr env class_env method_var_env e1 in
                let tau2 = type_expr env class_env method_var_env e2 in
                begin match binop with
                | Beq | Bneq when compatible env class_env tau1.e_typ tau2.e_typ -> TEbinop (binop, tau1, tau2), Tboolean
                | Beq | Bneq -> raise (Typing_error (e1.e_loc, "unsupported operand types: " ^ print_incompatible tau1.e_typ tau2.e_typ)) 
                | Blt | Ble | Bgt | Bge -> begin match tau1.e_typ with
                        | Tint -> begin match tau2.e_typ with
                                | Tint -> TEbinop (binop, tau1, tau2), Tboolean
                                | _ -> raise (Typing_error (e2.e_loc, "unsupported operand type. expected: int. found: " ^ print_typ tau2.e_typ))
                                end
                        | _ -> raise (Typing_error (e1.e_loc, "unsupported operand type. expected: int. found: " ^ print_typ tau1.e_typ))
                        end
                | Bsub | Bmul | Bdiv | Bmod -> begin match tau1.e_typ with
                        | Tint -> begin match tau2.e_typ with
                                | Tint -> TEbinop (binop, tau1, tau2), Tint
                                | _ -> raise (Typing_error (e2.e_loc, "unsupported operand type. expected: int. found: " ^ print_typ tau2.e_typ))
                                end
                        | _ -> raise (Typing_error (e1.e_loc, "unsupported operand type. expected: int. found: " ^ print_typ tau1.e_typ))
                        end
                | Band | Bor -> begin match tau1.e_typ with
                        | Tboolean -> begin match tau2.e_typ with
                                | Tboolean -> TEbinop (binop, tau1, tau2), Tboolean
                                | _ -> raise (Typing_error (e2.e_loc, "unsupported operand type. expected: bool. found: " ^ print_typ tau2.e_typ))
                                end
                        | _ -> raise (Typing_error (e1.e_loc, "unsupported operand type. expected: bool. found: " ^ print_typ tau1.e_typ))
                        end
                | Badd -> begin match tau1.e_typ with
                        | Tstring -> begin match tau2.e_typ with
                                | Tstring | Tint -> TEbinop (binop, tau1, tau2), Tstring
                                | _ -> raise (Typing_error (e1.e_loc, "unspported operand type. expected: string or int. found: " ^ print_typ tau2.e_typ))
                                end
                        | Tint -> begin match tau2.e_typ with
                                | Tstring -> TEbinop (binop, tau1, tau2), Tstring
                                | Tint -> TEbinop (binop, tau1, tau2), Tint
                                | _ -> raise (Typing_error (e2.e_loc, "unspported operand type. expected: string or int. found: " ^ print_typ tau2.e_typ))
                                end
                        | _ -> raise (Typing_error (e1.e_loc, "unsupported operand type. expected: string or int. found: " ^ print_typ tau1.e_typ))
                        end
                end
        | Eprint e1 -> let tau = type_expr env class_env method_var_env e1 in 
                begin match tau.e_typ with
                | Tstring -> TEprint tau, Tvoid
                | _ -> raise (Typing_error (e1.e_loc, "unsupported argument type. expected: string. found: " ^ print_typ tau.e_typ))
                end
        end
 
and type_access env class_env method_var_env a =
        let d, tau = compute_type_access env class_env method_var_env a in
        { a_tdesc = d; a_typ = tau }

and compute_type_access env class_env method_var_env a =
        begin match a.a_desc with
        | Aident id -> if Smap.mem id method_var_env then 
                ( TAident id, Smap.find id method_var_env )
                else if class_env.name = "Main" then raise (Typing_error (a.a_loc, "unbound variable " ^ id))
                else begin try ( 
                        ( TAident id, find_field env class_env.name id a.a_loc) )
                with Not_found -> raise (Typing_error (a.a_loc, "unbound variable " ^ id)) end
        | Aattr (e, attr_id) -> let tau = type_expr env class_env method_var_env e in 
                begin match tau.e_typ with
                | Tnew (nt_id, _) -> let field_typ = find_field env nt_id attr_id a.a_loc in
                        TAattr (tau, attr_id), field_typ
                | _ -> raise (Typing_error (a.a_loc, "type doesn't support attributes"))
        (* need to modify this *)
                end     
        end

and type_method_call env class_env method_var_env a e_list =
        (* calling either a method from current class (first case) or a method from class of object (second case) *)
        begin match a.a_desc with
                | Aident mtd_id -> 
                begin try (
                let method_dict = find_method env class_env.name mtd_id a.a_loc in
                let ret_tau = method_dict.ret_typ in
                let te_list = List.map (fun e -> type_expr env class_env method_var_env e) e_list in
                        let (b, m) =  match_formals_actuals env class_env method_dict.formals te_list in
                        if b then 
                        let tacc = { a_tdesc = TAident mtd_id ; a_typ = ret_tau } in
                        TSEmethodCall (tacc, te_list), ret_tau
                        else raise (Typing_error (a.a_loc, m))
                ) with Typing_error (loc, m) -> raise (Typing_error (loc, m)) end
                | Aattr (e, mtd_id) -> 
                let tau = type_expr env class_env method_var_env e in 
                        begin match tau.e_typ with
                        | Tnew (nt_id, _) ->         
                        begin try (
                        let method_dict = find_method env nt_id mtd_id a.a_loc in 
                        let ret_tau = method_dict.ret_typ in
                        let tacc = { a_tdesc = TAattr (tau, mtd_id) ; a_typ = ret_tau } in
                        let te_list = List.map (fun e -> type_expr env class_env method_var_env e) e_list in
                        TSEmethodCall (tacc, te_list), ret_tau
                        ) 
                        with Typing_error (loc, m) -> raise (Typing_error (loc, m)) end
                        | Tstring -> if mtd_id = "equals" then 
                                let ret_tau = Tboolean in
                                let tacc = { a_tdesc = TAattr (tau, mtd_id) ; a_typ = ret_tau } in
                                let te_list = List.map (fun e -> type_expr env class_env method_var_env e) e_list in
                                TSEmethodCall (tacc, te_list), ret_tau
                        else raise (Typing_error (a.a_loc, "String class has no method " ^ mtd_id))
                        | _ -> raise (Typing_error (a.a_loc, "expression admits no attribute"))
                        end
        end

and match_formals_actuals env class_env formals actuals =
        (* when method call, checks if right number and type of arguments *)
        if List.length formals <> List.length actuals then (false, "actual and formal argument lists differ in length")
        else 
                let b = List.for_all2 
                (fun (Pparam (t, _)) { e_tdesc = _ ; e_typ = t_a } -> let t_f = get_typ t in compatible env class_env t_f t_a)
                formals actuals
                in 
                if b then (true, "")
                else (false, "actual and formal types do not match")

and check_constr_call env nt te_list loc =
        let Nntype (id, _) = nt in
        if id = "Object" then true
        else
                begin try ( let class_env = Smap.find id env in
                if class_env.is_class then
                        if Smap.mem id class_env.methods then
                        let method_dict = Smap.find id class_env.methods in
                        let formals = method_dict.formals in
                        let (b, m) = match_formals_actuals env class_env formals te_list in
                        if b then true 
                        else raise (Typing_error (loc, m))
                        else true
                else raise (Typing_error (loc, id ^ " is abstract; cannot be instantiated"))
        ) with Not_found -> raise (Typing_error (loc, "unbound class ")) end 

and find_method env nt_id mtd_id loc =
        let rec find_mtd nt_id mtd_id =
                begin try (
                let class_env = Smap.find nt_id env in
                if Smap.mem mtd_id class_env.methods then Smap.find mtd_id class_env.methods 
                else let scl = class_env.extends in
                begin match scl with
                | None | Some (Nntype ("Object", _)) -> raise (Typing_error (loc, "class " ^ nt_id ^ " not found")) 
                | Some (Nntype (cls_id, None)) -> find_mtd cls_id mtd_id
                | Some (Nntype (cls_id, Some x)) -> find_mtd cls_id mtd_id 
                end ) with Not_found -> raise (Typing_error (loc, "class " ^ nt_id ^ " not found")) end
        in find_mtd nt_id mtd_id
 
let rec type_instruction env class_env method_var_env ret_type i =
        begin match i.i_desc with
        | Ivoid -> method_var_env
        | Ise e -> let _ = type_expr env class_env method_var_env e in method_var_env
        | Iassign (a, e) -> let tau1 = type_access env class_env method_var_env a in 
        let tau2 = type_expr env class_env method_var_env e in
        if compatible env class_env tau1.a_typ tau2.e_typ then 
        method_var_env
        else raise (Typing_error (i.i_loc, print_incompatible tau1.a_typ tau2.e_typ))
        | Ideclare (t, id) -> 
                if Smap.mem id method_var_env then raise (Typing_error (i.i_loc, "variable " ^ id ^ "already defined in this class")) 
                else Smap.add id (get_typ t) method_var_env
        | IassignTyp (t, id, e) -> let tau = type_expr env class_env method_var_env e in
                let decl_t = get_typ t in
                if compatible env class_env decl_t tau.e_typ then 
                        if Smap.mem id method_var_env then raise (Typing_error (i.i_loc, "variable " ^ id ^ "already defined in this class")) 
                        else Smap.add id (get_typ t) method_var_env
                else raise (Typing_error (e.e_loc, print_incompatible decl_t tau.e_typ))
        | Iif (e, i) -> let tau = type_expr env class_env method_var_env e in 
                let t = tau.e_typ in
                if t = Tboolean then let _ = type_instruction env class_env method_var_env ret_type i in method_var_env 
                else raise (Typing_error (e.e_loc, "if clause requires a boolean. type " ^ print_typ t ^ " is incompatible with type boolean "))
        | IifElse (e, i1, i2) -> let tau = type_expr env class_env method_var_env e in 
                let t = tau.e_typ in
                if t = Tboolean then let _ = type_instruction env class_env method_var_env ret_type i1 in 
                        let _ = type_instruction env class_env method_var_env ret_type i2 in
                        method_var_env
                else raise (Typing_error (e.e_loc, "if clause requires a boolean. type " ^ print_typ t ^ " is incompatible with type boolean"))
        | Iwhile (e, i) -> let tau = type_expr env class_env method_var_env e in 
                let t = tau.e_typ in
                if t = Tboolean then let _ = type_instruction env class_env method_var_env ret_type i in
                        method_var_env
                else raise (Typing_error (e.e_loc, "while clause requires bool. type " ^ print_typ t ^ " is incompatible with type boolean"))
        | Iblock i_list -> let _ = List.fold_left (fun var_env i -> type_instruction env class_env var_env ret_type i) method_var_env i_list in
                method_var_env
        | Ireturn (Some e) -> let tau = type_expr env class_env method_var_env e in
                if compatible env class_env ret_type tau.e_typ then method_var_env
                else raise (Typing_error (e.e_loc, "invalid return type: " ^ print_incompatible ret_type tau.e_typ))
        | Ireturn None -> if ret_type = Tvoid then method_var_env
                else raise (Typing_error (i.i_loc, "invalid return type. type " ^ print_typ ret_type ^ " is incompatible with type void"))
        end

let add_formals method_var_env formals =
        List.fold_left 
        (fun v_env (Pparam (t, id)) -> 
        if Smap.mem id v_env then raise (Typing_error (fake_pos, "variable " ^ id ^ " is already defined in this environment"))
        else
        Smap.add id (get_typ t) v_env )
        method_var_env
        formals

let type_method env class_env method_env i_list loc =
        if List.for_all (fun (Pparam (t, _)) -> well_formed env class_env t) method_env.formals then
                let var_env = add_formals method_env.m_vars method_env.formals in
                let new_method_var_env = List.fold_left (fun method_var_env i -> type_instruction env class_env method_var_env method_env.ret_typ i) var_env i_list
                in { method_env with m_vars = new_method_var_env }
        else raise (Typing_error (loc, "invalid type"))
        
let check_has_return i_list =
        let rec has_return i =
                match i.i_desc with
                | Ireturn (Some e) -> true
                | Iblock i_list -> List.exists has_return i_list
                | _ -> false
        in List.exists has_return i_list

let type_decl env class_env d current_cls =
        let class_env_methods = class_env.methods in
        match d.d_desc with
        | Ddecl (t, id) -> if well_formed env class_env t then class_env else raise (Typing_error (d.d_loc, "unsound type"))
        | Dconstructor (Cconstr (id, par_list, i_list)) -> 
                if id <> current_cls then raise (Typing_error (d.d_loc, "constructor must have the same name as class")) 
                else
                begin try (
                let method_env = Smap.find id class_env_methods in 
                let new_method_env = type_method env class_env method_env i_list d.d_loc in
                let new_methods = Smap.add id new_method_env class_env_methods in
                { class_env with methods = new_methods }
                ) with Not_found -> failwith "constructor not found" end
        | Dmethod (Mmtd (p, i_list)) -> 
                begin match p with
                | Pproto (t, id, par_list) -> 
                        if check_has_return i_list then
                        begin try (let method_env = Smap.find id class_env_methods in 
                        let new_method_env = type_method env class_env method_env i_list d.d_loc in
                        let new_methods = Smap.add id new_method_env class_env_methods in
                        { class_env with methods = new_methods }
                        ) with Not_found -> failwith "method not found" end
                        else raise (Typing_error (d.d_loc, "missing return statement"))
                | PprotoVoid (id, par_list) -> 
                        begin try (let method_env = Smap.find id class_env.methods in 
                        let new_method_env = type_method env class_env method_env i_list d.d_loc in 
                        let new_methods = Smap.add id new_method_env class_env.methods in
                        { class_env with methods = new_methods }
                        ) with Not_found -> failwith "method not found" end
                end

let match_methods env class_env mtd_env1 mtd_env2 =
        (* returns true if return type of 1 is sub_type of return type of 2 and argument types match *)
        if sub_type env class_env mtd_env1.ret_typ mtd_env2.ret_typ then
                if List.length mtd_env1.formals = List.length mtd_env1.formals then
                        List.for_all2
                        (fun (Pparam (m_t, _)) (Pparam (c_t, _)) -> m_t = c_t ) (* check that argument types match *)
                        mtd_env1.formals mtd_env2.formals
                else false                                        
        else false

let check_type_params formals actuals =
        let f_list = begin match formals with
        | None -> []
        | Some x -> x 
        end
        in
        if List.length f_list <> List.length actuals then false
        else
        true

let check_extends env class_env c =
        (* checks that superclass exists and is a class *)
        match class_env.extends with
        | None -> (true, "")
        | Some (Nntype ("String", _)) -> (false, "cannot inherit from String class")
        | Some (Nntype ("Object", _)) -> (true, "") 
        | Some (Nntype (cls_id, None)) -> 
        if Smap.mem cls_id env then
                if (Smap.find cls_id env).is_class 
                        then if not (is_superclass env class_env.name cls_id) then (true, "")
                        else (false, "inheritence cycle between classes " ^ class_env.name ^ " and " ^ cls_id)
                else (false, "cannot inherit from interface")
        else (false, "class " ^ cls_id ^ " not defined")
        | Some nt -> if well_formed env class_env (Tntype nt) then 
                let (Nntype (cls_id, t_pars)) = nt in 
                begin match t_pars with
                | Some type_pars ->
                        if Smap.mem cls_id env then 
                                let superclass_env = Smap.find cls_id env in
                                (check_type_params superclass_env.type_vars type_pars, "")
                        else (false, "class " ^ cls_id ^ " not defined")
                | None -> (true, "")
                end
                else (false, "well_formed")

let check_implements env class_env c =
        (* check that interfaces exist and that all methods defined in them are correctly redefined in class *)
        match class_env.implements with
        | None -> (true, "")
        | Some (nt_list) -> let b = List.for_all
                (fun nt -> match nt with
                | Nntype (intf_id, None) -> if Smap.mem intf_id env && not (Smap.find intf_id env).is_class then (* interface exists *)
                if well_formed env class_env (Tntype nt) then 

                        let rec check_method_impl intf_id class_id =
                                let intf_env = Smap.find intf_id env in
                                let intf_methods = intf_env.methods in
                                let class_env = Smap.find class_id env in
                                let cls_methods = class_env.methods in
                                if Smap.for_all 
                                (fun mtd_id intf_mtd_env ->
                                        if Smap.mem mtd_id cls_methods then 
                                                let cls_mtd_env = Smap.find mtd_id cls_methods in
                                                match_methods env class_env cls_mtd_env intf_mtd_env
                                        else false
                                )
                                intf_methods
                                        then let extends = intf_env.implements in 
                                        begin match extends with
                                        | None -> true
                                        | Some nt_list ->
                                                List.for_all
                                                (fun (Nntype (id, _)) ->
                                                        check_method_impl id class_id)
                                                nt_list
                                        end
                                else false
                        in check_method_impl intf_id class_env.name
                        else false
                else raise (Typing_error (c.c_loc, "well_formed returned false in check_implements"))
                | Nntype (intf_id, Some nt_list) -> well_formed env class_env (Tntype nt) 
                )
                nt_list
        in if b then (true, "")
        else (false, "invalid implements argument")

let type_class env class_env c =
        (* VERIFICATIONS *)
        let (b1, m1) = check_extends env class_env c in
        if b1 then
                let (b2, m2) = check_implements env class_env c in
                if b2 then
                let current = c.c_name in (* class is identified by ident *)
                List.fold_left (fun c_env d -> type_decl env c_env d current ) class_env c.c_declarations
                (*with Typing_error (l, m) -> raise (Typing_error (l, m)) end*)
                else raise (Typing_error (c.c_loc, m2)) 
        else raise (Typing_error (c.c_loc, m1)) 
                
let check_extends_intf env intf_env i =
        (* checks that all superinterfaces are interfaces and that if i redefines super-methods then they are coherent *)
        let extends = intf_env.implements in
        match extends with
        | None -> (true, "")
        | Some nt_list ->
                let b = List.for_all
                (fun nt -> match nt with
                | Nntype (nt_id, None) -> if not (Smap.mem nt_id env) then false 
                else if (Smap.find nt_id env).is_class then false 
                else
                if not (is_superinterface env intf_env.name nt_id) then
                       let super_intf_env = Smap.find nt_id env in
                       let method_coherent =
                       Smap.for_all (* checks that all methods are coherent with superinterface methods *)
                       (fun mtd_id mtd_env -> if Smap.mem mtd_id super_intf_env.methods then 
                       (match_methods env intf_env mtd_env (Smap.find mtd_id super_intf_env.methods)) 
                       else true
                       )
                       intf_env.methods
                       in if method_coherent then true 
                       else false 
                else false
                | Nntype (nt_id, Some _) -> false 
                )
                nt_list
        in if b then (true, "")
        else (false, "invalid extends argument")

let type_intf env intf_env i = 
        let (b, m) = check_extends_intf env intf_env i in
        if b then 
        intf_env
        else raise (Typing_error (i.it_loc, m)) 

(* == PREPROCESSING == *)

let decl_cls_type_vars c =
        (* class -> paramtype list option *)
        let par_list =
        begin match c.c_params with
        | None -> None
        | Some (Pparamstype p_list) -> Some p_list 
        (*
        | None -> Smap.empty
        | Some (Pparamstype p_list) -> 
        List.fold_left 
        (fun env (Pparamtype (id, n_list)) ->
        Smap.add id n_list env
        )
        Smap.empty
        p_list*)
        end
        in begin match par_list with (* check for duplicates *)
        | None -> par_list
        | Some p_list ->
        let _ = List.fold_left
        (fun set (Pparamtype (id, _)) ->
                if Sset.mem id set then raise (Typing_error (fake_pos, "type variable already defined in class")) 
                else Sset.add id set)
        Sset.empty
        p_list 
        in par_list
        end

let decl_methods_fields c =
        (* CLASS PREPROCESSING: adds constructor, methods and fields to env *)
        List.fold_left
        (fun (mtds, fields) decl -> 
        begin match decl.d_desc with
        | Ddecl (t, id) -> if Smap.mem id fields then raise (Typing_error (decl.d_loc, "field " ^ id ^ " already defined in this class")) 
                else (mtds, Smap.add id (get_typ t) fields)
        | Dmethod (Mmtd (p, _)) -> 
                begin match p with
                | Pproto (t, id, p_list) ->
                if Smap.mem id mtds then raise (Typing_error (decl.d_loc, "method " ^ id ^ " already defined in this class")) 
                else (Smap.add id { ret_typ = (get_typ t); formals = p_list; m_vars = Smap.empty } mtds, fields)
                | PprotoVoid (id, p_list) -> 
                if Smap.mem id mtds then raise (Typing_error (decl.d_loc, "method " ^ id ^ " already defined in this class")) 
                else (Smap.add id { ret_typ = Tvoid; formals = p_list; m_vars = Smap.empty } mtds, fields)
                end
        | Dconstructor (Cconstr (id, p_list, i_list)) -> 
                if Smap.mem id mtds then raise (Typing_error (decl.d_loc, "constructor " ^ id ^ " already defined in this class"))        
                else (Smap.add id { ret_typ = Tvoid ; formals = p_list; m_vars = Smap.empty } mtds, fields)
        end
        )
        (empty_class_env.methods, empty_class_env.fields)
        c.c_declarations 

let init_class_env c method_dict fields type_vars =
        { is_class = true; name = c.c_name; extends = c.c_extends; implements = c.c_implements; type_vars = type_vars; methods = method_dict; fields = fields }

let decl_intf_type_vars i =
        (* interface -> ntype list option Smap.t *)
        let par_list =
        begin match i.i_params with
        | None -> None
        | Some (Pparamstype p_list) -> Some p_list
        (*
        | None -> Smap.empty
        | Some (Pparamstype p_list) -> 
        List.fold_left 
        (fun env (Pparamtype (id, n_list)) ->
        Smap.add id n_list env
        )
        Smap.empty
        p_list*)
        end
        in begin match par_list with (* check for duplicates *)
        | None -> par_list
        | Some p_list ->
        let _ = List.fold_left
        (fun set (Pparamtype (id, _)) ->
                if Sset.mem id set then raise (Typing_error (fake_pos, "type variable already defined in class")) 
                else Sset.add id set)
        Sset.empty
        p_list 
        in par_list
        end

let decl_proto i =
        List.fold_left
        (fun env p -> 
                begin match p with
                | Pproto (t, id, p_list) ->
                if Smap.mem id env then raise (Typing_error (i.it_loc, "method " ^ id ^ " already defined in this class")) 
                else Smap.add id { ret_typ = (get_typ t); formals = p_list; m_vars = Smap.empty } env
                | PprotoVoid (id, p_list) ->
                if Smap.mem id env then raise (Typing_error (i.it_loc, "method " ^ id ^ " already defined in this class")) 
                else Smap.add id { ret_typ = Tvoid; formals = p_list; m_vars = Smap.empty } env
                end
        )
        Smap.empty
        i.i_protocols

let init_intf_env i method_dict type_vars =
        { is_class = false; name = i.i_name; extends = None; implements = i.i_extends; type_vars = type_vars; methods = method_dict; fields = empty_class_env.fields }

let decl_cls_intf env cls_intf_list =
        (* file PREPROCESSING before the actual typing *)
        (* adds classes and interfaces to env *)
        List.fold_left 
        (fun env c_i -> 
                begin match c_i with
                | CClass c -> let id = c.c_name in
                if Smap.mem id env then raise (Typing_error (c.c_loc, "duplicate class: " ^ id))
                else
                        let (method_dict, fields) = decl_methods_fields c in
                        let type_vars = decl_cls_type_vars c in
                        let class_env = init_class_env c method_dict fields type_vars in
                        Smap.add id class_env env
                | CInterface i -> let id = i.i_name in
                if Smap.mem id env then raise (Typing_error (i.it_loc, "duplicate interface: " ^ id))
                else 
                        let method_dict = decl_proto i in
                        let type_vars = decl_intf_type_vars i in
                        let intf_env = init_intf_env i method_dict type_vars in
                        Smap.add id intf_env env 
                end
        )
        env
        cls_intf_list

let rec type_cls_intf env cls_intf_list =
        let new_env = decl_cls_intf env cls_intf_list in (* PREPROCESSING *)
        let new_full_env = new_env in 
        List.fold_left (* ACTUAL TYPING: going down the ast to expr *)
        (fun env c_i -> 
                begin match c_i with
                | CClass c -> begin try ( let class_env = Smap.find c.c_name env in
                        let new_class_env = type_class env class_env c in
                        Smap.add c.c_name new_class_env env )
                with Not_found -> failwith "class not found" end
                | CInterface i -> begin try ( let intf_env = Smap.find i.i_name env in
                        let new_intf_env = type_intf env intf_env i in
                        Smap.add i.i_name new_intf_env env ) 
                with Not_found -> failwith "interface not found" end
                end
        )
        new_full_env
        cls_intf_list

let type_class_main env class_main =
        let class_id = "Main" in
        let new_env = Smap.add class_id empty_class_env env in 
        let (CClassMain (_, i_list)) = class_main in
        let ret_type = Tvoid in
        let new_var_env = 
        List.fold_left (fun m_v_env i -> type_instruction new_env empty_class_env m_v_env ret_type i) empty_method_env.m_vars i_list 
        in
        let class_main_method_env = { ret_typ = Tvoid; formals = []; m_vars = new_var_env } in
        let class_main_env = { empty_class_env with methods = Smap.add "main" class_main_method_env empty_class_env.methods } in
        Smap.add class_id class_main_env env
        
let type_file ast_file = 
        (* -> full_env *)
        let (Ffile (cls_intf_list, class_main)) = ast_file in
        let new_env = type_cls_intf empty_env cls_intf_list
        in type_class_main new_env class_main
