open Format
open Ast
open Typed_ast
open X86_64
open Utils

exception Compile_error of string

type pos_env = int Smap.t

(*type object_table = (ident, (string, string, int, int Smap.t)) Hashtbl.t*)
(*let object_table = Hashtbl.create 17*)

(*type label = string*)
type class_descriptor =
        { cls_label : label; (* class label *)
        scl_label : label option; (* superclass label or None *)
        mtd_ofs : int Smap.t; (* offset of methods in class descriptor *)
        mtd_labels: label Smap.t; (* method labels in class descriptor *)
        field_map: int Smap.t; (* offset of fields *)
        field_list: ident list} (* list of fields in right order *)

let field_table = Hashtbl.create 17 (* only for superest classes *)
let class_descriptor_table = Hashtbl.create 17 (* for all classes *)

let string_data = ref (label ".Sprint_int" ++ string "%d")
let string_nb = ref 0 
let w_nb = ref 0 
let l_nb = ref 0
let i_nb = ref 0
let e_nb = ref 0
let r_nb = ref 0
let a_nb = ref 0
let o_nb = ref 0
let h_nb = ref 0
let f_nb = ref 0

let generate_str_label () =
        incr string_nb;
        "str" ^ string_of_int !string_nb

let generate_w_label () =
        incr w_nb;
        "W" ^ string_of_int !w_nb

let generate_l_label () =
        incr l_nb;
        "L" ^ string_of_int !l_nb

let generate_i_label () =
        incr i_nb;
        "I" ^ string_of_int !i_nb

let generate_e_label () =
        incr e_nb;
        "E" ^ string_of_int !e_nb

let generate_r_label () =
        incr r_nb;
        "R" ^ string_of_int !r_nb

let generate_a_label () =
        incr a_nb;
        "A" ^ string_of_int !a_nb

let generate_o_label () =
        incr o_nb;
        "O" ^ string_of_int !o_nb

let generate_h_label () =
        incr h_nb;
        "H" ^ string_of_int !h_nb

let generate_f_label () =
        incr f_nb;
        "F" ^ string_of_int !f_nb

let print_string msg_lab =
        movq !%rdi !%rsi ++
        movq (lab ("$"^msg_lab)) !%rdi ++
        movq (imm 0) !%rax ++
        call "printf"
(*
let find_method_id a = 
        match a.a_tdesc with
        | TAident id -> id
        | TAattr (_, id) -> id (* change later *)*)

let rec compile_expr pos_env field_map e =
        (* -> asm.text *)
        match e.e_tdesc with
        | TEconstant c ->
                begin match c with
                | Cstring s -> let msg_lab = generate_str_label () in
                string_data := !string_data ++ label msg_lab ++ string s; 
                movq (lab ("$"^msg_lab)) !%rax ++
                pushq !%rax
                | Cint n ->
                movq (imm n) !%rax ++
                pushq !%rax 
                end 
        | TEbool b -> 
                let bit = begin match b with
                | false -> 0
                | true -> 1
                end in
                movq (imm bit) !%rax ++
                pushq !%rax
        | TEthis -> 
                begin try (
                let this_ofs = Smap.find "this" pos_env in
                movq (ind ~ofs:this_ofs rbp) !%rdi ++
                pushq !%rdi
                ) with Not_found -> failwith "this not found" end
        | TEnew (nt, class_id, scl_id, e_list) -> 
                (* APPEL À CONSTRUCTEUR *)
                let call_super_constr = begin match scl_id with
                | None -> nop
                | Some s_id -> let super_constr_label = s_id ^ "_constr" in call super_constr_label
                end in
                let constr_label = class_id ^ "_constr" in
                begin try (let cl_descr = Hashtbl.find class_descriptor_table class_id
                in
                let obj_field_list = cl_descr.field_list in
                let obj_field_map = cl_descr.field_map in
                let nb_fields = (List.length obj_field_list + 1) * 8 in
                let init_fields = List.fold_left
                (fun code field_id -> let ofs = Smap.find field_id obj_field_map in code ++ movq (imm 0) (ind ~ofs:ofs rax))
                nop obj_field_list in
                let code_args = List.fold_left (fun code e -> code ++ compile_expr pos_env field_map e) nop e_list in
                let nb_args = (List.length e_list + 1) * 8 in
                code_args ++
                movq (imm nb_fields) !%rdi ++
               (* movq (imm nb_args) !%rdi ++ *)
                call "malloc" ++
                init_fields ++
                pushq !%rax ++
                (* mettre l'adresse du descripteur de classe dans le premier champ *)
                call_super_constr ++
                call constr_label ++
                addq (imm nb_args) !%rsp ++
                pushq !%rax
                ) with Not_found -> failwith ("class not found in TEnew constructor call " ^ class_id) end 
        | TEassign (a, e) -> (* modification de la valeur d'une variable locale *)
                begin match a.a_tdesc with
                | TAident id ->
                        if Smap.mem id pos_env then
                                let ofs = - (Smap.find id pos_env) in
                                compile_expr pos_env field_map e ++
                                popq rax ++
                                movq !%rax (ind ~ofs rbp) ++
                                pushq !%rax 
                        else begin try
                                let obj_ofs = Smap.find id field_map in
                                compile_expr pos_env field_map e ++
                                popq rax ++
                                movq (ind ~ofs:24 rbp) !%rdi ++
                                movq !%rax (ind ~ofs:obj_ofs rdi) ++
                                pushq !%rax
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in TEassign")) end
                | TAattr (e2, id) -> 
                        let static_type = begin match e.e_typ with
                        | Tnew (c_id, _) -> c_id (* get superclass id *)
                        | _ -> failwith "should not happen here in TEassign"
                        end in
                        begin try
                                let obj_field_map = Hashtbl.find field_table static_type in
                                let ofs = Smap.find id field_map in
                                compile_expr pos_env field_map e ++
                                popq rax ++
                                compile_expr pos_env obj_field_map e2 ++
                                popq rcx ++
                                movq !%rax (ind ~ofs rcx) ++
                                pushq !%rax
                        with Not_found -> failwith "field_map not found in compile_expr" end
                end
        | TEmethodCall (a, e_list) -> 
                (* APPEL DE MÉTHODE *)
                let code_args = List.fold_left (fun code e -> code ++ compile_expr pos_env field_map e) nop e_list in
                let nb_args = (List.length e_list + 1) * 8 in
                begin match a.a_tdesc with
                | TAident id -> failwith "not implemented yet in TEmethodCall" 
                                (*let static_type = current_cls_id in 
                                (* this? *)
                                let mtd_label = static_type^"_"^id in
                                code_args ++ 
                                (*compile_expr pos_env field_map e ++ *)
                                call mtd_label ++
                                addq (imm nb_args) !%rsp ++
                                pushq !%rax*)

                        (*if Smap.mem id pos_env then 
                                let ofs = - (Smap.find id pos_env) in
                                movq (ind ~ofs rbp) !%rax ++
                                pushq !%rax
                        else begin try
                                let obj_ofs = Smap.find id field_map in
                                movq (ind ~ofs:24 rbp) !%rdi ++
                                movq (ind ~ofs:obj_ofs rdi) !%rax ++
                                pushq !%rax
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in TEaccess")) end*)
                | TAattr (e, id) ->
                        begin match e.e_typ with
                        | Tnew (c_id, _) -> let static_type = c_id in (* get superclass id *)
                                let mtd_label = static_type^"_"^id in
                                code_args ++ (* arguments on stack *)
                                compile_expr pos_env field_map e ++ (* this on stack *)
                                call mtd_label ++
                                addq (imm nb_args) !%rsp ++
                                pushq !%rax
                        | Tstring -> let h_lab = generate_h_label () in
                                let f_lab = generate_f_label () in
                                compile_expr pos_env field_map e ++
                                code_args ++
                                popq rdx ++
                                popq rcx ++
                                cmpq !%rdx !%rcx ++
                                jz h_lab ++ (* if true *)
                                movq (imm 0) !%rax ++ (* false *)
                                pushq !%rax ++
                                jmp f_lab ++
                                label h_lab ++
                                movq (imm 1) !%rax ++ (* true *)
                                pushq !%rax ++
                                label f_lab
                        | _ -> failwith ("should not happen here in TEmethodCall ")
                        end
               end
(*
                        (* place arguments on top of stack
                           call method_label
                           dépiler les arguments *)
                let nb_params = List.length e_list in
                List.fold_left (fun code e -> code ++ compile_expr e)
                nop
                e_list
                ++ call (find_method_id a)
                ++ addq (imm 8*nb_params) !%rsp (* restore stack *)
                ++ pushq !%rax*)
 
        | TEaccess a -> (* accès à une variable locale *)
                begin match a.a_tdesc with
                | TAident id -> 
                        if Smap.mem id pos_env then 
                                let ofs = - (Smap.find id pos_env) in
                                movq (ind ~ofs rbp) !%rax ++
                                pushq !%rax
                        else begin try
                                let obj_ofs = Smap.find id field_map in
                                movq (ind ~ofs:24 rbp) !%rdi ++
                                movq (ind ~ofs:obj_ofs rdi) !%rax ++
                                pushq !%rax
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in TEaccess")) end
                | TAattr (e, id) ->
                        let static_type = begin match e.e_typ with
                        | Tnew (c_id, _) -> c_id (* get superclass id *)
                        | _ -> failwith "should not happen here"
                        end in
                        begin try
                                let field_map = Hashtbl.find field_table static_type in
                                let ofs = Smap.find id field_map in
                                compile_expr pos_env field_map e ++
                                popq rcx ++
                                movq (ind ~ofs rcx) !%rax ++
                                pushq !%rax
                        with Not_found -> failwith "field_map not found in compile_expr" end
                end
        | TEnull -> 
                movq (imm 0) !%rax ++
                pushq !%rax
        | TEunop (o, e) ->
                let op = begin match o with
                | Uneg -> negq
                | Unot -> notq
                end in
                compile_expr pos_env field_map e ++ 
                popq rax ++
                op !%rax ++
                pushq !%rax
        | TEbinop (o, e1, e2) ->
                let exprs = 
                compile_expr pos_env field_map e1 ++
                compile_expr pos_env field_map e2 ++
                popq rbx ++
                popq rax 
                in
                begin match o with
                | Bdiv | Bmod -> 
                        let result = begin match o with
                        | Bdiv -> !%rax
                        | Bmod -> !%rdx 
                        | _ -> assert false
                        end in
                        exprs ++
                        movq (imm 0) !%rdx ++
                        idivq !%rbx ++
                        pushq result
                | Badd | Bsub | Bmul  ->
                        let op = begin match o with
                        | Badd -> addq
                        | Bsub -> subq
                        | Bmul -> imulq
                        | _ -> assert false
                        end in 
                        exprs ++
                        op !%rbx !%rax ++
                        pushq !%rax
                | Band ->
                        let a_lab1 = generate_a_label () in
                        let a_lab2 = generate_a_label () in
                        compile_expr pos_env field_map e1 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jz a_lab1 ++
                        compile_expr pos_env field_map e2 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jz a_lab1 ++
                        movq (imm 1) !%rax ++
                        pushq !%rax ++
                        jmp a_lab2 ++
                        label a_lab1 ++
                        pushq !%rax ++
                        label a_lab2
                | Bor -> 
                        let o_lab1 = generate_o_label () in
                        let o_lab2 = generate_o_label () in
                        compile_expr pos_env field_map e1 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jnz o_lab1 ++
                        compile_expr pos_env field_map e2 ++
                        popq rax ++
                        testq !%rax !%rax ++
                        jnz o_lab1 ++
                        movq (imm 0) !%rax ++
                        pushq !%rax ++
                        jmp o_lab2 ++
                        label o_lab1 ++
                        pushq !%rax ++
                        label o_lab2
                | Beq | Bneq | Blt | Ble | Bgt | Bge ->
                        let op = begin match o with
                        | Beq -> sete
                        | Bneq -> setne
                        | Blt -> setl
                        | Ble -> setle
                        | Bgt -> setg
                        | Bge -> setge
                        | _ -> assert false
                        end in
                        compile_expr pos_env field_map e1 ++
                        compile_expr pos_env field_map e2 ++
                        popq rbx ++
                        popq rcx ++
                        movq (imm 0) !%rax ++
                        cmpq !%rbx !%rcx ++
                        op !%al ++
                        pushq !%rax
                (*        exprs ++
                        cmpq !%rbx !%rax ++
                        op !%al ++
                        pushq !%rax*)
                end
          | TEconcat (Badd, e1, e2) ->
                compile_expr pos_env field_map e1 ++
                compile_expr pos_env field_map e2 ++
                popq rsi ++
                popq rdi ++
                call "concat" ++
                pushq !%rax
          | TEconcat (_, _, _) -> failwith "should not happen"
          | TEconcatIntString (Badd, e1, e2) ->
                compile_expr pos_env field_map e1 ++
                compile_expr pos_env field_map e2 ++
                popq rsi ++
                popq rdi ++
                call "concat_int_str" ++
                pushq !%rax
          | TEconcatIntString (_, _, _) -> failwith "should not happen"
          | TEconcatStringInt (Badd, e1, e2) ->
                compile_expr pos_env field_map e1 ++
                compile_expr pos_env field_map e2 ++
                popq rsi ++
                popq rdi ++
                call "concat_str_int" ++
                pushq !%rax
          | TEconcatStringInt (_, _, _) -> failwith "should not happen" 
          | TEprint e1 -> compile_expr pos_env field_map e1 ++
                popq rdi ++
                movq (imm 0) !%rax ++
                call "printf"

let rec compile_instr code pos_env field_map next i =
        (* -> asm.text, pos_env (maps a var to its pos on stack), next (first free pos on stack) *)
        match i with
        | TIvoid -> code, pos_env, next
        | TIse e -> begin match e.e_tdesc with
                | TEnew _ -> code ++ compile_expr pos_env field_map e ++ popq rax, pos_env, next
                |  _ -> code ++ compile_expr pos_env field_map e, pos_env, next
        end
        | TIassign (a, e) ->
                begin match a.a_tdesc with
                | TAident id ->
                        if Smap.mem id pos_env then
                                let ofs = - (Smap.find id pos_env) in
                                code ++
                                compile_expr pos_env field_map e ++
                                popq rax ++
                                movq !%rax (ind ~ofs rbp)
                        else begin try
                                let obj_ofs = Smap.find id field_map in
                                let this_ofs = Smap.find "this" pos_env in
                                code ++
                                compile_expr pos_env field_map e ++
                                popq rax ++
                                movq (ind ~ofs:this_ofs rbp) !%rdi ++
                                movq !%rax (ind ~ofs:obj_ofs rdi)
                        with Not_found -> raise (Compile_error ("undefined variable " ^ id ^ " in TIassign")) end
                | TAattr (e2, id) -> 
                        let static_type = begin match e2.e_typ with
                        | Tnew (c_id, _) -> c_id (* get superclass id *)
                        | _ -> failwith "should not happen here in TIassign"
                        end in
                        begin try
                                let obj_field_map = Hashtbl.find field_table static_type in
                                let ofs = Smap.find id field_map in
                                code ++
                                compile_expr pos_env field_map e ++
                                popq rax ++
                                compile_expr pos_env obj_field_map e2 ++
                                popq rcx ++
                                movq !%rax (ind ~ofs rcx)
                        with Not_found -> failwith "field_map not found in compile_expr" end
                end
                , pos_env, next
        | TIdeclare (_, id) -> code, (Smap.add id next pos_env), (next + 8) 
        | TIdeclAssign (_, id, e) -> 
                code ++ 
                compile_expr pos_env field_map e ++
                popq rax ++
                movq !%rax (ind ~ofs:(-next) rbp),
                (Smap.add id next pos_env), (next + 8)
        | TIif (e, i) ->
                let i_lab = generate_i_label () in
                let code_if, _, _ = compile_instr nop pos_env field_map next i in
                code ++ 
                compile_expr pos_env field_map e ++
                popq rax ++
                testq !%rax !%rax ++
                jz i_lab ++  (* if rax == 0: if false *)
                code_if ++
                label i_lab,
                pos_env, next
        | TIifElse (e, i1, i2) -> 
                let e_lab = generate_e_label () in
                let r_lab = generate_r_label () in
                let code_if_true, _, _ = compile_instr nop pos_env field_map next i1 in
                let code_if_false, _, _ = compile_instr nop pos_env field_map next i2 in
                code ++ 
                compile_expr pos_env field_map e ++
                popq rax ++
                testq !%rax !%rax ++
                jz e_lab ++  (* if rax == 0: if false *)
                code_if_true ++
                jmp r_lab ++
                label e_lab ++
                code_if_false ++
                label r_lab,
                pos_env, next
        | TIwhile (e, i) ->
                let w_lab = generate_w_label () in
                let l_lab = generate_l_label () in
                let code_while, _, _ = compile_instr nop pos_env field_map next i in
                code ++
                jmp l_lab ++
                label w_lab ++
                code_while ++
                label l_lab ++
                compile_expr pos_env field_map e ++
                popq rax ++
                testq !%rax !%rax ++
                jnz w_lab (* if rax == 1: if true *),
                pos_env, next
        | TIblock (i_list) -> 
                let code_block, _, _ = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env field_map next i)
                (nop, pos_env, 0)
                i_list
                in 
                code ++
                code_block,
                pos_env, next
        | TIreturn e -> begin match e with
                | Some e1 -> code ++
                        compile_expr pos_env field_map e1 ++
                        popq rax
                        , pos_env, next
                | None -> code ++
                        movq (imm 0) !%rax
                        , pos_env, next
                end

let compile_main tclass_main =
        let _, i_list = tclass_main in
        let field_map = Smap.empty in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env field_map next i) 
        (nop, Smap.empty, 0)
        i_list
        in
        subq (imm frame_size) !%rsp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        addq (imm frame_size) !%rsp ++
        movq (imm 0) !%rax

        (*
        (* pushq rbp (sauvegarde ancien rbp)
         movq rsp rbp (le sommet de la pile au moment de l'appel devient le fond de la frame de l'appelé)
         subq (imm frame_size) rsp (alloue son tableau d'activation = 8 * nombre de variables locales de la fonction) 
         on accède aux variables locales de la fonction par rapport à rbp 
         --code de la fonction--
         résultat dans rax
         leave (dépile tableau d'activation et restaure rbp)
         ret
         *)

let compile_method pos_env m =
        let p = m.tm_proto in 
        let lab = begin match p with
        | Pproto (_, id, _) -> id
        | PprotoVoid (id, _) -> id
        in 
        let m_env = m.tm_env in
        let formals = m_env.formals in
        let pos_env, _ = List.fold_left (fun (env, i) (_, p_id) ->
                (Smap.add p_id 16+8*i env, i+1) (*?*)
        )
        (pos_env, 1)
        formals
        in
        let compiled = List.fold_left (fun code i -> code ++ compile_instr i) 
        nop
        m.tm_instr
        in 
        let code =
        (label lab) ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        compiled ++
        popq rax ++
        popq rbp ++
        ret
        in code, pos_env *)

let compile_method mtd =
        let mtd_id = mtd.tm_id in
        let class_id = mtd.tm_cl_id in
        let scl_id = mtd.tm_scl_id in
        let method_env = mtd.tm_env in
        let formals = method_env.formals in
        let i_list = mtd.tm_instr in
        let c_id = begin match scl_id with
        | None -> class_id
        | Some s_id -> s_id 
        end in
        let field_map = begin try Hashtbl.find field_table c_id with Not_found -> failwith ("class not found in compile_constructor " ^ c_id) (* get superclass id *) end
        in
        let arg_size = (List.length formals + 1) * 8 in
        let pos_env, this_ofs = 
        List.fold_left (fun (pos_env, ofs) (_, arg) ->
                (Smap.add arg (-ofs) pos_env, ofs - 8))
        (Smap.empty, arg_size + (*16*) 8)
        formals
        in 
        let pos_env = Smap.add "this" this_ofs pos_env (* ? *)
        in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env field_map next i) 
        (nop, pos_env, 0)
        i_list
        in
        label (class_id^"_"^mtd_id) ++
        pushq !%rbp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        popq rbp ++
        ret

let compile_constructor constr =
        let class_id, scl_id, formals, i_list, _ = constr in
        let c_id = begin match scl_id with
        | None -> class_id
        | Some s_id -> s_id 
        end in
        let field_map = begin try Hashtbl.find field_table c_id with Not_found -> failwith ("class not found in compile_constructor " ^ c_id) (* get superclass id *) end
        in
        let arg_size = (List.length formals + 1) * 8 in
        let pos_env, this_ofs = 
        List.fold_left (fun (pos_env, ofs) (_, arg) ->
                (Smap.add arg (-ofs) pos_env, ofs - 8))
        (Smap.empty, arg_size + 16)
        formals
        in 
        let pos_env = Smap.add "this" this_ofs pos_env (* ? *)
        in
        let (code_instr, _, frame_size) = List.fold_left (fun (code, pos_env, next) i -> compile_instr code pos_env field_map next i) 
        (nop, pos_env, 0)
        i_list
        in
        label (class_id^"_constr") ++
        pushq !%rbp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        movq (ind ~ofs:24 rbp) !%rax ++
        popq rbp ++
        ret


        (*
        subq (imm frame_size) !%rsp ++
        leaq (ind ~ofs:(frame_size - 8) rsp) rbp ++
        code_instr ++
        movq (ind ~ofs:8 rbp) !%rax ++
        addq (imm frame_size) !%rsp ++
        ret *)



(*
(*        let descr_label, constr_label, nb_fields, field_map = Hashtbl.find object_table class_id in*)
        let field_values = List.fold_left (fun code field -> 
                let ofs = Smap.find field field_map in 
                code ++
                popq !%rax ++
                movq !%rax (ind ~ofs rdi)
        )
        nop
        fields
        in
        (label constr_label) ++
        field_values ++
        ret 

        let pos_env, _ = List.fold_left (fun (env, i) (_, p_id) ->
                (Smap.add p_id 16+8*i env, i+1) (*?*)
        )
        (pos_env, 1)
        formals
        in
        let compiled = List.fold_left (fun code i -> code ++ compile_instr i) 
        nop
        i_list
        in 
        let code =
        (label lab) ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        compiled ++
        popq rax ++
        popq rbp ++
        ret
        in code, pos_env
*)
let compile_decl d =
        match d with
        | TDdecl (_, id) -> nop
        | TDconstructor c -> compile_constructor c
        | TDmethod m -> compile_method m

let compile_class c =
        let decls = c.tc_declarations in
        List.fold_left (fun code d -> code ++ compile_decl d) 
        nop
        decls

let compile_classes cls_intf_list =
        List.fold_left (fun code c_i -> 
               begin match c_i with
               | TCClass c -> code ++ compile_class c
               | TCInterface i -> code
               end) 
        nop
        cls_intf_list
(*
let get_superclass full_env c_id =
        (* Some c_id | None *)
        let c_env = Smap.find c_id full_env in
        let scl = c_env.extends in
        begin match scl with
        | None -> None
        | Some (Nntype (c, _)) -> Some c
        end

let fill_field_table full_env field_table c_id = 
        let c_env = Smap.find c_id full_env in
        let fields = c_env.fields in
        let field_map, _ =
        Smap.fold
        (fun (env, i) field -> Smap.add field 16+8*i env, i+1)
        (Smap.empty, 1)
        fields
        in
        Hashtbl.add field_table c_id field_map
        *)

let make_field_map class_id field_list =
        let field_map, _ =
        List.fold_left (fun (env, pos) field ->
                (Smap.add field pos env, pos + 8)) 
        (Smap.empty, 8)
        field_list
        in field_map

let make_field_table cls_intf_list full_env =
        List.iter
        (fun cls_intf ->
                begin match cls_intf with
                | TCClass cls ->
                        let sclass_env = cls.tc_scl in
                        let sclass_id = sclass_env.name in
                        if Hashtbl.mem field_table sclass_id then ()
                        else let field_list = sclass_env.field_list in
                        let field_map = make_field_map sclass_id field_list in
                        Hashtbl.add field_table sclass_id field_map
                | TCInterface _ -> ()
                end 
        )
        cls_intf_list        
 
let make_class_descriptor c =
        let c_id = c.tc_name in
        let descr_label = "descr_" ^ c_id in 
        let class_env = c.tc_env in
        let scl_env = class_env.scl_env in 
        let method_env = class_env.methods in
        let cls_descriptor = begin match scl_env with
        | None | _ ->  (* change this *)
        let mtd_ofs, mtd_labels, _ = Smap.fold
        (fun mtd_id mtd_env (mtd_ofs, mtd_labels, ofs) ->
                (Smap.add mtd_id ofs mtd_ofs, Smap.add mtd_id (c_id ^ "_" ^ mtd_id) mtd_labels, ofs + 8)
        )
        method_env
        (Smap.empty, Smap.empty, 8)
        in
        let field_list = class_env.field_list in
        let field_map = make_field_map c_id field_list in
        { cls_label = descr_label;
        scl_label = None;
        mtd_ofs = mtd_ofs;
        mtd_labels = mtd_labels; 
        field_map = field_map; 
        field_list = field_list} 
        | Some scl_env -> let scl_id = scl_env.name in
                let scl_descriptor = begin try 
                        (Hashtbl.find class_descriptor_table scl_id) with Not_found -> failwith "superclass not found in class_descriptor_table" end
                in let scl_label = scl_descriptor.cls_label
                in
                let scl_mtd_ofs = scl_descriptor.mtd_ofs in
                let mtd_ofs, mtd_labels, _ = Smap.fold
                (fun mtd_id mtd_env (mtd_ofs, mtd_labels, ofs) ->
                if Smap.mem mtd_id scl_mtd_ofs then 
                        (Smap.add mtd_id (Smap.find mtd_id scl_mtd_ofs) mtd_ofs, Smap.add mtd_id (c_id ^ "_" ^ mtd_id) mtd_labels, ofs + 8) 
                else 
                (Smap.add mtd_id ofs mtd_ofs, Smap.add mtd_id (c_id ^ "_" ^ mtd_id) mtd_labels, ofs + 8)
                ) 
               method_env
               (Smap.empty, Smap.empty, 8)
        in
        let field_list = class_env.field_list in
        let field_map = make_field_map c_id field_list in
        { cls_label = descr_label;
        scl_label = Some scl_label;
        mtd_ofs = mtd_ofs; 
        mtd_labels = mtd_labels; 
        field_map = field_map; 
        field_list = field_list} 
        end
        in Hashtbl.add class_descriptor_table c_id cls_descriptor

let make_class_descriptors cls_intf_list =
        (* treat superclass first *)
        List.iter (fun c_intf -> begin match c_intf with
        | TCClass c -> make_class_descriptor c
        | TCInterface _ -> ()
        end 
        )
        cls_intf_list 

       (* 
        let rec make_class c_id =
                if Hashtbl.mem class_table c_id then ()
                else begin match get_superclass full_env c_id with
                | None -> make_class_descriptor class_table c_id
                | Some scl -> make_class scl; make_class_descriptor class_table c_id; fill_field_table field_table c_id
        in List.iter (fun c -> make_class c) cls_intf_list*)

let write_class_descriptors cls_intf_list =
        make_class_descriptors cls_intf_list;
        Hashtbl.fold (fun c_id meta_descr code -> let s_label = begin match meta_descr.scl_label with
        | None -> "0"
        | Some scl_label -> scl_label
        end in 
        code ++
        label meta_descr.cls_label ++
        address [s_label] ++
        Smap.fold (fun mtd_id mtd_label code -> code ++ address [mtd_label]
        ) meta_descr.mtd_labels
        nop
        )
        class_descriptor_table 
        nop
       
(* main function *)
let compile_file typed_ast out_file = 
        (* takes as input the output of the main typer function = typed ast *)

        let cls_intf_list = typed_ast.tf_cls_intf_list in
        let class_main = typed_ast.tf_class_main in
        let full_env = typed_ast.tf_full_env in
        make_field_table cls_intf_list full_env;
        make_class_descriptors cls_intf_list;

        let cls_data = nop in (*write_class_descriptors cls_intf_list in*)

        let class_code = compile_classes cls_intf_list in
        
        let main_code = compile_main class_main in

        let p =
        {
	        text =
                        globl "main" ++
                        label "main" ++
                        main_code ++
                        ret ++
                        class_code ++
                        concat_code ++
                        concat_int_str_code ++
                        concat_str_int_code
        ;
                data = 
                        !string_data ++
                        cls_data
	}

        in
        let f = open_out out_file in
        let fmt = formatter_of_out_channel f in
        print_program fmt p;
        fprintf fmt "@?";
        close_out f
